#pragma once
#include "ShaderProgram.h"
#include "SceneComponent.h"
#include "Transform.h"
#include "Framebuffer.h"
#include <string>
#include <QMatrix4x4>

class Camera : public SceneComponent
{
public:
	typedef void (*RenderFunction)(
		const QMatrix4x4&, 
		const QMatrix4x4&, 
		const QVector3D&, 
		Framebuffer*, 
		QVector4D, 
		bool);

	Camera(SceneObject* parent);
	Camera(
		SceneObject* parent, 
		RenderFunction renderFunction, 
		Framebuffer* outputTarget = nullptr, 
		QVector4D backgroundColor = QVector4D(0.3f, 0.3f, 0.3f, 1.0f));
	~Camera();

	static void renderAll();

	void setProjection(float fov, float aspectRatio, float nearPlane = 0.1f, float farPlane = 100.0f);
	void setTransform(const QVector3D& position, const QQuaternion& rotation);

	const QMatrix4x4& getProjection();
	QMatrix4x4 getView();

	void start() override;

	RenderFunction renderFunction; // cannot be set to nullptr
	Framebuffer* outputTarget; // set to nullptr for default framebuffer
	QVector4D backgroundColor;
	bool isReflectionEnabled;

private:
	static std::set<Camera*> allCameras;

	QMatrix4x4 projection;
	Transform* transform;	
};
