#pragma once
#include <QVector3D>
const QVector3D CubemapForwards[6] =
{
	QVector3D(1, 0, 0),
	QVector3D(-1, 0, 0),
	QVector3D(0, 1, 0),
	QVector3D(0, -1, 0),
	QVector3D(0, 0, 1),
	QVector3D(0, 0, -1)
};
const QVector3D CubemapUpwards[6] =
{
	QVector3D(0, -1, 0),
	QVector3D(0, -1, 0),
	QVector3D(0, 0, 1),
	QVector3D(0, 0, -1),
	QVector3D(0, -1, 0),
	QVector3D(0, -1, 0)
};