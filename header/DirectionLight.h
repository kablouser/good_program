#pragma once
#include "GenericLight.h"
#include "StandardRenderer.h"

class DirectionLight : public GenericLight<StandardRenderer::DirectionLight>
{
public:
	DirectionLight(
		SceneObject* parent,
		QVector3D color = QVector3D(1.0f,1.0f,1.0f));
	~DirectionLight();

	static bool initialize(
		const char* vertexShaderPath,
		const char* fragmentShaderPath,
		int shadowMapResolution = 1024,
		float mapBounds = 10.0f);
	static void updateShadowMaps(int viewportWidth, int viewportHeight);
	static void setShadowMapShader(const QMatrix4x4& modelToProjection);

	virtual void updateLightData() override;
	virtual void setEnabled(bool isEnabled) override;

protected:
	static std::set<DirectionLight*> allDirectionLights;
	static ShaderProgram shadowMapShader;
	static Framebuffer shadowMapFramebuffer;
	static int shadowMapResolution;
	static float mapBounds;
	static int modelToProjectionId;

	virtual void initializeLight() override;
	virtual bool hasLightChanged() override;
};
