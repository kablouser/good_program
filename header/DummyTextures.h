#pragma once
#include "Texture.h"
// GLSL requires all texture samplers to be bound to some valid texture unit when it runs.
// Even if the sampler will never be used.
// So you're not using a sampler you should bind it to some dummy texture.

class DummyTextures
{
public:
	static Texture* getDummyTexture()
	{
		if (texture.isValid() == false)
			texture.createDummy();
		return &texture;
	}
private:
	static Texture texture;
};
Texture DummyTextures::texture;