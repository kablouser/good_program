#pragma once
#include "Texture.h"
#include <vector>
class Framebuffer
{
public:
	Framebuffer();
	~Framebuffer();
	bool create(int width, int height);
	bool createDepthOnly(int width, int height);
	bool createDepthOnly3D(int widthPerFace, int heightPerFace);

	inline static void bindDefault()
	{
		OpenGLProfile::getCurrent()->glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}
	inline void bind()
	{
		OpenGLProfile::getCurrent()->glBindFramebuffer(GL_FRAMEBUFFER, frameBufferId);
	}
	inline Texture& getAttachment()
	{
		return attachment;
	}
	/// <summary>
	/// Requires framebuffer to be binded first.
	/// </summary>
	void attachExternalFace(Texture& cubemap, int face);
	void attachExternalDepth(Texture& depthMap);
	void attachExternalDepth3D(Texture& depthCubemap);

private:
	void createDepthStencil(int width, int height);

	GLuint frameBufferId, renderBufferObject;
	Texture attachment;
	bool is3D : 1;
};
