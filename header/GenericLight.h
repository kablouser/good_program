#pragma once
#include "SceneComponent.h"
#include "Transform.h"
#include "Texture.h"
template <typename T>
class GenericLight : public SceneComponent
{
public:
	GenericLight(SceneObject* parent) :
		SceneComponent(parent), transform(nullptr) {}

	T lightData;

	virtual void updateLightData() = 0;
	virtual void start() override
	{
		transform = getComponent<Transform>();
		if (transform == nullptr)
		{
			qCritical("Light : cannot find transform");
			return;
		}
		initializeLight();
		updateLightData();
	};
	virtual void update() override
	{
		if (hasLightChanged())
			updateLightData();
	};
	virtual void setEnabled(bool isEnabled) override = 0;

protected:
	Transform* transform;
	Texture shadowMap;

	virtual void initializeLight() = 0;
	virtual bool hasLightChanged() = 0;
};
