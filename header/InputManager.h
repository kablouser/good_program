#pragma once

#include <QWidget>

class InputManager
{
public:
	InputManager(QWidget* parent, bool isCursorLocked = false);

	void keyPressEvent(QKeyEvent* event);
	void keyReleaseEvent(QKeyEvent* event);

	void mousePressEvent(QMouseEvent* event);
	void mouseReleaseEvent(QMouseEvent* event);

	void update();

	// key e.g. Qt::Key_Escape or Qt:LeftButton
	bool isKeyPressed(int key);
	void setCursorLock(bool isLocked);
	void getMousePosition(int& x, int& y);
	void getMouseDelta(int& x, int& y);

private:
	static const int
		block1Start = Qt::Key_Escape,
		block1End = Qt::Key_Direction_R,

		block2Start = Qt::Key_Space,
		block2End = Qt::Key_AsciiTilde,

		block3Start = Qt::LeftButton,
		block3End = Qt::MidButton;

	bool block1Keys[block1End - block1Start + 1];
	bool block2Keys[block2End - block2Start + 1];
	bool block3Keys[block3End - block3Start + 1];

	bool isCursorLocked;
	int mousePositionX, mousePositionY; // screen position
	int mouseDeltaX, mouseDeltaY;

	QWidget* parent;

	void setKeyRecord(int key, bool newValue);
	bool* getKeyRecord(int key);
	QPoint convertToLocal(QPoint cursorPosition);
};
