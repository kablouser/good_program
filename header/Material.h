#pragma once
#include "Texture.h"
#include <QVector4D>
#include <QVector3D>
template <typename T>
union ColorData
{
	ColorData(Texture* texture) : texture(texture) {}
	ColorData(T singleColor) : singleColor(singleColor) {}
	Texture* texture;
	T singleColor;
};
template <typename T>
struct MaterialComponent
{
	bool isSingle;
	ColorData<T> data;
	MaterialComponent(Texture* texture) :
		isSingle(false), data(texture) {}
	MaterialComponent(T singleColor) :
		isSingle(true), data(singleColor) {}
};
struct Material
{
	MaterialComponent<QVector4D> diffuse;
	MaterialComponent<QVector3D> specular;
	MaterialComponent<QVector3D> emission;
	float shininess, alphaCutoff;
	bool isTransparent;
	Material() :
		diffuse(QVector4D(1, 1, 1, 1)),
		specular(QVector3D(0, 0, 0)),
		emission(QVector3D(0, 0, 0)),
		shininess(64.0f), alphaCutoff(0.0f), isTransparent(false) {}
};
