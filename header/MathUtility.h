#pragma once

#include <QtMath>
#include <QQuaternion>
#include <QVector3D>
#include <QMatrix3x3>

class MathUtility
{
public:
	static QQuaternion invertQuaternion(QQuaternion original)
	{
		//q  = w + xi + yj + zk
		//q` = w - xi - yj - zk / |q|^2

		float lengthSquared = original.lengthSquared();
		if (lengthSquared == 0.0f)
			return QQuaternion(0.0f, 0.0f, 0.0f, 0.0f);
		else
			return QQuaternion(
				original.scalar()/lengthSquared,
				-original.x() / lengthSquared,
				-original.y() / lengthSquared,
				-original.z() / lengthSquared);
	};

	static QQuaternion eulerToQuaternion(float pitch, float yaw, float roll)
	{
		pitch = qDegreesToRadians(pitch * 0.5);
		yaw = qDegreesToRadians(yaw * 0.5);
		roll = qDegreesToRadians(roll * 0.5);

		// roll degrees around the z axis, pitch degrees around the x axis, and yaw degrees around the y axis
		// Abbreviations for the various angular functions
		double cy = qCos(yaw);
		double sy = qSin(yaw);
		double cp = qCos(pitch);
		double sp = qSin(pitch);
		double cr = qCos(roll);
		double sr = qSin(roll);

		return QQuaternion(
			cr * cp * cy + sr * sp * sy,			
			cr * sp * cy + sr * cp * sy,
			cr * cp * sy - sr * sp * cy,
			sr * cp * cy - cr * sp * sy);
	}

	static QQuaternion eulerToQuaternion(QVector3D vector)
	{
		return eulerToQuaternion(vector.x(), vector.y(), vector.z());
	}

	static QQuaternion lookAt(QVector3D forward, QVector3D up)
	{
		if (qFuzzyIsNull(forward.x()) && qFuzzyIsNull(forward.y()) && qFuzzyIsNull(forward.z()))
			return QQuaternion();

		const QVector3D zAxis(forward.normalized());
		QVector3D xAxis(QVector3D::crossProduct(up, zAxis));
		if (qFuzzyIsNull(xAxis.lengthSquared())) {
			// collinear or invalid up vector; derive shortest arc to new direction
			const QVector3D v0(QVector3D(0.0f, 0.0f, 1.0f));
			const QVector3D v1(zAxis);

			float d = QVector3D::dotProduct(v0, v1) + 1.0f;

			// if dest vector is close to the inverse of source vector, ANY axis of rotation is valid
			if (qFuzzyIsNull(d)) {
				QVector3D axis = QVector3D::crossProduct(QVector3D(1.0f, 0.0f, 0.0f), v0);
				if (qFuzzyIsNull(axis.lengthSquared()))
					axis = QVector3D::crossProduct(QVector3D(0.0f, 1.0f, 0.0f), v0);
				axis.normalize();

				// same as QQuaternion::fromAxisAndAngle(axis, 180.0f)
				return QQuaternion(0.0f, axis.x(), axis.y(), axis.z());
			}

			d = std::sqrt(2.0f * d);
			const QVector3D axis(QVector3D::crossProduct(v0, v1) / d);

			return QQuaternion(d * 0.5f, axis).normalized();
		}

		xAxis.normalize();
		const QVector3D yAxis(QVector3D::crossProduct(zAxis, xAxis));

		QMatrix3x3 rot3x3;
		rot3x3(0, 0) = xAxis.x();
		rot3x3(1, 0) = xAxis.y();
		rot3x3(2, 0) = xAxis.z();
		rot3x3(0, 1) = yAxis.x();
		rot3x3(1, 1) = yAxis.y();
		rot3x3(2, 1) = yAxis.z();
		rot3x3(0, 2) = zAxis.x();
		rot3x3(1, 2) = zAxis.y();
		rot3x3(2, 2) = zAxis.z();

		float scalar;
		float axis[3];

		const float trace = rot3x3(0, 0) + rot3x3(1, 1) + rot3x3(2, 2);
		if (trace > 0.00000001f) {
			const float s = 2.0f * std::sqrt(trace + 1.0f);
			scalar = 0.25f * s;
			axis[0] = (rot3x3(2, 1) - rot3x3(1, 2)) / s;
			axis[1] = (rot3x3(0, 2) - rot3x3(2, 0)) / s;
			axis[2] = (rot3x3(1, 0) - rot3x3(0, 1)) / s;
		}
		else {
			static int s_next[3] = { 1, 2, 0 };
			int i = 0;
			if (rot3x3(1, 1) > rot3x3(0, 0))
				i = 1;
			if (rot3x3(2, 2) > rot3x3(i, i))
				i = 2;
			int j = s_next[i];
			int k = s_next[j];

			const float s = 2.0f * std::sqrt(rot3x3(i, i) - rot3x3(j, j) - rot3x3(k, k) + 1.0f);
			axis[i] = 0.25f * s;
			scalar = (rot3x3(k, j) - rot3x3(j, k)) / s;
			axis[j] = (rot3x3(j, i) + rot3x3(i, j)) / s;
			axis[k] = (rot3x3(k, i) + rot3x3(i, k)) / s;
		}

		return QQuaternion(scalar, axis[0], axis[1], axis[2]);
	}

	static QQuaternion inverseQuaternion(const QQuaternion& quaternion)
	{
		// Need some extra precision if the length is very small.
		double len = double(quaternion.scalar()) * double(quaternion.scalar()) +
			double(quaternion.x()) * double(quaternion.x()) +
			double(quaternion.y()) * double(quaternion.y()) +
			double(quaternion.z()) * double(quaternion.z());
		if (!qFuzzyIsNull(len))
			return QQuaternion(float(double(quaternion.scalar()) / len), float(double(-quaternion.x()) / len),
				float(double(-quaternion.y()) / len), float(double(-quaternion.z()) / len));
		return QQuaternion(0.0f, 0.0f, 0.0f, 0.0f);
	}
};
