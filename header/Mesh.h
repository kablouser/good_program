#pragma once
#include "Texture.h"
#include "ShaderProgram.h"
#include <vector>
#include <QVector3D>
#include <QVector2D>

class Mesh
{
public:
	struct Vertex
	{
		QVector3D position;
		QVector3D normal;
		QVector2D uv;

		Vertex() {}
		Vertex(QVector3D position, QVector3D normal, QVector2D uv) :
			position(position),
			normal(normal),
			uv(uv) {}
	};

	Mesh();
	~Mesh();

	void setAsCube(float scale = 1.0f);
	void setAsQuad(float scale = 1.0f, bool isDoubleSided = false);
	void setAsTriangle(float scale = 1.0f);

	// set the raw data
	void setData(
		GLsizeiptr verticesSize, const void* vertices,
		GLsizeiptr indicesSize, const void* indices,
			int attributeSize,			// sizeof the attribute array?
			const int* attributeSizes,	// how big is each individual attribute?
		const GLenum* attributeTypes,	// what is the type of each individual attribute?
		GLenum usage = GL_STATIC_DRAW	// the expected usage pattern of the data store {GL_STATIC_DRAW, GL_DYNAMIC_DRAW}
	);

	// set using the Vertex structure,
	// this in turn calls setData
	// so that you don't need to do that
	void setMesh(
		std::vector<Vertex> vertices,
		std::vector<unsigned int> indices
	);

	/// <summary>
	/// Specifies what kind of primitives to render.
	/// Symbolic constants GL_POINTS, GL_LINE_STRIP, GL_LINE_LOOP,
	/// GL_LINES, GL_LINE_STRIP_ADJACENCY, GL_LINES_ADJACENCY, GL_TRIANGLE_STRIP,
	/// GL_TRIANGLE_FAN, GL_TRIANGLES, GL_TRIANGLE_STRIP_ADJACENCY, GL_TRIANGLES_ADJACENCY
	/// and GL_PATCHES are accepted.
	/// </summary>
	void setDrawMode(GLenum drawMode);

	/// <summary>
	/// make sure a shader program is bounded before calling
	/// </summary>
	void draw();

	inline bool getIsDataSet() { return isDataSet; }

private:
	GLuint vertexBufferObject, vertexArrayObject, elementBufferObject;	
	GLsizei drawSize;
	GLenum drawMode;
	bool isDataSet : 1;
	bool useIndices : 1;

	int sizeofType(GLenum type);
	void scaleVertexPositions(float* vertices, int count, float scale);
};
