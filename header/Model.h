#pragma once
#include "ShaderProgram.h"
#include "Mesh.h"
#include "OpenGLProfile.h"
#include "Material.h"

#include <QString>
#include <vector>
#include <map>

class Model
{
public:
    ~Model();

    bool loadModel(const char* path, double scale = 1.0, bool flipUVs = false);
    inline int getMeshCount() { return meshMaterialPairs.size(); }
    /// <summary>
    /// Retrieve the mesh data. Storing the result in outMesh and outMaterial.
    /// Set outMesh or outMaterial to nullptr to not retrieve any data.
    /// </summary>
    void getMeshMaterial(
        int index,
        Mesh** outMesh, Material** outMaterial);

private:
    enum class FaceType { unknown, position, positionTexture, positionTextureNormal, positionNormal };
    struct MeshMaterialPair
    {
        Mesh* mesh;
        Material* material;
    };

    bool loadMtl(
        const QString& folder, const QString& mtlFile,
        std::map<QString, Material*>& materialMap);
    Texture* loadTextureMap(const QString& path);
    void addMeshMaterial(
        FaceType usingFaceType,
        std::vector<float>& verticesData,
        Material* currentMaterial);

    FaceType getFaceType(QString token);
    void clearData();

    // model data
    std::vector<MeshMaterialPair> meshMaterialPairs;
    std::vector<Texture*> textures;
};
