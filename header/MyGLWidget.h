#pragma once

#include <vector>
#include <QGLWidget>
#include <QOpenGLDebugLogger>
#include <QVector3D>
#include <QVector2D>

#include "InputManager.h"
#include "TimeManager.h"
#include "OpenGLProfile.h"
#include "Mesh.h"
#include "ShaderProgram.h"
#include "Texture.h"
#include "Transform.h"
#include "Camera.h"
#include "Model.h"

#include "StandardRenderer.h"
#include "DirectionLight.h"
#include "PointLight.h"
#include "SpotLight.h"
#include "UnlitRenderer.h"
#include "OutlineRenderer.h"
#include "PostProcessor.h"
#include "ReflectionProbe.h"

class MyGLWidget : public QGLWidget
{
public:
	OpenGLProfile* profile;

	MyGLWidget(QWidget* parent, InputManager& inputManager, TimeManager& timeManager);
	~MyGLWidget();

	// reflection controls
	void setReflection(bool isEnabled);
	void setReflectionUpdateFrequency(unsigned int probesPerFrame);

	// light controls
	void setDirectionLight(bool isEnabled);
	void setDirectionLightBrightness(float brightness);
	void setRedLight(bool isEnabled);
	void setRedLightBrightness(float brightness);
	void setGreenLight(bool isEnabled);
	void setGreenLightBrightness(float brightness);
	void setBlueLight(bool isEnabled);
	void setBlueLightBrightness(float brightness);

	void setSpotLight(bool isEnabled);
	void setSpotLightBrightness(float brightness);
	void setSpotLightAngle(float angle);
	void pointSpotLight();

	// outline controls
	void setOutline(bool isEnabled);
	void setOutlineScale(float scale);

	// reverse camera controls
	void setReverseCamera(bool isEnabled);

	// kernel controls
	void setKernel(bool isEnabled);
	void setKernelMatrix(int index);

protected:
	// called when OpenGL context is set up
	void initializeGL() override;
	// called every time the widget is resized
	void resizeGL(int w, int h) override;
	// called whenever OpenGL needs repainting
	void paintGL() override;

private:
	void initializeObjects();

	struct TransformObject
	{
		SceneObject sceneObject;
		Transform transform;
		TransformObject(QString name = "object") :
			sceneObject(name), transform(&sceneObject) { }
	};
	struct RenderObject
	{
		SceneObject sceneObject;
		Transform transform;
		StandardRenderer renderer;
		RenderObject(QString name = "object") :
			sceneObject(name), transform(&sceneObject), renderer(&sceneObject) { }
	};
	struct UnlitObject
	{
		SceneObject sceneObject;
		Transform transform;
		UnlitRenderer renderer;
		UnlitObject(QString name = "object") :
			sceneObject(name), transform(&sceneObject), renderer(&sceneObject) { }
	};
	struct OutlineObject
	{
		SceneObject sceneObject;
		Transform transform;
		OutlineRenderer renderer;
		OutlineObject(QString name = "object") :
			sceneObject(name), transform(&sceneObject), renderer(&sceneObject) { }
	};

	QVector2D cameraAngle;
	InputManager& inputManager;
	TimeManager& timeManager;
	QOpenGLDebugLogger logger;

	Scene scene;

	SceneObject cameraObject;
	Transform cameraTransform;
	Camera camera;

	SceneObject reverseCameraObject;
	Transform reverseCameraTransform;
	Camera reverseCamera;
	
	RenderObject* pointLights[3];
	PointLight* pointLightComponents[3];
	RenderObject spotLight;
	SpotLight spotLightComponent;
	TransformObject directionLight;
	DirectionLight directionLightComponent;

	ReflectionProbe 
		isosphereReflectionProbe,
		monitorReflectionProbe,
		weaponReflectionProbe,
		windowReflectionProbe1,
		windowReflectionProbe2;
	Framebuffer reverseFramebuffer;

	UnlitObject reverseMirror;
	RenderObject* windows[5];
	RenderObject
		floor,
		normalPhoto,
		bigPhoto;

	OutlineObject
		globe, globeStand, globeHolder,
		isosphere,
		monitor,
		person,
		shield,
		sword;

	const int outlineObjectCount = 8;
	OutlineObject* allOutlineObjects[8]
	{ &globe, &globeStand, &globeHolder, &isosphere, &monitor, &person, &shield, &sword };

	Material 
		floorMaterial, 
		normalPhotoMaterial,
		bigPhotoMaterial,
		windowMaterial,
		globeStandMaterial,
		isosphereMaterial,
		lightBulbRed,
		lightBulbGreen,
		lightBulbBlue;
	Texture
		windowTexture,
		windowSpecular,
		normalPhotoTexture,
		bigPhotoTexture,
		lightBulbEmissiveRed,
		lightBulbEmissiveGreen,
		lightBulbEmissiveBlue;
	Texture*
		spotLightEmissive;

	Mesh
		floorMesh,
		reverseMesh,
		windowMesh,
		normalPhotoMesh,
		bigPhotoMesh;
	Model
		globeModel, globeStandModel, globeHolderModel,
		isosphereModel,
		monitorModel,
		personModel,
		shieldModel,
		swordModel,
		lightBulbModel,
		spotLightModel;

	int kernelMode;
};
