#pragma once

#include <QWidget>
#include <QBoxLayout>
#include <QTimer>
#include <QPushButton>
#include <QGroupBox>
#include <QSlider>
#include <QLabel>
#include <QFormLayout>
#include <QCheckBox>
#include <QComboBox>

#include "MyGLWidget.h"
#include "InputManager.h"
#include "TimeManager.h"

class MyGLWindow : public QWidget
{
Q_OBJECT

public:
	InputManager inputManager;
	TimeManager timeManager;

	MyGLWindow();

public slots:
	void update();

protected:
	// input manager stuff
	void keyPressEvent(QKeyEvent* event) override;
	void keyReleaseEvent(QKeyEvent* event) override;
	void mousePressEvent(QMouseEvent* event) override;
	void mouseReleaseEvent(QMouseEvent* event) override;
	void focusInEvent(QFocusEvent* event) override;

private:
	MyGLWidget myGLWidget;
	QTimer updateTimer;

	// layout stuff
	QHBoxLayout horizontalLayout;
	QVBoxLayout verticalLayout;

	QLabel instructions;
	QLabel fpsLabel;

	// reflection controls
	QGroupBox reflectionGroupBox;
	QFormLayout reflectionLayout;

	QLabel reflectionCheckBoxLabel;
	QCheckBox reflectionCheckBox;
	QLabel reflectionSliderLabel;
	QSlider reflectionSlider;
	QHBoxLayout reflectionSliderLegend;
	QLabel reflectionSliderLegendLeft;
	QLabel reflectionSliderLegendRight;

	// light controls
	QGroupBox lightGroupBox;
	QFormLayout lightLayout;
	QHBoxLayout row1, row2, row3, row4, row5;
		
	QLabel directionLightLabel;
	QCheckBox directionLightCheckBox;
	QSlider directionLightSlider;
	QLabel redBulbLabel;
	QCheckBox redBulbCheckBox;
	QSlider redBulbSlider;
	QLabel greenBulbLabel;
	QCheckBox greenBulbCheckBox;
	QSlider greenBulbSlider;
	QLabel blueBulbLabel;
	QCheckBox blueBulbCheckBox;
	QSlider blueBulbSlider;

	QLabel spotLightLabel;
	QCheckBox spotLightCheckBox;
	QPushButton spotLightButton;
	QSlider spotLightVolumeSlider;

	QLabel spotLightRadiusLabel;
	QSlider spotLightRadiusSlider;

	QGroupBox bottomBox;
	QFormLayout bottomForm;

	// outline controls	
	QLabel outlineLabel;
	QHBoxLayout outlineRow;
	QCheckBox outlineCheckBox;
	QSlider outlineSlider;

	// reverse camera controls
	QLabel reverseCamLabel;
	QCheckBox reverseCamCheckBox;

	// kernel controls
	QLabel kernelLabel;
	QHBoxLayout kernelRow;
	QCheckBox kernelCheckBox;
	QComboBox kernelComboBox;
};