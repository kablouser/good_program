#pragma once

#include <QOpenGLFunctions_3_3_Core>

#define OpenGLProfile_Major 3
#define OpenGLProfile_Minor 3
#define OpenGLProfile_Profile QSurfaceFormat::CoreProfile

class OpenGLProfile : public QOpenGLFunctions_3_3_Core
{
public:
	OpenGLProfile();
	~OpenGLProfile();
	inline static OpenGLProfile* getCurrent()
	{
		return current;
	}

private:
	static OpenGLProfile* current;
};
