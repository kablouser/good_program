#pragma once
#include "StandardRenderer.h"
class OutlineRenderer : public StandardRenderer
{
public:
	OutlineRenderer(SceneObject* parent,
		Mesh* mesh = nullptr, Material* material = nullptr,
		QVector3D color = QVector3D(1.0f,1.0f,1.0f), bool scaleAlongNormals = false, float scale = 1.05f);

	bool outlineEnabled;
	QVector3D outlineColor;
	bool outlineAlongNormals; // if true, outline will scale along normals. otherwise scales from model origin.
	float outlineScale; // scale and size of the outline

protected:
	virtual void renderActual(const QMatrix4x4& projection, const QMatrix4x4& view, bool isReflectionEnabled) override;
};
