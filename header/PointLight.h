#pragma once
#include "StandardRenderer.h"
#include "GenericLight.h"
class PointLight : public GenericLight<StandardRenderer::PointLight>
{
public:
	PointLight(
		SceneObject* parent,
		QVector3D color = QVector3D(1.0f,1.0f,1.0f));
	~PointLight();

	static bool initialize(
		const char* vertexShaderPath,
		const char* geometryShaderPath,
		const char* fragmentShaderPath,
		int shadowCubemapResolution = 1024,
		float nearPlane = 0.1f,
		float farPlane = 10.0f);
	static void updateShadowMaps(int viewportWidth, int viewportHeight);

	virtual void updateLightData() override;
	virtual void setEnabled(bool isEnabled) override;

protected:
	static std::set<PointLight*> allPointLights;
	static ShaderProgram shadowCubemapShader;
	static Framebuffer shadowCubemapFramebuffer;
	static int shadowCubemapResolution;
	static float nearPlane, farPlane;
	static int modelId, lightPositionId, farPlaneId;
	static int viewToProjectionIds[6];

	virtual void initializeLight() override;
	virtual bool hasLightChanged() override;
};
