#pragma once
#include "Mesh.h"
#include "ShaderProgram.h"
#include "Framebuffer.h"
#include "Camera.h"
/// <summary>
/// static class, don't create an instance of this
/// </summary>
class PostProcessor
{
public:
	static bool initialize(
		const char* vertexShaderPath, const char* fragmentShaderPath,
		float offsetPixels = 1.0f,
		QMatrix3x3 kernel = QMatrix3x3(defaultKernel),
		QVector3D baseColor = QVector3D());
	static void renderAll(
		const QMatrix4x4& projection,
		const QMatrix4x4& view,
		const QVector3D& cameraPosition,
		Framebuffer* outputTarget = nullptr, QVector4D backgroundColor = QVector4D(0.3f, 0.3f, 0.3f, 1.0f),
		bool isReflectionEnabled = true);
	static bool viewportUpdate(int screenWidth, int screenHeight);

	static float offsetPixels;
	static QMatrix3x3 kernel;
	static QVector3D baseColor;

private:
	PostProcessor();

	static float defaultKernel[9];

	static ShaderProgram shaderProgram;
	static Framebuffer framebuffer;
	static Mesh overlayMesh;
	static int
		screenTextureId,
		offsetPixelsId,
		kernelId,
		baseColorId;
};
