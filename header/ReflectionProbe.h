#pragma once
#include "Texture.h"
#include "Framebuffer.h"
#include <QVector3D>
#include <set>
class ReflectionProbe
{
public:
	static unsigned int probesPerFrame;

	static bool initialize(int probeResolution);
	static void updateProbes(int viewportWidth, int viewportHeight);
	static void setEnvironmentCubemap(const QVector3D& position);

	ReflectionProbe(QVector3D position = QVector3D());
	~ReflectionProbe();

	QVector3D position;

private:
	static std::set<ReflectionProbe*> allReflectionProbes;
	static int updateIterator,	probeResolution;
	static Framebuffer framebuffer;
		
	Texture cubemap;	
};
