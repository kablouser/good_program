#pragma once

#include <set>
#include <functional>

#include "SceneComponent.h"
#include "Transform.h"
#include "Framebuffer.h"

class Renderer : public SceneComponent
{
public:
	~Renderer();

	/// <param name="framebuffer">draw destination. set to nullptr for default framebuffer.</param>
	static void renderAll(
		const QMatrix4x4& projection,
		const QMatrix4x4& view,
		const QVector3D& cameraPosition,
		Framebuffer* outputTarget = nullptr,
		QVector4D backgroundColor = QVector4D(0.3f,0.3f,0.3f,1.0f),
		bool isReflectionEnabled = true);
	static void foreachRenderer(std::function<void(Renderer*)> const& action);

	virtual void start() override;
	virtual void render(const QMatrix4x4& projection, const QMatrix4x4& view, bool isReflectionEnabled) = 0;
	virtual void draw() = 0;

	QMatrix4x4 getModelMatrix() const;

protected:
	Renderer(SceneObject* parent);
	Transform* transform;

	static std::set<Renderer*> allRenderers;
};
