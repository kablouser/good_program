#pragma once

#include <vector>
#include <set>

class SceneObject;
class SceneComponent;

class Scene
{
public:
	Scene();

	static Scene* getCurrentScene();

	void addComponent(SceneComponent* component);
	// components can be set to ended, which is another way of removing components
	bool removeComponent(SceneComponent* component);

	void addRootObject(SceneObject* rootObject);
	bool removeRootObject(SceneObject* rootObject);

	int getRootObjectsSize();
	std::set<SceneObject*>::iterator getRootObjectsIterator();
	std::set<SceneObject*>::iterator getRootObjectsEnd();

	void start();
	void update();

private:
	static Scene* current;
	static bool compareComponentOrder(SceneComponent* componentA, SceneComponent* componentB);

	bool started = false;
	std::vector<SceneComponent*> unsortedAllComponents;
	// sorted set of components that can be executed
	std::multiset<SceneComponent*, decltype(&compareComponentOrder)> allComponents;
	std::set<SceneObject*> rootObjects;
};

#include "SceneObject.h"
#include "SceneComponent.h"
