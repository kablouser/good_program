#pragma once

#include <functional>

class Scene;
class SceneObject;

// this class is abstract, you should derive from it publicly
class SceneComponent
{
public:	
	SceneComponent(const SceneComponent&) = delete;

protected:
	/// <param name="parent">set parent object</param>
	/// <param name="isHeaped">this object will be deleted when ended</param>
	SceneComponent(SceneObject* parent);
	/// <param name="parent">set parent object</param>
	/// <param name="isHeaped">this object will be deleted when ended</param>
	/// <param name="scene">scene that this component belongs to</param>
	SceneComponent(SceneObject* parent, Scene* scene);

public:
	SceneObject* getAttachedObject();
	void setAttachedObject(SceneObject* sceneObject);

	bool getEnded();
	void setEnded(bool isEnded);

	bool getEnabled();
	virtual void setEnabled(bool isEnabled);

	bool getStarted();
	void setStarted(bool isStarted);

	/// <summary>
	/// Only executable when enabled and parent is inherited enabled
	/// </summary>
	bool getExecutable();
		
	virtual void start() {};
	virtual void update() {};
	virtual void onParent(SceneObject* oldParent) {};

	virtual int getExecutionOrder() const;
	bool operator<(const SceneComponent& other);

	template<typename T>
	T* getComponent();
	template<typename T>
	T* getParentComponent();
	template<typename T>
	void foreachChildComponent(std::function<void(T*)> const& action);
private:
	SceneObject* attachedObject = nullptr;

	// bit fields to reduce memory slightly

	bool isStarted : 1;
	bool isEnded : 1;
	bool isEnabled : 1; // the enabled state of this component	
};

#include "Scene.h"
#include "SceneObject.h"

template<typename T>
T* SceneComponent::getComponent()
{
	SceneObject* attachedObject = getAttachedObject();
	if (attachedObject != nullptr)
	{
		return attachedObject->getComponent<T>();
	}
	else
	{
		return nullptr;
	}
}

template<typename T>
T* SceneComponent::getParentComponent()
{
	SceneObject* attachedObject = getAttachedObject();
	if (attachedObject != nullptr)
	{
		return attachedObject->getParentComponent<T>();
	}
	else
	{
		return nullptr;
	}
};

template<typename T>
void SceneComponent::foreachChildComponent(std::function<void(T*)> const& action)
{
	SceneObject* attachedObject = getAttachedObject();
	if (attachedObject != nullptr)
		attachedObject->foreachChildComponent<T>(action);
};