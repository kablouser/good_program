#pragma once

#include <QString>
#include <set>

#include <functional>

class Scene;
class SceneComponent;

class SceneObject
{
public:
	SceneObject(QString name = "object", SceneObject* parent = nullptr);
	SceneObject(QString name, SceneObject* parent, Scene* scene);

	QString name;

	inline SceneObject* getParent() { return parent; }
	void setParent(SceneObject* newParent);
	
	inline int getChildrenSize() {	return children.size(); }
	inline std::set<SceneObject*>::iterator getChildrenIterator() { return children.begin(); }
	inline std::set<SceneObject*>::iterator getChildrenEnd() {	return children.end(); }
		
	void addComponent(SceneComponent* component);	
	// removes the component if found within its list
	bool removeComponent(SceneComponent* component);

	bool getSelfEnabled();
	void setSelfEnabled(bool isSelfEnabled);
	bool getEnabled();

	template<typename T>
	T* getComponent();

	// removes the first component of type T
	template<typename T>
	bool removeComponent();

	template<typename T>
	T* getParentComponent();

	template<typename T>
	void foreachChildComponent(std::function<void(T*)> const& action);

private:
	Scene* scene = nullptr;
	SceneObject* parent = nullptr;
	std::set<SceneObject*> children;
	std::set<SceneComponent*> components;

	bool isSelfEnabled : 1;
	bool isInheritedEnabled : 1;
	bool isEnabled : 1;
	
	void setInheritedEnabled(bool isInheritedEnabled);
	void updateEnabled();
};

#include "Scene.h"
#include "SceneComponent.h"

template<typename T>
T* SceneObject::getComponent()
{
	for (auto i = components.begin(); i != components.end(); ++i)
		if (T* component = dynamic_cast<T*>(*i))
			return component;
	return nullptr;
};

template<typename T>
bool SceneObject::removeComponent()
{
	for (auto i = components.begin(); i != components.end(); ++i)
		if (T* component = dynamic_cast<T*>(*i))
		{
			// this will call removeComponent(*i) which will update the list
			(*i)->setAttachedObject(nullptr);
			return true;
		}
	return false;
};

template<typename T>
T* SceneObject::getParentComponent()
{
	SceneObject* parent = getParent();
	if (parent != nullptr)
	{
		return parent->getComponent<T>();
	}
	else
	{
		return nullptr;
	}
}

template<typename T>
void SceneObject::foreachChildComponent(std::function<void(T*)> const& action)
{
	for (auto i = children.begin(); i != children.end(); ++i)
	{
		T* childComponent = (*i)->getComponent<T>();
		if (childComponent != nullptr)
		{
			action(childComponent);
		}
	}
}
