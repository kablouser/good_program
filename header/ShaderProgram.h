#pragma once
#include "Texture.h"

#include <QMatrix4x4>
#include <QVector2D>

class ShaderProgram
{
public:
	ShaderProgram();
	~ShaderProgram();

	void use();
	bool compileFiles(const char* vertexShaderPath, const char* fragmentShaderPath);
	bool compileFiles(const char* vertexShaderPath, const char* geometryShaderPath, const char* fragmentShaderPath);
	inline bool isValid() {	return id != 0;	}

	inline void setUniformFloat(int id, float x)
	{
		OpenGLProfile::getCurrent()->glUniform1f(id, x);
	}
	inline void setUniform2f(int id, const QVector2D& vector2)
	{
		OpenGLProfile::getCurrent()->glUniform2f(id, vector2.x(), vector2.y());
	}
	inline void setUniform3f(int id, const QVector3D& vector3)
	{
		OpenGLProfile::getCurrent()->glUniform3f(id, vector3.x(), vector3.y(), vector3.z());
	}
	inline void setUniform4f(int id, const QVector4D& vector4)
	{
		OpenGLProfile::getCurrent()->glUniform4f(id, vector4.x(), vector4.y(), vector4.z(), vector4.w());
	}

	inline void setUniformInt(int id, int x)
	{
		OpenGLProfile::getCurrent()->glUniform1i(id, x);
	}
	inline void setUniform2i(int id, int x, int y)
	{
		OpenGLProfile::getCurrent()->glUniform2i(id, x, y);
	}
	inline void setUniform3i(int id, int x, int y, int z)
	{
		OpenGLProfile::getCurrent()->glUniform3i(id, x, y, z);
	}
	inline void setUniform4i(int id, int x, int y, int z, int w)
	{
		OpenGLProfile::getCurrent()->glUniform4i(id, x, y, z, w);
	}

	inline void setUniformMatrix(int id, const QMatrix2x2& matrix)
	{
		OpenGLProfile::getCurrent()->glUniformMatrix2fv(id, 1, GL_FALSE, matrix.constData());
	}
	inline void setUniformMatrix(int id, const QMatrix3x3& matrix)
	{
		OpenGLProfile::getCurrent()->glUniformMatrix3fv(id, 1, GL_FALSE, matrix.constData());
	}
	inline void setUniformMatrix(int id, const QMatrix4x4& matrix)
	{
		OpenGLProfile::getCurrent()->glUniformMatrix4fv(id, 1, GL_FALSE, matrix.constData());
	}

	void setTextureUnit(Texture* texture, int uniformId, unsigned int textureUnit);
	void setTextureUnit3D(Texture* cubemap, int uniformId, unsigned int textureUnit);

	bool checkUniformId(const char* name, int& outId);

private:
	GLuint id;

	bool readFile(const char* path, const char* shaderName, std::string& outString);
	bool compileStrings(const char** vertexShaderSource, const char** fragmentShaderSource);
	bool compileStrings(const char** vertexShaderSource, const char** geometryShaderSource, const char** fragmentShaderSource);
	unsigned int compileShader(const char** sourceCode, GLenum shaderType);	
};
