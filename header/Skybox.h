#pragma once
#include "Texture.h"
#include "ShaderProgram.h"
#include "Mesh.h"
#include <QMatrix4x4>
class Skybox
{
public:
	///<param name="cubemapSidePaths">in order of : right, left, top, bottom, back, front</param>
	static bool initialize(
		const char* vertexShaderPath,
		const char* fragmentShaderPath,
		const char* cubemapSidePaths[6]);

	static void render(const QMatrix4x4& projection, const QMatrix4x4& view);
	static Texture& getCubemap();

private:
	Skybox();
		
	static bool isEnabled;
	static ShaderProgram shaderProgram;
	static int viewToProjectionId, cubemapId;
	
	static Mesh cube;
	static Texture cubemap;
};
