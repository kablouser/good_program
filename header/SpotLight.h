#pragma once
#include "GenericLight.h"
#include "StandardRenderer.h"
class SpotLight : public GenericLight<StandardRenderer::SpotLight>
{
public:
	SpotLight(SceneObject* parent, QVector3D color = QVector3D(1.0f,1.0f,1.0f), float angle = 20.0f);
	~SpotLight();

	static bool initialize(int shadowMapResolution = 256, float nearPlane = 0.1f, float farPlane = 10.0f);
	static void updateShadowMaps(int viewportWidth, int viewportHeight);

	virtual void updateLightData() override;
	virtual void setEnabled(bool isEnabled) override;

protected:
	static std::set<SpotLight*> allSpotLights;
	static Framebuffer shadowMapFramebuffer;
	static int shadowMapResolution;
	static float nearPlane, farPlane;

	virtual void initializeLight() override;
	virtual bool hasLightChanged() override;
};
