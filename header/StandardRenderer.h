#pragma once
#include "Renderer.h"
#include "ShaderProgram.h"
#include "Texture.h"
#include "Mesh.h"
#include "Material.h"

class StandardRenderer : public Renderer
{
public:
	template <typename T>
	struct LightComponents
	{
		T ambient, diffuse, specular;
	};
	template <typename T>
	struct Scaling
	{
		T constant, linear, quadratic;
	};
	struct DirectionLight
	{
		QVector3D direction;
		LightComponents<QVector3D> components;
		Texture* shadowMap;
		QMatrix4x4 lightSpace; // light projection * light view
	};
	struct PointLight
	{
		QVector3D position;
		LightComponents<QVector3D> components;
		Scaling<float> scaling;
		Texture* shadowMap; // cubemap
	};
	struct SpotLight
	{
		QVector3D position;
		QVector3D direction;
		LightComponents<QVector3D> components;
		Scaling<float> scaling;
		float cutOff;  // cosine of the inner spotlight angle
		float outerCutoff; // cosine of the outer spotlight angle
		Texture* shadowMap;
		QMatrix4x4 lightSpace; // light projection * light view
	};

	StandardRenderer(
		SceneObject* parent, Mesh* mesh = nullptr, Material* material = nullptr);

	static bool initialize(const char* vertexShaderPath, const char* fragmentShaderPath);
	static void renderAllTransparent(
		const QMatrix4x4& projection, 
		const QMatrix4x4& view, 
		const QVector3D& cameraPosition, 
		bool isReflectionsEnabled);

	virtual void render(const QMatrix4x4& projection, const QMatrix4x4& view, bool isReflectionEnabled) override;
	inline virtual void draw() override
	{
		mesh->draw();
	}

	static bool setDirectionLight(DirectionLight* directionLight);
	static bool removeDirectionLight(DirectionLight* directionLight);

	static bool setPointLight(PointLight* pointLight);
	static bool removePointLight(PointLight* pointLight);

	static bool setSpotLight(SpotLight* spotLight);
	static bool removeSpotLight(SpotLight* spotLight);

	static void setEnvironmentCubemap(Texture* cubemap);
	static void setReflections(bool isEnabled);

	static void setPointLightFarPlane(float farPlane);

	Mesh* mesh;
	Material* material;

protected:
	static ShaderProgram shaderProgram;

	virtual void renderActual(const QMatrix4x4& projection, const QMatrix4x4& view, bool isReflectionEnabled);

private:
	struct DirectionLightIds
	{
		int direction;
		LightComponents<int> components;
		int shadowMap;
		int lightSpace; // light projection * light view
	};
	struct PointLightIds
	{
		int position;
		LightComponents<int> components;
		Scaling<int> scaling;
		int shadowMap;
	};
	struct SpotLightIds
	{
		int position;
		int direction;
		LightComponents<int> components;
		Scaling<int> scaling;
		int cutOff, outerCutoff;
		int shadowMap;
		int lightSpace; // light projection * light view
	};

	static std::vector<DirectionLight*> allDirectionLights;
	static std::vector<PointLight*> allPointLights;
	static std::vector<SpotLight*> allSpotLights;
	static int
		model,
		modelToProjection,
		modelToView,
		directionModelToView,
		worldToView,
		directionWorldToView,
		isDiffuseSingle, singleDiffuse, diffuse,
		isSpecularSingle, singleSpecular, specular,
		isEmissionSingle, singleEmission, emission,
		shininess, alphaCutoff,
		directionLightsEnabled,
		pointLightsEnabled,
		spotLightsEnabled,
		isReflectionEnabled,
		environmentCubemap,
		pointLightFarPlane;
	static DirectionLightIds directionLightIds[];
	static PointLightIds pointLightIds[];
	static SpotLightIds spotLightIds[];
	static std::vector<StandardRenderer*> transparentRenderers;
	
	static bool initializeLightIds();
	
	static void setDirectionLightIndex(int index, DirectionLight* directionLight);
	static void setPointLightIndex(int index, PointLight* pointLight);
	static void setSpotLightIndex(int index, SpotLight* spotLight);
};
