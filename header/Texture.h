#pragma once
#include "OpenGLProfile.h"

class Texture
{
public:
	Texture();
	~Texture();

	bool create(
		const char* imagePath,
		GLint wrapMode = GL_CLAMP_TO_EDGE,
		GLint textureFilter = GL_LINEAR,
		GLint mipmapFilter = GL_LINEAR_MIPMAP_LINEAR);

	/// <param name="cubemapSidePaths">in order of : right, left, top, bottom, back, front</param>
	bool create3D(
		const char* cubemapSidePaths[6],
		GLint wrapMode = GL_CLAMP_TO_EDGE,
		GLint textureFilter = GL_LINEAR,
		GLint mipmapFilter = GL_LINEAR);

	/// <param name="format">GL_DEPTH_COMPONENT or GL_RGB or GL_RGBA</param>
	void createRaw(
		int width, int height,
		GLint wrapMode = GL_CLAMP_TO_EDGE,
		GLint textureFilter = GL_LINEAR,
		GLint mipmapFilter = GL_LINEAR,
		GLint format = GL_RGBA);

	/// <param name="format">GL_DEPTH_COMPONENT or GL_RGBA</param>
	void createRaw3D(
		int widthPerFace, int heightPerFace,
		GLint wrapMode = GL_CLAMP_TO_EDGE,
		GLint textureFilter = GL_LINEAR,
		GLint mipmapFilter = GL_LINEAR,
		GLint format = GL_RGBA);

	void createDummy();
		
	void bind();
	void bind3D();
	inline bool isValid() { return id != -1; }

	inline GLuint getId()
	{
		return id;
	}

private:
	GLuint id;

	GLint getDataType(GLint format);
};
