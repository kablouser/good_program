#pragma once
#include <QElapsedTimer>

class TimeManager
{
public:
	float time;		 // seconds since window start
	float deltaTime; // seconds since last update

	TimeManager();
	void update();

private:
	QElapsedTimer programTimer;
};
