#pragma once
#include "SceneComponent.h"

#include <functional>

#include <QVector3D>
#include <QQuaternion>

// represents a transformed object in world space
class Transform : public SceneComponent
{
public:
	Transform(SceneObject* parent, QVector3D position = QVector3D(), QQuaternion rotation = QQuaternion());

	/// <summary>
	/// if true then during re-parenting world position and rotation stays the same.
	/// if false local position and rotation stays the same
	/// </summary>
	bool isWorldAnchored : 1;

	QVector3D getFlatForward() const; // normalized forward vector with y = 0
	QVector3D getForward() const;
	QVector3D getRight() const;
	QVector3D getUp() const;

	const QVector3D& getLocalPosition() const;
	const QQuaternion& getLocalRotation() const;

	void setLocalPosition(const QVector3D& localPosition);
	void setLocalRotation(const QQuaternion& localRotation);

	/// <summary>
	/// get world position
	/// </summary>
	QVector3D getPosition() const;
	/// <summary>
	/// get world rotation
	/// </summary>
	QQuaternion getRotation() const;

	void setPosition(const QVector3D& position);
	void setRotation(const QQuaternion& rotation);

	void onParent(SceneObject* oldParent) override;

private:
	QVector3D localPosition;
	QQuaternion localRotation;

	QVector3D parentWorldPosition;
	QQuaternion parentWorldRotation;
};
