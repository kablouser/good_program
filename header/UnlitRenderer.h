#pragma once
#include "Renderer.h"
#include "StandardRenderer.h"
class UnlitRenderer : public Renderer
{
public:
	UnlitRenderer(SceneObject* parent,
		Mesh* mesh = nullptr, QVector3D color = QVector3D(1.0f,1.0f,1.0f));
	UnlitRenderer(SceneObject* parent,
		Mesh* mesh, Texture* texture);

	static bool initialize(const char* vertexShaderPath, const char* fragmentShaderPath);
	static void setShaderParameters(
		const QMatrix4x4& projection,
		const QMatrix4x4& view,
		Transform* transform,
		bool isScaleEnabled,
		bool scaleAlongNormals,
		float scale,
		const MaterialComponent<QVector3D>& unlitData,
		bool isReflectionEnabled);

	virtual void render(const QMatrix4x4& projection, const QMatrix4x4& view, bool isReflectionEnabled) override;
	inline virtual void draw() override
	{
		mesh->draw();
	}

	Mesh* mesh;
	MaterialComponent<QVector3D> unlitData;

private:
	static ShaderProgram unlitShader;
	static int modelToProjection,
		isScaleEnabled,
		scaleAlongNormals,
		scale,
		isTextureEnabled,
		texture0,
		color;
};
