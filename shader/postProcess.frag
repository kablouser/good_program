#version 330 core
out vec4 FragColor;
  
in vec2 TexCoords;

uniform sampler2D screenTexture;
uniform float offsetPixels = 1.0;
uniform mat3 kernel = mat3(
    0.0, 0.0, 0.0,
    0.0, 1.0, 0.0,
    0.0, 0.0, 0.0);
uniform vec3 baseColor = vec3(0.0);

void main()
{
    ivec2 screenSize = textureSize(screenTexture, 0);
    float aspectRatio = screenSize.x / screenSize.y;
    float offset = offsetPixels / screenSize.y;

    vec2 offsets[9] = vec2[](
        vec2(-offset * aspectRatio,  offset), // top-left
        vec2( 0.0f,    offset), // top-center
        vec2( offset * aspectRatio,  offset), // top-right
        vec2(-offset * aspectRatio,  0.0f),   // center-left
        vec2( 0.0f,    0.0f),   // center-center
        vec2( offset * aspectRatio,  0.0f),   // center-right
        vec2(-offset * aspectRatio, -offset), // bottom-left
        vec2( 0.0f,   -offset), // bottom-center
        vec2( offset * aspectRatio, -offset)  // bottom-right    
    );
    
    vec3 sampleTex[9];
    for(int i = 0; i < 9; ++i)
        sampleTex[i] = vec3(texture(screenTexture, TexCoords.st + offsets[i]));

    vec3 color = baseColor;
    int i = 0;
    for(int x = 0; x < 3; ++x)
        for(int y = 0; y < 3; ++y)
        {
            color += sampleTex[i] * kernel[y][x];
            ++i;
        }
    
    FragColor = vec4(color, 1.0);
}  