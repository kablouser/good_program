#version 330 core

struct DirectionLight
{
    vec3 direction;
  
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;

    sampler2D shadowMap;
    mat4 lightSpace; // light projection * light view
};

struct PointLight
{
    vec3 position;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;

    float constant;
    float linear;
    float quadratic;

    samplerCube shadowMap;        
};

struct SpotLight
{
    vec3 position;
    vec3 direction;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;

    float constant;
    float linear;
    float quadratic;

    float cutOff;
    float outerCutoff;

    sampler2D shadowMap;
    mat4 lightSpace; // light projection * light view
};

struct Material
{
    sampler2D diffuse[1];
    sampler2D specular[1];
    sampler2D emission[1];

    bool isDiffuseSingle;
    bool isSpecularSingle;
    bool isEmissionSingle;
    
    vec4 singleDiffuse;
    vec3 singleSpecular;
    vec3 singleEmission;

    float shininess;
    float alphaCutoff;
};

in vec3 Normal;
in vec3 FragmentPosition;
in vec2 UV;
in vec3 VertexPosition;

uniform mat4 model;
uniform mat4 worldToView; // view
uniform mat4 directionWorldToView; // transpose(inverse(view))

uniform Material material;

uniform DirectionLight directionLights[5];
uniform PointLight pointLights[5];
uniform SpotLight spotLights[5];

uniform int directionLightsEnabled = 0;
uniform int pointLightsEnabled = 0;
uniform int spotLightsEnabled = 0;

// reflections
uniform bool isReflectionEnabled = false;
uniform samplerCube environmentCubemap;

uniform float pointLightFarPlane;

out vec4 FragColor; 

// returns colors
vec3 ComputeDirectionLight(DirectionLight light, vec3 normal, vec3 viewDirection, vec3 materialColor, vec3 specularColor);
vec3 ComputePointLight(PointLight light, vec3 normal, vec3 viewDirection, vec3 materialColor, vec3 specularColor, vec3 worldPosition);
vec3 ComputeSpotLight(SpotLight light, vec3 normal, vec3 viewDirection, vec3 materialColor, vec3 specularColor);
vec3 ComputePhong(vec3 normal, vec3 lightDirection, vec3 viewDirection, vec3 ambient, vec3 diffuse, vec3 specular, float shadowFactor);

float ComputeShadowMap(vec3 lightViewDirection, sampler2D shadowMap, mat4 lightSpace, vec3 normal);
float ComputeShadowCubemap(vec3 worldPosition, vec3 lightPosition, samplerCube shadowCubemap, vec3 normal);

void main()
{
    vec4 fullDiffuse;
    if(material.isDiffuseSingle)
        fullDiffuse = material.singleDiffuse;
    else
        fullDiffuse = texture(material.diffuse[0], UV);

    if(fullDiffuse.a <= material.alphaCutoff)
        discard;

    vec3 materialColor = vec3(fullDiffuse);

    vec3 normal = normalize(Normal);
    vec3 viewDirection = normalize(-FragmentPosition);
    vec3 worldPosition = vec3(model * vec4(VertexPosition, 1.0));

    vec3 specularColor;
    if(material.isSpecularSingle)
        specularColor = material.singleSpecular;
    else
        specularColor = vec3(texture(material.specular[0], UV));
    
    vec3 totalColor = vec3(0.0);
    int i;
    for(i = 0; i < directionLights.length() && i < directionLightsEnabled; ++i)
        totalColor += ComputeDirectionLight(directionLights[i], normal, viewDirection, materialColor, specularColor);
    for(i = 0; i < pointLights.length() && i < pointLightsEnabled; ++i)
        totalColor += ComputePointLight(pointLights[i], normal, viewDirection, materialColor, specularColor, worldPosition);
    for(i = 0; i < spotLights.length() && i < spotLightsEnabled; ++i)
        totalColor += ComputeSpotLight(spotLights[i], normal, viewDirection, materialColor, specularColor);
    if(isReflectionEnabled && 0.0 < dot(specularColor, specularColor))
    {
        vec3 I      = -viewDirection;
        vec3 viewR  = reflect(I, normal);
        vec3 worldR = inverse(mat3(worldToView)) * viewR;
        totalColor += specularColor * vec3(texture(environmentCubemap, worldR));
    }

    if(material.isEmissionSingle)
        totalColor += material.singleEmission;
    else
        totalColor += vec3(texture(material.emission[0], UV));
    
    FragColor = vec4(totalColor, fullDiffuse.a);
}

// returns color
vec3 ComputeDirectionLight(DirectionLight light, vec3 normal, vec3 viewDirection, vec3 materialColor, vec3 specularColor)
{
    vec3 lightViewDirection = normalize(mat3(directionWorldToView) * -light.direction);

    return ComputePhong(
        normal,
        lightViewDirection,
        viewDirection,
        light.ambient * materialColor,
        light.diffuse * materialColor,
        light.specular * specularColor,
        ComputeShadowMap(lightViewDirection, light.shadowMap, light.lightSpace, normal));
}

// returns color
vec3 ComputePointLight(PointLight light, vec3 normal, vec3 viewDirection, vec3 materialColor, vec3 specularColor, vec3 worldPosition)
{
    vec3 fragmentToLight = vec3(worldToView * vec4(light.position, 1.0)) - FragmentPosition;
    float distance = length(fragmentToLight);

    return ComputePhong(
        normal,
        normalize(fragmentToLight),
        viewDirection,
        light.ambient * materialColor,
        light.diffuse * materialColor,
        light.specular * specularColor,
        ComputeShadowCubemap(worldPosition, light.position, light.shadowMap, normal))
        // attenuation
        / (light.constant +
        light.linear * distance +
        light.quadratic * distance * distance);
}

// returns color
vec3 ComputeSpotLight(SpotLight light, vec3 normal, vec3 viewDirection, vec3 materialColor, vec3 specularColor)
{
    vec3 fragmentToLight = vec3(worldToView * vec4(light.position, 1.0)) - FragmentPosition;
    vec3 fragmentToLightUnit = normalize(fragmentToLight);
    vec3 inwardLightDirection = normalize(mat3(directionWorldToView) * -light.direction);

    float intensity = clamp(
        (dot(fragmentToLightUnit, inwardLightDirection) - light.outerCutoff) /
        (light.cutOff - light.outerCutoff),
        0.0, 1.0);

    if(0.0 < intensity)
    {
        float distance = length(fragmentToLight);
        return ComputePhong(
            normal,
            fragmentToLightUnit,
            viewDirection,
            light.ambient * materialColor,
            light.diffuse * materialColor,
            light.specular * specularColor,
            ComputeShadowMap(fragmentToLightUnit, light.shadowMap, light.lightSpace, normal))
            //attenuation
            / (light.constant +
            light.linear * distance +
            light.quadratic * distance * distance)
            //intensity
            * intensity;
    }
    else
        return vec3(0.0, 0.0, 0.0);
}

// returns color
vec3 ComputePhong(vec3 normal, vec3 lightDirection, vec3 viewDirection, vec3 ambient, vec3 diffuse, vec3 specular, float shadowFactor)
{
    // diffuse shading
    float diffuseScale = max(dot(normal, lightDirection), 0.0);

    // Blinn specular shading
    // vec3 reflectDirection = reflect(-lightDirection, normal);
    // float specularScale = pow(max(dot(viewDirection, reflectDirection), 0.0), material.shininess);

    // Blinn-Phong
    vec3 halfwayDirection = normalize(lightDirection + viewDirection);
    float specularScale = pow(max(dot(halfwayDirection, normal), 0.0), material.shininess);
    return ambient + shadowFactor * (diffuse * diffuseScale + specular * specularScale);
}

// lightViewDirection := light direction in view space
float ComputeShadowMap(vec3 lightViewDirection, sampler2D shadowMap, mat4 lightSpace, vec3 normal)
{
    vec4 lightFragmentPosition = lightSpace * model * vec4(VertexPosition, 1.0);
    // perform perspective divide
    vec3 projectionCoords = lightFragmentPosition.xyz / lightFragmentPosition.w;
    // transform to [0,1] range
    projectionCoords = projectionCoords * 0.5 + 0.5;

    if(1.0 < projectionCoords.z)
        return 0.0;

    // get depth of current fragment from light's perspective
    float currentDepth = projectionCoords.z;
    // calculate bias
    float bias = max(0.005 * (1.0 - dot(normal, lightViewDirection)), 0.0005);
    float shadow = 0.0;
    vec2 texelSize = 1.0 / textureSize(shadowMap, 0);
    for(int x = -1; x <= 1; ++x)
    {
        for(int y = -1; y <= 1; ++y)
        {
            // percentage close fitting
            float pcfDepth = texture(shadowMap, projectionCoords.xy + vec2(x, y) * texelSize).r;
            shadow += pcfDepth < currentDepth - bias ? 0.0 : 1.0;
        }
    }
    return shadow / 9.0;
}

// worldPosition = vec3(model * vec4(VertexPosition, 1.0))
float ComputeShadowCubemap(vec3 worldPosition, vec3 lightPosition, samplerCube shadowCubemap, vec3 normal)
{
    // get vector between fragment position and light position
    vec3 fragmentToLight = worldPosition - lightPosition;
    // use the light to fragment vector to sample from the depth map    
    float closestDepth = texture(shadowCubemap, fragmentToLight).r;
    // it is currently in linear range between [0,1]. Re-transform back to original value
    closestDepth *= pointLightFarPlane;
    // now get current linear depth as the length between the fragment and light position
    float currentDepth = length(fragmentToLight);
    // now test for shadows
    float bias = max(0.05 * (1.0 - dot(normal, normalize(-fragmentToLight))), 0.005);
    return closestDepth < currentDepth - bias ? 0.0 : 1.0;
}