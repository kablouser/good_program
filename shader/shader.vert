#version 330 core
layout (location = 0) in vec3 aVertex;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in vec2 aUV;

uniform mat4 modelToProjection; // projection * view * model
uniform mat4 modelToView; // view * model
uniform mat4 directionModelToView; // transpose(inverse(view * model))

out vec3 Normal;
out vec3 FragmentPosition;
out vec2 UV;
out vec3 VertexPosition;

void main()
{
    gl_Position = modelToProjection * vec4(aVertex, 1.0);
    Normal = mat3(directionModelToView) * aNormal;
    FragmentPosition = vec3(modelToView * vec4(aVertex, 1.0));
    UV = aUV;
    VertexPosition = aVertex;
}
