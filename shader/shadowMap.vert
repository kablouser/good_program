#version 330 core
layout (location = 0) in vec3 aPosition;

uniform mat4 modelToProjection; // projection * view * model

void main()
{
    gl_Position = modelToProjection * vec4(aPosition, 1.0);
}