#version 330 core
layout (location = 0) in vec3 aVertex;

uniform mat4 viewToProjection; // projection * view

out vec3 TextureCoordinate;

void main()
{
    TextureCoordinate = aVertex;
    vec4 position = viewToProjection * vec4(aVertex, 1.0);
    gl_Position = position.xyww;
}
