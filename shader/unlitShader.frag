#version 330 core
in vec2 UV;

out vec4 FragColor;

uniform bool isTextureEnabled = false;
uniform sampler2D texture0;
uniform vec3 color = vec3(1.0, 1.0, 1.0);

void main()
{
    if(isTextureEnabled)
        FragColor = texture(texture0, UV);
    else
        FragColor = vec4(color, 1.0);
}
