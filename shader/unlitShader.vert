#version 330 core
layout (location = 0) in vec3 aVertex;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in vec2 aUV;

uniform mat4 modelToProjection; // projection * view * model

uniform bool isScaleEnabled = false;
uniform bool scaleAlongNormals = false;
uniform float scale = 1.0;

out vec2 UV;

void main()
{
    if(isScaleEnabled)
    {
        if(scaleAlongNormals)
            gl_Position = modelToProjection * vec4(aVertex + normalize(aNormal) * (scale - 1.0), 1.0);
        else
            gl_Position = modelToProjection * mat4x4(
                scale,0,0,0,
                0,scale,0,0,
                0,0,scale,0,
                0,0,0    ,1) * vec4(aVertex, 1.0);
    }
    else
    {
        gl_Position = modelToProjection * vec4(aVertex, 1.0);
    }

    UV = aUV;
}
