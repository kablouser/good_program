#include "Camera.h"
#include "MathUtility.h"

Camera::Camera(SceneObject* parent) :
	Camera(parent, nullptr) {}

Camera::Camera(
	SceneObject* parent,
	RenderFunction renderFunction,
	Framebuffer* outputTarget,
	QVector4D backgroundColor) :
	SceneComponent(parent), transform(nullptr),
	renderFunction(renderFunction),
	outputTarget(outputTarget),
	backgroundColor(backgroundColor),
	isReflectionEnabled(false)
{
	allCameras.insert(this);
}

Camera::~Camera()
{
	allCameras.erase(this);
}

void Camera::renderAll()
{
	for (auto i = allCameras.begin(); i != allCameras.end(); ++i)
	{
		Camera* camera = *i;
		if (camera->getExecutable())
		{
			RenderFunction renderFunction = camera->renderFunction;
			if (renderFunction != nullptr)
				renderFunction(
					camera->getProjection(),
					camera->getView(),
					camera->transform->getPosition(),
					camera->outputTarget,
					camera->backgroundColor,
					camera->isReflectionEnabled); // toggle reflections ui
		}
	}
}

void Camera::setProjection(float fov, float aspectRatio, float nearPlane, float farPlane)
{
	projection.setToIdentity();
	projection.perspective(fov, aspectRatio, nearPlane, farPlane);
}

const QMatrix4x4& Camera::getProjection()
{
	return projection;
}

QMatrix4x4 Camera::getView()
{
	QMatrix4x4 view;
	QVector3D eye = transform->getPosition();
	view.lookAt(eye, eye + transform->getForward(), QVector3D(0.0f, 1.0f, 0.0f));
	return view;
}

void Camera::start()
{
	transform = getComponent<Transform>();
	if (transform == nullptr)
		qCritical("Camera : cannot find transform");
}

void Camera::setTransform(const QVector3D& position, const QQuaternion& rotation)
{
	transform->setPosition(position);
	transform->setRotation(rotation);
}

std::set<Camera*> Camera::allCameras;
