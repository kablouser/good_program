#include "DirectionLight.h"

DirectionLight::DirectionLight(SceneObject* parent, QVector3D color) :
	GenericLight(parent)
{
	lightData.direction = QVector3D(0, 0, -1);
	lightData.components.ambient = color * 0.3f;
	lightData.components.diffuse = color * 0.5f;
	lightData.components.specular = color;
	allDirectionLights.insert(this);
}

DirectionLight::~DirectionLight()
{
	allDirectionLights.erase(this);
}

bool DirectionLight::initialize(
	const char* vertexShaderPath, 
	const char* fragmentShaderPath, 
	int shadowMapResolution, 
	float mapBounds)
{
	bool success =
		shadowMapShader.compileFiles(vertexShaderPath, fragmentShaderPath) &&
		shadowMapShader.checkUniformId("modelToProjection", modelToProjectionId) &&
		shadowMapFramebuffer.createDepthOnly(shadowMapResolution, shadowMapResolution);
	if (success)
	{
		DirectionLight::shadowMapResolution = shadowMapResolution;
		DirectionLight::mapBounds = mapBounds;
	}
	return success;
}

void DirectionLight::updateShadowMaps(int viewportWidth, int viewportHeight)
{
	shadowMapShader.use();

	OpenGLProfile::getCurrent()->glViewport(0, 0, shadowMapResolution, shadowMapResolution);
		
	for (auto i = allDirectionLights.begin(); i != allDirectionLights.end(); ++i)
	{
		if ((*i)->getExecutable())
		{
			shadowMapFramebuffer.attachExternalDepth((*i)->shadowMap);
			OpenGLProfile::getCurrent()->glClear(GL_DEPTH_BUFFER_BIT);
			Renderer::foreachRenderer([=](Renderer* renderer)
			{
				// set model matrix
				shadowMapShader.setUniformMatrix(modelToProjectionId,
					(*i)->lightData.lightSpace * renderer->getModelMatrix());
				renderer->draw();
			}
			);
		}
	}

	OpenGLProfile::getCurrent()->glViewport(0, 0, viewportWidth, viewportHeight);
}

void DirectionLight::setShadowMapShader(const QMatrix4x4& modelToProjection)
{
	shadowMapShader.use();
	shadowMapShader.setUniformMatrix(modelToProjectionId, modelToProjection);
}

void DirectionLight::updateLightData()
{
	StandardRenderer::setDirectionLight(&lightData);
}

void DirectionLight::setEnabled(bool isEnabled)
{
	if (getEnabled() == isEnabled)
		return;

	if (isEnabled)
		updateLightData();
	else
		StandardRenderer::removeDirectionLight(&lightData);

	SceneComponent::setEnabled(isEnabled);
}

void DirectionLight::initializeLight()
{
	lightData.direction = transform->getForward();
	lightData.shadowMap = &shadowMap;
	lightData.lightSpace.ortho(-mapBounds, mapBounds, -mapBounds, mapBounds, -mapBounds, mapBounds);

	QVector3D position = transform->getPosition();
	lightData.lightSpace.lookAt(position, position + lightData.direction, transform->getUp());
	shadowMap.createRaw(shadowMapResolution, shadowMapResolution,
		GL_CLAMP_TO_EDGE, GL_LINEAR, GL_LINEAR, GL_DEPTH_COMPONENT);
}

bool DirectionLight::hasLightChanged()
{
	QVector3D newDirection = transform->getForward();
	if (qFuzzyCompare(lightData.direction, newDirection) == false)
	{
		lightData.direction = newDirection;
		lightData.lightSpace.setToIdentity();
		lightData.lightSpace.ortho(-mapBounds, mapBounds, -mapBounds, mapBounds, -mapBounds, mapBounds);
		lightData.lightSpace.lookAt(transform->getPosition(), transform->getPosition() + lightData.direction, transform->getUp());
		return true;
	}
	else
	{
		return false;
	}
}

std::set<DirectionLight*> DirectionLight::allDirectionLights;
ShaderProgram DirectionLight::shadowMapShader;
Framebuffer DirectionLight::shadowMapFramebuffer;
int DirectionLight::shadowMapResolution = 0;
float DirectionLight::mapBounds = 0.0f;
int DirectionLight::modelToProjectionId = -1;
