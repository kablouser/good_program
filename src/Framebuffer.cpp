#include "Framebuffer.h"

Framebuffer::Framebuffer() : frameBufferId(-1), renderBufferObject(-1), is3D(false) {}

Framebuffer::~Framebuffer()
{
	if (OpenGLProfile::getCurrent() == nullptr)
		return;

	if (frameBufferId != -1)
		OpenGLProfile::getCurrent()->glDeleteFramebuffers(1, &frameBufferId);
	if (renderBufferObject != -1)
		OpenGLProfile::getCurrent()->glDeleteRenderbuffers(1, &renderBufferObject);
}

bool Framebuffer::create(int width, int height)
{
	createDepthStencil(width, height);

	attachment.createRaw(width, height);
	// attach it to currently bound framebuffer object
	OpenGLProfile::getCurrent()->glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
		attachment.getId(), 0);
	
	if (OpenGLProfile::getCurrent()->glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		qCritical("Framebuffer : is not complete!");
		OpenGLProfile::getCurrent()->glBindFramebuffer(GL_FRAMEBUFFER, 0);
		return false;
	}

	// bind back to default framebuffer
	OpenGLProfile::getCurrent()->glBindFramebuffer(GL_FRAMEBUFFER, 0);
	is3D = false;
	return true;
}

bool Framebuffer::createDepthOnly(int width, int height)
{
	if (frameBufferId == -1)
		OpenGLProfile::getCurrent()->glGenFramebuffers(1, &frameBufferId);
	OpenGLProfile::getCurrent()->glBindFramebuffer(GL_FRAMEBUFFER, frameBufferId);

	attachment.createRaw(width, height, GL_CLAMP_TO_EDGE, GL_LINEAR, GL_LINEAR, GL_DEPTH_COMPONENT);

	// attach it to currently bound framebuffer object
	OpenGLProfile::getCurrent()->glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D,
		attachment.getId(), 0);
	OpenGLProfile::getCurrent()->glDrawBuffer(GL_NONE);
	OpenGLProfile::getCurrent()->glReadBuffer(GL_NONE);

	if (OpenGLProfile::getCurrent()->glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		qCritical("Framebuffer : is not complete!");
		OpenGLProfile::getCurrent()->glBindFramebuffer(GL_FRAMEBUFFER, 0);
		return false;
	}

	// bind back to default framebuffer
	OpenGLProfile::getCurrent()->glBindFramebuffer(GL_FRAMEBUFFER, 0);
	is3D = false;
	return true;
}

bool Framebuffer::createDepthOnly3D(int widthPerFace, int heightPerFace)
{
	if (frameBufferId == -1)
		OpenGLProfile::getCurrent()->glGenFramebuffers(1, &frameBufferId);
	OpenGLProfile::getCurrent()->glBindFramebuffer(GL_FRAMEBUFFER, frameBufferId);

	attachment.createRaw3D(widthPerFace, heightPerFace, GL_CLAMP_TO_EDGE, GL_LINEAR, GL_LINEAR, GL_DEPTH_COMPONENT);
	OpenGLProfile::getCurrent()->glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, attachment.getId(), 0);
	OpenGLProfile::getCurrent()->glDrawBuffer(GL_NONE);
	OpenGLProfile::getCurrent()->glReadBuffer(GL_NONE);

	if (OpenGLProfile::getCurrent()->glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		qCritical("Framebuffer : is not complete!");
		OpenGLProfile::getCurrent()->glBindFramebuffer(GL_FRAMEBUFFER, 0);
		return false;
	}

	// bind back to default framebuffer
	OpenGLProfile::getCurrent()->glBindFramebuffer(GL_FRAMEBUFFER, 0);
	is3D = true;
	return true;
}

void Framebuffer::attachExternalFace(Texture& cubemap, int face)
{
	if (is3D == true)
		qFatal("Framebuffer : can only attach external face on a 2d framebuffer");
	OpenGLProfile::getCurrent()->glBindFramebuffer(GL_FRAMEBUFFER, frameBufferId);
	OpenGLProfile::getCurrent()->glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
		GL_TEXTURE_CUBE_MAP_POSITIVE_X + face, cubemap.getId(), 0);
}

void Framebuffer::attachExternalDepth(Texture& depthMap)
{
	if (is3D == true)
		qFatal("Framebuffer : can only attach external depth on a 2d framebuffer");
	OpenGLProfile::getCurrent()->glBindFramebuffer(GL_FRAMEBUFFER, frameBufferId);
	OpenGLProfile::getCurrent()->glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthMap.getId(), 0);
}

void Framebuffer::attachExternalDepth3D(Texture& depthCubemap)
{
	if (is3D == false)
		qFatal("Framebuffer : can only attach external depth 3d on a 3d framebuffer");
	OpenGLProfile::getCurrent()->glBindFramebuffer(GL_FRAMEBUFFER, frameBufferId);
	OpenGLProfile::getCurrent()->glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depthCubemap.getId(), 0);
}

void Framebuffer::createDepthStencil(int width, int height)
{
	if (frameBufferId == -1)
		OpenGLProfile::getCurrent()->glGenFramebuffers(1, &frameBufferId);
	OpenGLProfile::getCurrent()->glBindFramebuffer(GL_FRAMEBUFFER, frameBufferId);

	// composite depth, stencil render buffer object
	if (renderBufferObject == -1)
		OpenGLProfile::getCurrent()->glGenRenderbuffers(1, &renderBufferObject);
	OpenGLProfile::getCurrent()->glBindRenderbuffer(GL_RENDERBUFFER, renderBufferObject);
	OpenGLProfile::getCurrent()->glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);
	OpenGLProfile::getCurrent()->glBindRenderbuffer(GL_RENDERBUFFER, 0);

	// attach it to currently bound framebuffer object
	OpenGLProfile::getCurrent()->glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER,
		renderBufferObject);
}
