#include "InputManager.h"

#include <QKeyEvent>
#include <QMouseEvent>
#include <QWindow>
#include <QObject>

InputManager::InputManager(QWidget* parent, bool isCursorLocked) :
	block1Keys(), block2Keys(), block3Keys()
{
	this->parent = parent;
	this->isCursorLocked = isCursorLocked;
	mousePositionX = 0;
	mousePositionY = 0;
	mouseDeltaX = mouseDeltaY = 0;
}

void InputManager::keyPressEvent(QKeyEvent* event)
{
	if (event->isAutoRepeat())
		return;
	setKeyRecord(event->key(), true);
}

void InputManager::keyReleaseEvent(QKeyEvent* event)
{
	if (event->isAutoRepeat())
		return;
	setKeyRecord(event->key(), false);
}

void InputManager::mousePressEvent(QMouseEvent* event)
{
	setKeyRecord(event->button(), true);
}

void InputManager::mouseReleaseEvent(QMouseEvent* event)
{
	setKeyRecord(event->button(), false);
}

void InputManager::update()
{
	QPoint localPos = convertToLocal(QCursor::pos());

	mouseDeltaX = localPos.x() - mousePositionX;
	mouseDeltaY = localPos.y() - mousePositionY;

	if (parent->isActiveWindow() && isCursorLocked)
	{
		QCursor::setPos(
			parent->x() + parent->width() / 2,
			parent->y() + parent->height() / 2);

		mousePositionX = 0;
		mousePositionY = 0;
	}
	else
	{
		mousePositionX = localPos.x();
		mousePositionY = localPos.y();
	}
}

bool InputManager::isKeyPressed(int key)
{
	bool* keyRecord = getKeyRecord(key);
	if (keyRecord != NULL)
		return *keyRecord;
	else
	{
		qWarning("key not being recorded, %i", key);
		return false;
	}
}

void InputManager::setCursorLock(bool isLocked)
{
	isCursorLocked = isLocked;
}

void InputManager::getMousePosition(int& x, int& y)
{
	x = mousePositionX;
	y = mousePositionY;
}

void InputManager::getMouseDelta(int& x, int& y)
{
	x = mouseDeltaX;
	y = mouseDeltaY;
}

void InputManager::setKeyRecord(int key, bool newValue)
{
	bool* keyRecord = getKeyRecord(key);
	if (keyRecord != NULL)
		*keyRecord = newValue;
}

bool* InputManager::getKeyRecord(int key)
{
	if (block1Start <= key && key <= block1End)
		return block1Keys + key - block1Start;
	else if (block2Start <= key && key <= block2End)
		return block2Keys + key - block2Start;
	else if (block3Start <= key && key <= block3End)
		return block3Keys + key - block3Start;
	else
		return NULL;
}

QPoint InputManager::convertToLocal(QPoint cursorPosition)
{
	return cursorPosition - QPoint(
		parent->x() + parent->width() / 2,
		parent->y() + parent->height() / 2);
}