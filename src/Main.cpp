#include <QApplication>
#include <QTimer>
#include <QElapsedTimer>
#include <QStyle>
#include <QDesktopWidget>
#include <QFont>
#include "MyGLWindow.h"

#include <QGLFormat>

int main(int argc, char* argv[])
{
	QApplication app(argc, argv);
	QFont font = app.font();
	font.setPointSize(8);
	app.setFont(font);

    QGLFormat glFormat;
    glFormat.setVersion(OpenGLProfile_Major, OpenGLProfile_Minor);
    glFormat.setProfile(QGLFormat::CoreProfile);
	QGLFormat::setDefaultFormat(glFormat);

	MyGLWindow window;
	window.showMaximized();

	return app.exec();
}
