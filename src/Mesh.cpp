#include "Mesh.h"
#include "OpenGLProfile.h"

#include <QImage>
#include <QGLWidget>

Mesh::Mesh()
{
	vertexArrayObject = vertexBufferObject = elementBufferObject = -1;
	isDataSet = useIndices = false;
	drawSize = 0;
	drawMode = GL_TRIANGLES;
}

Mesh::~Mesh()
{
	if (OpenGLProfile::getCurrent() == nullptr)
		return;

	if(vertexArrayObject != -1)
		OpenGLProfile::getCurrent()->glDeleteVertexArrays(1, &vertexArrayObject);
	if (vertexBufferObject != -1)
		OpenGLProfile::getCurrent()->glDeleteBuffers(1, &vertexBufferObject);
	if (useIndices && elementBufferObject != -1)
		OpenGLProfile::getCurrent()->glDeleteBuffers(1, &elementBufferObject);
}

void Mesh::setAsCube(float scale)
{
	float vertices[] =
	{
		// positions          // normals           // texture coords
		-0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f, 0.0f,
		 0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f, 1.0f,
		 0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f, 0.0f,
		 0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  0.0f, 1.0f,
		-0.5f, -0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f, 0.0f,
		-0.5f,  0.5f, -0.5f,  0.0f,  0.0f, -1.0f,  1.0f, 1.0f,

		-0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   0.0f, 0.0f,
		 0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   1.0f, 0.0f,
		 0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   1.0f, 1.0f,
		 0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   1.0f, 1.0f,
		-0.5f,  0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   0.0f, 1.0f,
		-0.5f, -0.5f,  0.5f,  0.0f,  0.0f, 1.0f,   0.0f, 0.0f,

		-0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  1.0f, 1.0f,
		-0.5f,  0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  0.0f, 1.0f,
		-0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  0.0f, 0.0f,
		-0.5f, -0.5f, -0.5f, -1.0f,  0.0f,  0.0f,  0.0f, 0.0f,
		-0.5f, -0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  1.0f, 0.0f,
		-0.5f,  0.5f,  0.5f, -1.0f,  0.0f,  0.0f,  1.0f, 1.0f,

		 0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  0.0f, 1.0f,
		 0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f,
		 0.5f,  0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f,
		 0.5f, -0.5f, -0.5f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f,
		 0.5f,  0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  0.0f, 1.0f,
		 0.5f, -0.5f,  0.5f,  1.0f,  0.0f,  0.0f,  0.0f, 0.0f,

		-0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  0.0f, 1.0f,
		 0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  1.0f, 1.0f,
		 0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  1.0f, 0.0f,
		 0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  1.0f, 0.0f,
		-0.5f, -0.5f,  0.5f,  0.0f, -1.0f,  0.0f,  0.0f, 0.0f,
		-0.5f, -0.5f, -0.5f,  0.0f, -1.0f,  0.0f,  0.0f, 1.0f,

		-0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  0.0f, 1.0f,
		 0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  1.0f, 0.0f,
		 0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  1.0f, 1.0f,
		 0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  1.0f, 0.0f,
		-0.5f,  0.5f, -0.5f,  0.0f,  1.0f,  0.0f,  0.0f, 1.0f,
		-0.5f,  0.5f,  0.5f,  0.0f,  1.0f,  0.0f,  0.0f, 0.0f
	};
	static const int attributeSizes[] = { 3, 3, 2 };
	static const GLenum attributeTypes[] = { GL_FLOAT, GL_FLOAT, GL_FLOAT };

	scaleVertexPositions(vertices, sizeof(vertices) / sizeof(float), scale);

	setData(
		sizeof(vertices), vertices,
		0, NULL,
		sizeof(attributeSizes), attributeSizes, attributeTypes,
		GL_STATIC_DRAW);
}

void Mesh::setAsQuad(float scale, bool isDoubleSided)
{
	float vertices[] =
	{
		// positions          // normals           // texture coords
		0.5f,  0.5f, 0.0f,	  0.0f, 0.0f, 1.0f,	   1.0f, 1.0f,  // top right
		0.5f, -0.5f, 0.0f,	  0.0f, 0.0f, 1.0f,	   1.0f, 0.0f,  // bottom right
	   -0.5f, -0.5f, 0.0f,	  0.0f, 0.0f, 1.0f,	   0.0f, 0.0f,  // bottom left
	   -0.5f,  0.5f, 0.0f,	  0.0f, 0.0f, 1.0f,	   0.0f, 1.0f   // top left 
	};
	static const unsigned int indices[] =
	{
		0, 3, 1,   // first triangle
		1, 3, 2,   // second triangle
		1, 3, 0,   // first triangle
		2, 3, 1    // second triangle
	};
	static const int attributeSizes[] = { 3, 3, 2 };
	static const GLenum attributeTypes[] = { GL_FLOAT, GL_FLOAT, GL_FLOAT };

	scaleVertexPositions(vertices, sizeof(vertices) / sizeof(float), scale);

	int indicesSize = isDoubleSided ? sizeof(indices) : sizeof(indices) / 2;

	setData(
		sizeof(vertices), vertices,
		indicesSize, indices,
		sizeof(attributeSizes), attributeSizes, attributeTypes,
		GL_STATIC_DRAW);
}

void Mesh::setAsTriangle(float scale)
{
	float vertices[] =
	{
		// positions          // normals           // texture coords
	   -0.5f, -0.5f, 0.0f,	  0.0f, 0.0f, 1.0f,	   0.0f, 0.0f,  // bottom left
		0.5f, -0.5f, 0.0f,	  0.0f, 0.0f, 1.0f,	   1.0f, 0.0f,  // bottom right
		0.0f,  0.5f, 0.0f,	  0.0f, 0.0f, 1.0f,	   0.5f, 1.0f   // top middle
	};
	static const int attributeSizes[] = { 3, 3, 2 };
	static const GLenum attributeTypes[] = { GL_FLOAT, GL_FLOAT, GL_FLOAT };

	scaleVertexPositions(vertices, sizeof(vertices) / sizeof(float), scale);

	setData(
		sizeof(vertices), vertices,
		0, NULL,
		sizeof(attributeSizes), attributeSizes, attributeTypes,
		GL_STATIC_DRAW);
}

void Mesh::setData(
	GLsizeiptr verticesSize, const void* vertices,
	GLsizeiptr indicesSize, const void* indices,
	int attributeSize,			// size of the attributesSizes array
	const int* attributeSizes,	// array of ints describing the size of each attribute
	const GLenum* attributeTypes,	// what is the type of each individual attribute?
	GLenum usage	// the expected usage pattern of the data store
	)
{
	bool newUseIndices = indices != NULL;

	if (vertexBufferObject == -1)
		OpenGLProfile::getCurrent()->glGenBuffers(1, &vertexBufferObject);
	if (vertexArrayObject == -1)
		OpenGLProfile::getCurrent()->glGenVertexArrays(1, &vertexArrayObject);

	if (useIndices == false && newUseIndices)
		OpenGLProfile::getCurrent()->glGenBuffers(1, &elementBufferObject);
	else if (useIndices && newUseIndices == false)
		OpenGLProfile::getCurrent()->glDeleteBuffers(1, &elementBufferObject);
	useIndices = newUseIndices;

	// ..:: Initialization code (done once (unless your object frequently changes)) :: ..
	// 1. bind Vertex Array Object
	OpenGLProfile::getCurrent()->glBindVertexArray(vertexArrayObject);
	// 2. copy our vertices array in a buffer for OpenGL to use
	OpenGLProfile::getCurrent()->glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObject);
	OpenGLProfile::getCurrent()->glBufferData(GL_ARRAY_BUFFER, verticesSize, vertices, usage);
	if (useIndices)
	{
		// 3. copy our index array in a element buffer for OpenGL to use
		OpenGLProfile::getCurrent()->glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementBufferObject);
		OpenGLProfile::getCurrent()->glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesSize, indices, usage);
		drawSize = indicesSize;
	}
	else
	{
		drawSize = verticesSize;
	}
	// 4. then set our vertex attributes pointers:
	//		>> first calculate the stride sum
	int stride = 0;
	int attributeCount = attributeSize / sizeof(int);
	for (int i = 0; i < attributeCount; i++)
	{
		stride += attributeSizes[i] * sizeofType(attributeTypes[i]);
	}
	//		>> then create each attribute
	int currentOffset = 0;
	for (int i = 0; i < attributeCount; i++)
	{
		OpenGLProfile::getCurrent()->glVertexAttribPointer(i, attributeSizes[i], attributeTypes[i], GL_FALSE, stride, (void*)currentOffset);
		OpenGLProfile::getCurrent()->glEnableVertexAttribArray(i);
		currentOffset += attributeSizes[i] * sizeofType(attributeTypes[i]);
	}
	// 5. unbind
	OpenGLProfile::getCurrent()->glBindBuffer(GL_ARRAY_BUFFER, 0);
	OpenGLProfile::getCurrent()->glBindVertexArray(0);

	isDataSet = true;
}

void Mesh::setMesh(std::vector<Vertex> vertices, std::vector<unsigned int> indices)
{
	static const int attributeSizes[] = { 3, 3, 2 };
	static const GLenum attributeTypes[] = { GL_FLOAT, GL_FLOAT, GL_FLOAT };

	setData(
		vertices.size() * sizeof(Vertex), &vertices[0],
		indices.size() * sizeof(unsigned int), &indices[0],
		sizeof(attributeSizes), attributeSizes, attributeTypes,
		GL_STATIC_DRAW);
}

void Mesh::setDrawMode(GLenum drawMode)
{
	this->drawMode = drawMode;
}

void Mesh::draw()
{
	if (isDataSet)
	{
		OpenGLProfile::getCurrent()->glBindVertexArray(vertexArrayObject);

		if (useIndices)
			OpenGLProfile::getCurrent()->glDrawElements(drawMode, drawSize, GL_UNSIGNED_INT, 0);
		else
			OpenGLProfile::getCurrent()->glDrawArrays(drawMode, 0, drawSize);

		OpenGLProfile::getCurrent()->glBindVertexArray(0);
	}
	else
	{
		qWarning("Mesh : cannot draw before data is set!");
	}
}

int Mesh::sizeofType(GLenum type)
{
	switch (type)
	{
	case GL_FLOAT:
		return sizeof(float);
	default:
		qWarning("unimplemented sizeof type %i", type);
		return 0;
	}
}

void Mesh::scaleVertexPositions(float* vertices, int count, float scale)
{
	for (int i = 0; i < count; i += 8)
	{
		vertices[i] *= scale;
		vertices[i + 1] *= scale;
		vertices[i + 2] *= scale;
	}
}
