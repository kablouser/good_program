#include "Model.h"

#include <QFile>
#include <QString>
#include <QTextStream>
#include <map>

using namespace std;

Model::~Model()
{
	clearData();
}

bool Model::loadModel(const char* path, double scale, bool flipUVs)
{
	if (path == nullptr)
	{
		qWarning() << "loadModel : path is null";
		return false;
	}

	QString pathString = QString(path);
	if(pathString.endsWith(".obj", Qt::CaseInsensitive) == false)
	{
		qWarning() << "loadModel : only wavefront .obj files supported" << pathString;
		return false;
	}

	QFile file(pathString);
	if (file.open(QIODevice::ReadOnly | QIODevice::Text) == false)
	{
		qWarning() << "loadModel : failed to open file" << pathString;
		return false;
	}

	clearData();
	
	QString folder = pathString.left(pathString.lastIndexOf('/')+1);

	vector<QVector3D> positions;
	vector<QVector3D> normals;
	vector<QVector2D> uvs;
	vector<float> verticesData;
	FaceType usingFaceType = FaceType::unknown;

	Material* currentMaterial = nullptr;
	map<QString, Material*> materialMap;

	QTextStream in(&file);
	while (in.atEnd() == false)
	{
		QString line = in.readLine();
		if (line.startsWith('#'))
			continue;

		QStringList tokens = line.split(QLatin1Char(' '));
		if (tokens[0] == "f")
		{
			if (4 < tokens.size())
			{
				qWarning() << "loadModel : only 3 sided faces supported";
				return false;
			}
			if (usingFaceType == FaceType::unknown)
			{
				usingFaceType = getFaceType(tokens[1]);
				if (usingFaceType != FaceType::positionNormal &&
					usingFaceType != FaceType::positionTextureNormal)
				{
					qWarning() << "loadModel : all face declares must include normals";
					return false;
				}
			}
			switch (usingFaceType)
			{
			case FaceType::positionNormal:
				// f v1//vn1 v2//vn2 v3//vn3
				for (int i = 1; i < 4; i++)
				{
					QStringList indices = tokens[i].split("//");
					// wavefront indices are 1-BASED!
					const QVector3D& position = positions[(size_t)indices[0].toInt() - 1];
					const QVector3D& normal = normals[(size_t)indices[1].toInt() - 1];
					verticesData.insert(verticesData.end(), {
						position.x(), position.y(), position.z(),
						normal.x(), normal.y(), normal.z() });
				}
				break;
			case FaceType::positionTextureNormal:
				// f v1/vt1/vn1 v2/vt2/vn2 v3/vt3/vn3
				for (int i = 1; i < 4; i++)
				{
					QStringList indices = tokens[i].split('/');
					// wavefront indices are 1-BASED!
					const QVector3D& position = positions[(size_t)indices[0].toInt() - 1];
					const QVector2D& uv = uvs[(size_t)indices[1].toInt() - 1];
					const QVector3D& normal = normals[(size_t)indices[2].toInt() - 1];
					verticesData.insert(verticesData.end(), {
						position.x(), position.y(), position.z(),
						normal.x(), normal.y(), normal.z(),
						uv.x(), uv.y() });
				}
				break;
			}
		}
		else if (tokens[0] == "v")
		{
			// vertex position data
			// v x y z [w]
			positions.push_back(QVector3D(
				tokens.at(1).toFloat() * scale, tokens.at(2).toFloat() * scale, tokens.at(3).toFloat() * scale));
		}
		else if (tokens[0] == "vt")
		{
			// texture coordinates
			// vt u [v [w]]
			QVector2D uv(tokens.at(1).toFloat(), 0);
			if (2 < tokens.size())
			{
				if(flipUVs)
					uv.setY(1.0 - tokens.at(2).toFloat());
				else
					uv.setY(tokens.at(2).toFloat());
			}
			uvs.push_back(uv);
		}
		else if (tokens[0] == "vn")
		{
			// vertex normals
			// vn x y z
			QVector3D normal(tokens.at(1).toFloat(), tokens.at(2).toFloat(), tokens.at(3).toFloat());
			normal.normalize();
			normals.push_back(normal);
		}
		else if (tokens[0] == "mtllib")
		{
			if (loadMtl(folder, line.mid(7), materialMap) == false)
				return false;
		}
		else if (tokens[0] == "usemtl")
		{
			auto oldMaterial = currentMaterial;
			auto newMaterial = materialMap[line.mid(7)];
			if (oldMaterial != newMaterial)
			{
				currentMaterial = newMaterial;
				if (oldMaterial != nullptr)
				{
					addMeshMaterial(usingFaceType, verticesData, currentMaterial);
					verticesData.clear();
				}
			}
		}
	}

	addMeshMaterial(usingFaceType, verticesData, currentMaterial);
	return true;
}

void Model::getMeshMaterial(int index, Mesh** outMesh, Material** outMaterial)
{
	if (0 <= index && index < meshMaterialPairs.size())
	{
		if (outMesh != nullptr)
			*outMesh = meshMaterialPairs[index].mesh;
		if (outMaterial != nullptr)
			*outMaterial = meshMaterialPairs[index].material;
	}
	else
	{
		if (outMesh != nullptr)
			*outMesh = nullptr;
		if (outMaterial != nullptr)
			*outMaterial = nullptr;
	}
}

bool Model::loadMtl(
	const QString& folder, const QString& mtlFile,
	std::map<QString, Material*>& materialMap)
{
	QString path(folder + mtlFile);
	QFile mtl(path);
	if (mtl.open(QIODevice::ReadOnly | QIODevice::Text) == false)
	{
		qWarning() << "loadModel : failed to open mtl" << path;
		return false;
	}

	QTextStream mtlIn(&mtl);
	Material* currentMaterial = nullptr;
	while (mtlIn.atEnd() == false)
	{
		QString mtlLine = mtlIn.readLine();
		if (mtlLine.startsWith('#'))
			continue;

		//materialMap
		QStringList mtlTokens = mtlLine.split(QLatin1Char(' '));
		if (mtlTokens[0] == "newmtl")
		{
			currentMaterial = materialMap[mtlLine.mid(7)];
			if (currentMaterial == nullptr)
			{
				materialMap[mtlLine.mid(7)] = currentMaterial = new Material();
			}
		}
		else if (mtlTokens[0] == "Kd")
		{
			if (currentMaterial == nullptr)
			{
				qWarning() << "loadModel : no newmtl";
				return false;
			}
			currentMaterial->diffuse = MaterialComponent<QVector4D>(
				QVector4D(mtlTokens[1].toFloat(), mtlTokens[2].toFloat(), mtlTokens[3].toFloat(), 1.0f));
		}
		else if (mtlTokens[0] == "Ks")
		{
			if (currentMaterial == nullptr)
			{
				qWarning() << "loadModel : no newmtl";
				return false;
			}
			currentMaterial->specular = MaterialComponent<QVector3D>(
				QVector3D(mtlTokens[1].toFloat(), mtlTokens[2].toFloat(), mtlTokens[3].toFloat()));
		}
		else if (mtlTokens[0] == "Ns")
		{
			if (currentMaterial == nullptr)
			{
				qWarning() << "loadModel : no newmtl";
				return false;
			}
			currentMaterial->shininess = mtlTokens[1].toFloat();
		}
		else if (mtlTokens[0] == "d")
		{
			if (currentMaterial == nullptr)
			{
				qWarning() << "loadModel : no newmtl";
				return false;
			}
			float alpha = mtlTokens[1].toFloat();
			if (alpha < 1.0f)
				currentMaterial->isTransparent = true;
			if (currentMaterial->diffuse.isSingle)
				currentMaterial->diffuse.data.singleColor.setW(alpha);
		}
		else if (mtlTokens[0] == "Tr")
		{
			if (currentMaterial == nullptr)
			{
				qWarning() << "loadModel : no newmtl";
				return false;
			}
			float transparency = mtlTokens[1].toFloat();
			if (0.0f < transparency)
				currentMaterial->isTransparent = true;
			if (currentMaterial->diffuse.isSingle)
				currentMaterial->diffuse.data.singleColor.setW(1.0 - transparency);
		}
		else if (mtlTokens[0] == "Ke")
		{
			if (currentMaterial == nullptr)
			{
				qWarning() << "loadModel : no newmtl";
				return false;
			}
			currentMaterial->emission = MaterialComponent<QVector3D>(
				QVector3D(mtlTokens[1].toFloat(), mtlTokens[2].toFloat(), mtlTokens[3].toFloat()));
		}
		else if (mtlTokens[0] == "map_Kd")
		{
			if (currentMaterial == nullptr)
			{
				qWarning() << "loadModel : no newmtl";
				return false;
			}
			if (Texture* texture = loadTextureMap(folder + mtlLine.mid(7)))
			{
				currentMaterial->diffuse =
					MaterialComponent<QVector4D>(texture);
			}
			else
			{
				return false;
			}
		}
		else if (mtlTokens[0] == "map_Ks")
		{
			if (currentMaterial == nullptr)
			{
				qWarning() << "loadModel : no newmtl";
				return false;
			}
			if (Texture* texture = loadTextureMap(folder + mtlLine.mid(7)))
			{
				currentMaterial->specular =
					MaterialComponent<QVector3D>(texture);
			}
			else
			{
				return false;
			}
		}
		else if (mtlTokens[0] == "map_Ke")
		{
			if (currentMaterial == nullptr)
			{
				qWarning() << "loadModel : no newmtl";
				return false;
			}
			if (Texture* texture = loadTextureMap(folder + mtlLine.mid(7)))
			{
				currentMaterial->emission =
					MaterialComponent<QVector3D>(texture);
			}
			else
			{
				return false;
			}
		}
		// import bump map?
	}
	return true;
}

Texture* Model::loadTextureMap(const QString& path)
{
	Texture* texture = new Texture();
	if (texture->create((path).toStdString().c_str()))
	{
		textures.push_back(texture);
		return texture;
	}
	else
	{
		delete texture;
		qWarning() << "loadModel : cannot find texture map" << path;
		return nullptr;
	}
}

void Model::addMeshMaterial(
	FaceType usingFaceType,
	std::vector<float>& verticesData,
	Material* currentMaterial)
{
	int attributes = usingFaceType == FaceType::positionNormal ? 2 * sizeof(int) : 3 * sizeof(int);
	const int attributeSizes[] = { 3, 3, 2 };
	const GLenum attributeTypes[] = { GL_FLOAT, GL_FLOAT, GL_FLOAT };

	MeshMaterialPair pair;
	pair.mesh = new Mesh();
	pair.mesh->setData(
		sizeof(float) * verticesData.size(), &verticesData[0],
		0, nullptr,
		attributes, attributeSizes, attributeTypes);

	pair.material = currentMaterial;
	if (pair.material == nullptr)
	{
		pair.material = new Material();
	}
	meshMaterialPairs.push_back(pair);
}

Model::FaceType Model::getFaceType(QString token)
{
	bool isPreviousCharSlash = false; // is the previous character a slash '/'
	int slashesCount = 0;
	for (int i = 0; i < token.length(); i++)
	{
		bool isSlash = token[i] == '/';
		if (isSlash)
		{
			if (isPreviousCharSlash)
				return FaceType::positionNormal;
			slashesCount++;
			if (2 <= slashesCount)
				return FaceType::positionTextureNormal;
		}
		isPreviousCharSlash = isSlash;
	}

	return slashesCount == 0 ? FaceType::position : FaceType::positionTexture;
}

void Model::clearData()
{
	for (int i = 0; i < meshMaterialPairs.size(); ++i)
	{
		delete meshMaterialPairs[i].mesh;
		delete meshMaterialPairs[i].material;
	}
	for (int i = 0; i < textures.size(); ++i)
	{
		delete textures[i];
	}
	meshMaterialPairs.clear();
	textures.clear();
}
