#include "MyGLWidget.h"
#include <QMatrix4x4>
#include <QtMath>
#include <QApplication>

#include "MyGLWindow.h"
#include "MathUtility.h"
#include "Skybox.h"

using namespace std;

MyGLWidget::MyGLWidget(QWidget* parent, InputManager& inputManager, TimeManager& timeManager) :
	QGLWidget(parent),
	cameraAngle(0, 90),
	inputManager(inputManager),
	timeManager(timeManager),
	logger(this),

	cameraObject("camera"),	
	cameraTransform(&cameraObject, QVector3D(0, 1, 0), 
		MathUtility::eulerToQuaternion(0, 0, 0)),
	camera(&cameraObject, Renderer::renderAll),

	reverseCameraObject("reverse camera", &cameraObject),
	reverseCameraTransform(&reverseCameraObject),
	reverseCamera(&reverseCameraObject, Renderer::renderAll, &reverseFramebuffer, 
		QVector4D(0.1, 0.1, 0.1, 1.0)),

	spotLight("spotlight"),
	spotLightComponent(&spotLight.sceneObject, QVector3D(1,1,1), 12.0f),
	directionLight("direction light"),
	directionLightComponent(&directionLight.sceneObject),

	reverseMirror("reverse mirror"),
	floor("floor"),
	normalPhoto("normalPhoto"), bigPhoto("bigPhoto"),

	globe("globe"), globeStand("globeStand"), globeHolder("globeHolder"),
	isosphere("isophere"),
	monitor("monitor"),
	person("person"),
	shield("shield"),
	sword("sword"),

	spotLightEmissive(nullptr),
	kernelMode(0)
{
	setMinimumSize(640, 480);
	profile = new OpenGLProfile();
	for (int i = 0; i < 3; ++i)
	{
		pointLights[i] = new RenderObject("pointlight" + i);
		pointLightComponents[i] = new PointLight(&pointLights[i]->sceneObject);
	}
	for (int i = 0; i < 5; ++i)
	{
		windows[i] = new RenderObject("window" + i);
	}
	cameraTransform.setRotation(MathUtility::eulerToQuaternion(cameraAngle.x(), cameraAngle.y(), 0));
}

MyGLWidget::~MyGLWidget()
{
	for (int i = 0; i < 3; ++i)
	{
		delete pointLights[i];
		delete pointLightComponents[i];
	}
	for (int i = 0; i < 5; ++i)
	{
		delete windows[i];
	}

	// have to leak some memory, 
	// QOpenGLFunctions_3_3_Core destructor causes crashes
	// delete profile;
}

void MyGLWidget::setReflection(bool isEnabled)
{
	camera.isReflectionEnabled = reverseCamera.isReflectionEnabled = isEnabled;
}

void MyGLWidget::setReflectionUpdateFrequency(unsigned int probesPerFrame)
{
	ReflectionProbe::probesPerFrame = probesPerFrame;
}

void MyGLWidget::setDirectionLight(bool isEnabled)
{
	directionLightComponent.setEnabled(isEnabled);
}

void MyGLWidget::setDirectionLightBrightness(float brightness)
{
	directionLightComponent.lightData.components.ambient = brightness * QVector3D(0.3, 0.3, 0.28);
	directionLightComponent.lightData.components.diffuse = brightness * QVector3D(0.5, 0.5, 0.5 * 207 / 255.0f);
	directionLightComponent.lightData.components.specular = brightness * QVector3D(1, 1, 207 / 255.0f);
	directionLightComponent.updateLightData();
}

void MyGLWidget::setRedLight(bool isEnabled)
{
	pointLightComponents[0]->setEnabled(isEnabled);
	pointLights[0]->renderer.material->emission = isEnabled ? 
		MaterialComponent<QVector3D>(&lightBulbEmissiveRed) : 
		MaterialComponent<QVector3D>(QVector3D());
}

void MyGLWidget::setRedLightBrightness(float brightness)
{
	pointLightComponents[0]->lightData.components.ambient = brightness * QVector3D(1.0, 0.0, 0.0) * 0.05f;
	pointLightComponents[0]->lightData.components.diffuse = brightness * QVector3D(1.0, 0.0, 0.0);
	pointLightComponents[0]->lightData.components.specular = brightness * QVector3D(1.0, 0.0, 0.0);
}

void MyGLWidget::setGreenLight(bool isEnabled)
{
	pointLightComponents[1]->setEnabled(isEnabled);
	pointLights[1]->renderer.material->emission = isEnabled ?
		MaterialComponent<QVector3D>(&lightBulbEmissiveGreen) :
		MaterialComponent<QVector3D>(QVector3D());
}

void MyGLWidget::setGreenLightBrightness(float brightness)
{
	pointLightComponents[1]->lightData.components.ambient = brightness * QVector3D(0.0, 1.0, 0.0) * 0.05f;
	pointLightComponents[1]->lightData.components.diffuse = brightness * QVector3D(0.0, 1.0, 0.0);
	pointLightComponents[1]->lightData.components.specular = brightness * QVector3D(0.0, 1.0, 0.0);
}

void MyGLWidget::setBlueLight(bool isEnabled)
{
	pointLightComponents[2]->setEnabled(isEnabled);
	pointLights[2]->renderer.material->emission = isEnabled ?
		MaterialComponent<QVector3D>(&lightBulbEmissiveBlue) :
		MaterialComponent<QVector3D>(QVector3D());
}

void MyGLWidget::setBlueLightBrightness(float brightness)
{
	pointLightComponents[2]->lightData.components.ambient = brightness * QVector3D(0.0, 0.0, 1.0) * 0.05f;
	pointLightComponents[2]->lightData.components.diffuse = brightness * QVector3D(0.0, 0.0, 1.0);
	pointLightComponents[2]->lightData.components.specular = brightness * QVector3D(0.0, 0.0, 1.0);
}

void MyGLWidget::setSpotLight(bool isEnabled)
{
	spotLightComponent.setEnabled(isEnabled);
	spotLight.renderer.material->emission = isEnabled ?
		MaterialComponent<QVector3D>(spotLightEmissive) :
		MaterialComponent<QVector3D>(QVector3D());
}

void MyGLWidget::setSpotLightBrightness(float brightness)
{
	spotLightComponent.lightData.components.ambient = QVector3D();
	spotLightComponent.lightData.components.diffuse = brightness * QVector3D(1.0, 1.0, 1.0);
	spotLightComponent.lightData.components.specular = brightness * QVector3D(1.0, 1.0, 1.0);
	spotLightComponent.updateLightData();
}

void MyGLWidget::setSpotLightAngle(float angle)
{
	spotLightComponent.lightData.cutOff = qCos(qDegreesToRadians(angle * 0.9f));
	spotLightComponent.lightData.outerCutoff = qCos(qDegreesToRadians(angle));
	spotLightComponent.updateLightData();
}

void MyGLWidget::pointSpotLight()
{		
	spotLight.transform.setRotation(MathUtility::lookAt(
		spotLight.transform.getPosition() - cameraTransform.getPosition(), QVector3D(0.0f, 1.0f, 0.0f)));
}

void MyGLWidget::setOutline(bool isEnabled)
{
	for (int i = 0; i < outlineObjectCount; ++i)
		allOutlineObjects[i]->renderer.outlineEnabled = isEnabled;
}

void MyGLWidget::setOutlineScale(float scale)
{
	for (int i = 0; i < outlineObjectCount; ++i)
		allOutlineObjects[i]->renderer.outlineScale = scale;
}

void MyGLWidget::setReverseCamera(bool isEnabled)
{
	reverseCamera.setEnabled(isEnabled);
	reverseMirror.sceneObject.setSelfEnabled(isEnabled);
}

void MyGLWidget::setKernel(bool isEnabled)
{
	camera.renderFunction = isEnabled ? PostProcessor::renderAll : Renderer::renderAll;
}

void MyGLWidget::setKernelMatrix(int index)
{
	kernelMode = index;

	static float sharpenKernel[9]
	{
		2,2,2,
		2,-15,2,
		2,2,2
	};
	static float blurKernel[9]
	{
		1 / 16.0f, 2 / 16.0f, 1 / 16.0f,
		2 / 16.0f, 4 / 16.0f, 2 / 16.0f,
		1 / 16.0f, 2 / 16.0f, 1 / 16.0f
	};
	static float edgeKernel[9]
	{
		1,1,1,
		1,-8,1,
		1,1,1
	};
	static float doubleKernel[9]
	{
		0,0,0,
		0.5, 0, 0.5,
		0,0,0
	};
	static float* kernels[4]{ sharpenKernel, blurKernel, edgeKernel, doubleKernel };
	PostProcessor::offsetPixels = 1.0f;
	if (0 <= index && index < 4)
	{
		PostProcessor::kernel = QMatrix3x3(kernels[index]);
		PostProcessor::baseColor = QVector3D();
	}
}

void MyGLWidget::initializeGL()
{
	grabKeyboard();

	// initialise OpenGL
	if (profile->initializeOpenGLFunctions() == false)
	{
		qCritical("Failed to load OpenGL 3.3 (Core)!");
		exit(EXIT_FAILURE);
    	return;
	}

	// initialise logger
	logger.initialize();
	connect(&logger, &QOpenGLDebugLogger::messageLogged, [=](const QOpenGLDebugMessage& debugMessage)
	{		
		if (debugMessage.severity() == QOpenGLDebugMessage::LowSeverity ||
			debugMessage.severity() == QOpenGLDebugMessage::NotificationSeverity ||
			debugMessage.severity() == QOpenGLDebugMessage::MediumSeverity)
			return;

		qDebug() << debugMessage.message() << debugMessage.severity() << debugMessage.source() << debugMessage.type();
	}
	);
	logger.startLogging(QOpenGLDebugLogger::LoggingMode::SynchronousLogging);// AsynchronousLogging

	float kernel[9]
	{
		2,2,2,
		2,-15,2,
		2,2,2
	};

	const char* skyboxFaces[6]
	{
		"resource/textures/skybox/SkyMorning_Right.png",
		"resource/textures/skybox/SkyMorning_Left.png",
		"resource/textures/skybox/SkyMorning_Top.png",
		"resource/textures/skybox/SkyMorning_Bottom.png",
		"resource/textures/skybox/SkyMorning_Back.png",
		"resource/textures/skybox/SkyMorning_Front.png"		
	};

	if (StandardRenderer::initialize("shader/shader.vert", "shader/shader.frag") == false ||
		UnlitRenderer::initialize("shader/unlitShader.vert", "shader/unlitShader.frag") == false ||
		PostProcessor::initialize("shader/postProcess.vert", "shader/postProcess.frag",
			1.0f, QMatrix3x3(kernel), QVector3D()) == false ||

		Skybox::initialize("shader/skybox.vert", "shader/skybox.frag", skyboxFaces) == false ||
		ReflectionProbe::initialize(256) == false ||
		DirectionLight::initialize("shader/shadowMap.vert", "shader/shadowMap.frag", 1024*4, 10.0f) == false ||
		PointLight::initialize("shader/shadowCubemap.vert", "shader/shadowCubemap.geom", "shader/shadowCubemap.frag", 512) == false ||
		SpotLight::initialize(256) == false ||

		windowTexture.create("resource/textures/glass.png") == false ||
		windowSpecular.create("resource/textures/glass specular.png") == false ||
		normalPhotoTexture.create("resource/textures/Marc_Dekamps.ppm") == false ||
		bigPhotoTexture.create("resource/textures/Marc_Dekamps cutout.png") == false ||
		lightBulbEmissiveRed.create("resource/meshes/lightbulb/lightbulb emissive red.png") == false ||
		lightBulbEmissiveGreen.create("resource/meshes/lightbulb/lightbulb emissive green.png") == false ||
		lightBulbEmissiveBlue.create("resource/meshes/lightbulb/lightbulb emissive blue.png") == false ||		

		globeModel.loadModel("resource/meshes/globe/globe.obj") == false ||
		globeStandModel.loadModel("resource/meshes/globe/globe stand.obj") == false ||
		globeHolderModel.loadModel("resource/meshes/globe/globe holder.obj") == false ||

		isosphereModel.loadModel("resource/meshes/icosahedron/icosahedron.obj", 0.5) == false ||

		monitorModel.loadModel("resource/meshes/monitor/monitor.obj") == false ||
		personModel.loadModel("resource/meshes/person/lowpoly person.obj") == false ||
		shieldModel.loadModel("resource/meshes/shield/heater shield.obj") == false ||
		swordModel.loadModel("resource/meshes/sword/arming sword.obj") == false ||
		lightBulbModel.loadModel("resource/meshes/lightbulb/lightbulb.obj") == false ||
		spotLightModel.loadModel("resource/meshes/spotlight/spotlight.obj") == false)
	{
		exit(EXIT_FAILURE);
		return;
	}

	initializeObjects();

	profile->glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	// wireframe mode
	// profile->glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	profile->glEnable(GL_DEPTH_TEST);
	profile->glEnable(GL_CULL_FACE);
	profile->glCullFace(GL_BACK);
	profile->glFrontFace(GL_CCW);
	profile->glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	scene.start();
}

void MyGLWidget::resizeGL(int w, int h)
{
	if (w == 0 || h == 0)
		return;

	profile->glViewport(0, 0, w, h);
	camera.setProjection(70, w / (float)h);
	reverseCamera.setProjection(70, 1.0f);

	if (PostProcessor::viewportUpdate(w, h) == false)
		qCritical("PostProcessor : viewport cannot adjust to new dimensions %i x %i", w, h);

	if (reverseFramebuffer.create(w, h) == false)
		qCritical("reverse framebuffer : cannot create framebuffer to dimensions %i x %i", w, h);
}

void MyGLWidget::paintGL()
{
	// update behaviour
	QVector3D move;
	if (inputManager.isKeyPressed(Qt::Key_W))
		move += cameraTransform.getFlatForward();
	if (inputManager.isKeyPressed(Qt::Key_S))
		move -= cameraTransform.getFlatForward();
	if (inputManager.isKeyPressed(Qt::Key_A))
		move -= cameraTransform.getRight();
	if (inputManager.isKeyPressed(Qt::Key_D))
		move += cameraTransform.getRight();
	if (inputManager.isKeyPressed(Qt::Key_Space))
		move += QVector3D(0.0f, 1.0f, 0.0f);
	if (inputManager.isKeyPressed(Qt::Key_Shift))
		move -= QVector3D(0.0f, 1.0f, 0.0f);
	cameraTransform.setPosition(cameraTransform.getPosition() + move.normalized() * 2 * timeManager.deltaTime);

	if (inputManager.isKeyPressed(Qt::LeftButton) && underMouse())
	{
		const float mouseSensitivity = 0.5f;
		int deltaX, deltaY;
		inputManager.getMouseDelta(deltaX, deltaY);
		cameraAngle.setX(qBound(-89.0f, cameraAngle.x() - deltaY * mouseSensitivity, 89.0f));
		cameraAngle.setY(cameraAngle.y() - deltaX * mouseSensitivity);
		cameraTransform.setRotation(MathUtility::eulerToQuaternion(cameraAngle.x(), cameraAngle.y(), 0));
	}

	QVector3D lightPositions[] =
	{
		QVector3D(3.0f * (float)qSin(timeManager.time + 1.2), 1.1f, 3.0f * (float)qCos(timeManager.time + 1.2)),
		QVector3D(1.2f * (float)qSin(timeManager.time), 1.0f, 2.0f * (float)qCos(timeManager.time)),
		QVector3D(2.0f * (float)qCos(timeManager.time), 1.2f, 1.2f * (float)qSin(timeManager.time))		
	};

	for (int i = 0; i < 3; i++)
	{
		pointLights[i]->transform.setPosition(lightPositions[i]);
	}

	// animate globe
	const float globeOrbitsPerSecond = 0.05f;
	globeStand.transform.setLocalPosition(QVector3D(
		3 * qSin((qreal)timeManager.time * globeOrbitsPerSecond * qDegreesToRadians(360.0f)), 0,
		3 * qCos((qreal)timeManager.time * globeOrbitsPerSecond * qDegreesToRadians(360.0f))));
	const float globeHolderRotationsPerSecond = 0.2f;
	globeHolder.transform.setLocalRotation(MathUtility::eulerToQuaternion(0, timeManager.time * globeHolderRotationsPerSecond * 360, 0));
	const float globeRotationsPerSecond = 0.2f;
	QQuaternion yRotation = QQuaternion::fromAxisAndAngle(0.0f, 1.0f, 0.0f, timeManager.time * globeRotationsPerSecond * 360);
	QQuaternion zRotation = QQuaternion::fromAxisAndAngle(0.0f, 0.0f, 1.0f, -45.0f);
	// * yRotation
    globe.transform.setLocalRotation(zRotation * yRotation);

	// update kernel
	if (kernelMode == 3)
	{
		PostProcessor::offsetPixels = 30 * qSin(qDegreesToRadians((qreal)0.1f * 360.0f * timeManager.time));
	}
	else if (kernelMode == 4)
	{
		float kernel[9]
		{
			0,0,0,
			0,qCos(timeManager.time),0,
			0,0,0
		};
		PostProcessor::kernel = QMatrix3x3(kernel);
		PostProcessor::baseColor = QVector3D(1, 1, 1) * (0.5 - 0.5 * qCos(timeManager.time));
	}

	scene.update();
	DirectionLight::updateShadowMaps(width(), height());
	PointLight::updateShadowMaps(width(), height());
	SpotLight::updateShadowMaps(width(), height());
	ReflectionProbe::updateProbes(width(), height());
	Camera::renderAll();
}

void MyGLWidget::initializeObjects()
{
	reverseCamera.setEnabled(false);
	reverseCameraTransform.setLocalPosition(QVector3D(0, 0, 0));
	reverseCameraTransform.setLocalRotation(MathUtility::eulerToQuaternion(0, 180, 0));

	// lights
	Material* lightBulbMaterial;
	lightBulbModel.getMeshMaterial(0, nullptr, &lightBulbMaterial);	
	QVector3D pointColors[3] =
	{
		QVector3D(1.0, 0.0, 0.0),
		QVector3D(0.0, 1.0, 0.0),
		QVector3D(0.0, 0.0, 1.0)
	};
	Material* bulbMaterials[3] =
	{
		&lightBulbRed,
		&lightBulbGreen,
		&lightBulbBlue
	};
	Texture* bulbEmissives[3] =
	{
		&lightBulbEmissiveRed,
		&lightBulbEmissiveGreen,
		&lightBulbEmissiveBlue
	};
	for (int i = 0; i < 3; i++)
	{
		(*bulbMaterials[i]) = *lightBulbMaterial;
		bulbMaterials[i]->emission = MaterialComponent<QVector3D>(bulbEmissives[i]);
		lightBulbModel.getMeshMaterial(0, &pointLights[i]->renderer.mesh, nullptr);
		pointLights[i]->renderer.material = bulbMaterials[i];
		pointLightComponents[i]->lightData.components.ambient = pointColors[i] * 0.05f;
		pointLightComponents[i]->lightData.components.diffuse = pointColors[i];
		pointLightComponents[i]->lightData.components.specular = pointColors[i];
		//pointLightComponents[i]->setEnabled(false);
		//StandardRenderer::removePointLight(&pointLightComponents[i]->lightData);
	}	

	spotLight.transform.setPosition(QVector3D(0.0f, 3.0f, 4.0f));
	spotLight.transform.setRotation(
		MathUtility::lookAt(-QVector3D(0.0f, -3.0f, -4.0f), QVector3D(0.0f, 1.0f, 0.0f)));
	spotLightModel.getMeshMaterial(0, &spotLight.renderer.mesh, &spotLight.renderer.material);
	spotLightComponent.lightData.components.ambient = QVector3D();
	spotLightEmissive = spotLight.renderer.material->emission.data.texture;

	directionLight.transform.setPosition(QVector3D(0, 0, 0));
	directionLight.transform.setRotation(
		MathUtility::lookAt(-QVector3D(1, -0.5, 0), QVector3D(0.0f, 1.0f, 0.0f)));
	directionLightComponent.lightData.components.ambient = QVector3D(0.3, 0.3, 0.28);
	directionLightComponent.lightData.components.diffuse = QVector3D(0.5, 0.5, 0.5 * 207 / 255.0f);
	directionLightComponent.lightData.components.specular = QVector3D(1, 1, 207 / 255.0f);

	reverseMesh.setAsQuad(0.5f, true);
	reverseMirror.sceneObject.setParent(&cameraObject);
	reverseMirror.sceneObject.setSelfEnabled(false);
	reverseMirror.transform.setLocalPosition(QVector3D(0, 0.4f, -0.8f));
	reverseMirror.transform.setLocalRotation(QQuaternion());
	reverseMirror.renderer.mesh = &reverseMesh;
	reverseMirror.renderer.unlitData = MaterialComponent<QVector3D>(
		&reverseFramebuffer.getAttachment());

	// windows
	windowMaterial.isTransparent = true;
	windowMaterial.diffuse = MaterialComponent<QVector4D>(&windowTexture);
	windowMaterial.specular = MaterialComponent<QVector3D>(&windowSpecular);
	windowMesh.setAsQuad(1.0f, true);
	QVector3D windowPositions[5] =
	{
		QVector3D(-1.5f, 0.5f, 0.5f),
		QVector3D(1.5f, 0.5f, 1.2f),
		QVector3D(2.0f, 0.5f, 2.7f),
		QVector3D(0.3f, 0.5f, 2.3f),
		QVector3D(-0.3f, 0.5f, 0.6f) };
	QQuaternion windowRotations[5] =
	{
		MathUtility::eulerToQuaternion(0,-10,0),
		MathUtility::eulerToQuaternion(0,-20,0),
		MathUtility::eulerToQuaternion(0,0,0),
		MathUtility::eulerToQuaternion(0,10,0),
		MathUtility::eulerToQuaternion(0,20,0) };
	for (int i = 0; i < 5; i++)
	{
		windows[i]->transform.setPosition(windowPositions[i]);
		windows[i]->transform.setRotation(windowRotations[i]);
		windows[i]->renderer.material = &windowMaterial;
		windows[i]->renderer.mesh = &windowMesh;
	}
	windowReflectionProbe1.position = windowPositions[0];
	windowReflectionProbe2.position = windowPositions[3];

	floorMaterial.diffuse = 
		MaterialComponent<QVector4D>(QVector4D(0.3,0.3,0.3,1.0));
	floorMesh.setAsQuad(10.0f, true);
	floor.renderer.material = &floorMaterial;
	floor.renderer.mesh = &floorMesh;
	floor.transform.setLocalRotation(MathUtility::eulerToQuaternion(-90, 0, 0));

	normalPhotoMaterial.diffuse = 
		MaterialComponent<QVector4D>(&normalPhotoTexture);
	normalPhotoMesh.setAsQuad(1.0f, true);
	normalPhoto.renderer.material = &normalPhotoMaterial;
	normalPhoto.renderer.mesh = &normalPhotoMesh;
	normalPhoto.transform.setLocalRotation(MathUtility::eulerToQuaternion(0, 80, 0));
	normalPhoto.transform.setLocalPosition(QVector3D(-3, 0.5, -3));

	bigPhotoMaterial.diffuse =
		MaterialComponent<QVector4D>(&bigPhotoTexture);
	bigPhotoMaterial.emission =
		MaterialComponent<QVector3D>(&bigPhotoTexture);
	bigPhotoMaterial.alphaCutoff = 0.5f;
	bigPhotoMesh.setAsQuad(40.0f, false);
	bigPhoto.renderer.material = & bigPhotoMaterial;
	bigPhoto.renderer.mesh = &bigPhotoMesh;
	bigPhoto.transform.setLocalRotation(MathUtility::eulerToQuaternion(0, 90, 0));
	bigPhoto.transform.setLocalPosition(QVector3D(-90, 30, 0));

	globeModel.getMeshMaterial(0, &globe.renderer.mesh, &globe.renderer.material);
	globe.sceneObject.setParent(&globeHolder.sceneObject);
	globe.transform.setLocalPosition(QVector3D(0, 0.282351, 0));
	globe.transform.setLocalRotation(MathUtility::eulerToQuaternion(0, 0, -45));

	globeStandModel.getMeshMaterial(0, &globeStand.renderer.mesh, nullptr);
	globeStand.transform.setLocalPosition(QVector3D(-2, 0, -4));
	globeStand.transform.setLocalRotation(QQuaternion());
	globeStand.renderer.material = &globeStandMaterial;

	globeHolderModel.getMeshMaterial(0, &globeHolder.renderer.mesh, nullptr);
	globeHolder.sceneObject.setParent(&globeStand.sceneObject);
	globeHolder.transform.setLocalPosition(QVector3D());
	globeHolder.transform.setLocalRotation(QQuaternion());
	globeHolder.renderer.material = &globeStandMaterial;

	isosphereMaterial.diffuse = MaterialComponent<QVector4D>(QVector4D(0.5, 0.5, 0.5,1));
	isosphereMaterial.specular = MaterialComponent<QVector3D>(QVector3D(1,1,1));
	isosphereModel.getMeshMaterial(0, &isosphere.renderer.mesh, nullptr);
	isosphere.transform.setLocalPosition(QVector3D(0, 0.5, -4.2));
	isosphere.renderer.material = &isosphereMaterial;
	isosphereReflectionProbe.position = isosphere.transform.getPosition();

	monitorModel.getMeshMaterial(0, &monitor.renderer.mesh, &monitor.renderer.material);
	monitor.transform.setLocalPosition(QVector3D(2, 0, -4));
	monitor.transform.setLocalRotation(MathUtility::eulerToQuaternion(0, -30, 0));
	monitorReflectionProbe.position = monitor.transform.getPosition() + QVector3D(0,0.3,0);

	personModel.getMeshMaterial(0, &person.renderer.mesh, &person.renderer.material);
	person.transform.setLocalPosition(QVector3D(4, 0, -2));
	person.transform.setLocalRotation(MathUtility::eulerToQuaternion(0, -60, 0));

	shieldModel.getMeshMaterial(0, &shield.renderer.mesh, &shield.renderer.material);
	shield.transform.setLocalPosition(QVector3D(4, 0, -0.5));
	shield.transform.setLocalRotation(MathUtility::eulerToQuaternion(0, 90, 0));

	swordModel.getMeshMaterial(0, &sword.renderer.mesh, &sword.renderer.material);
	sword.transform.setLocalPosition(QVector3D(3.8, 0.2, 1));
	sword.transform.setLocalRotation(MathUtility::eulerToQuaternion(0, -100, 0));

	weaponReflectionProbe.position = QVector3D(3.9, 0.7, 0.3);

	for (int i = 0; i < outlineObjectCount; ++i)
	{
		allOutlineObjects[i]->renderer.outlineEnabled = false;
		allOutlineObjects[i]->renderer.outlineColor = QVector3D(
			rand() % 256 / 255.0f, rand() % 256 / 255.0f, rand() % 256 / 255.0f);
	}
}