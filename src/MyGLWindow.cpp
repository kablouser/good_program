#include "MyGLWindow.h"
#include "math.h"
#include <QApplication>
#include <QGLFormat>
#include <QSlider>

MyGLWindow::MyGLWindow() :
	QWidget(),
	myGLWidget(this, inputManager, timeManager),
	horizontalLayout(this),
	verticalLayout(this),
	reflectionGroupBox("Reflections"),
	lightGroupBox("Lights"),
	bottomBox("Others"),
	reflectionSlider(Qt::Horizontal),
	directionLightSlider(Qt::Horizontal),
	redBulbSlider(Qt::Horizontal),
	greenBulbSlider(Qt::Horizontal),
	blueBulbSlider(Qt::Horizontal),
	spotLightVolumeSlider(Qt::Horizontal),
	spotLightRadiusSlider(Qt::Horizontal),
	outlineSlider(Qt::Horizontal),
	inputManager(this, false)
{
	horizontalLayout.addWidget(&myGLWidget);
	horizontalLayout.addLayout(&verticalLayout, 0);
	verticalLayout.setAlignment(Qt::AlignTop);
	verticalLayout.addWidget(&instructions);	
	verticalLayout.addWidget(&reflectionGroupBox);
	verticalLayout.addWidget(&lightGroupBox);
	verticalLayout.addWidget(&bottomBox);
	verticalLayout.addWidget(&fpsLabel);

	instructions.setText("WASD = move, Spacebar = ascend, Shift = descend,\nClick+Drag on 3D viewport = rotate camera, Esc = close");
	instructions.setWordWrap(true);

	reflectionGroupBox.setFixedWidth(300);
	reflectionGroupBox.setLayout(&reflectionLayout);
	reflectionLayout.addRow(&reflectionCheckBoxLabel, &reflectionCheckBox);
	reflectionLayout.addRow(&reflectionSliderLabel, &reflectionSlider);
	reflectionLayout.addRow(nullptr, &reflectionSliderLegend);
	reflectionSliderLegend.addWidget(&reflectionSliderLegendLeft);
	reflectionSliderLegend.addStretch();
	reflectionSliderLegend.addWidget(&reflectionSliderLegendRight);
	reflectionCheckBoxLabel.setText("Enabled");
	reflectionSliderLabel.setText("Update frequency");
	reflectionSlider.setMinimum(0);
	reflectionSlider.setMaximum(30);
	reflectionSlider.setTickInterval(2);
	reflectionSlider.setTickPosition(QSlider::TicksBelow);
	reflectionSlider.setValue(ReflectionProbe::probesPerFrame);
	reflectionSliderLegendLeft.setText("never");
	reflectionSliderLegendRight.setText("every frame");

	connect(
		&reflectionCheckBox, &QCheckBox::stateChanged,
		[=](int state) { myGLWidget.setReflection(state == Qt::Checked); } );
	connect(
		&reflectionSlider, &QSlider::valueChanged,
		[=](int value) { myGLWidget.setReflectionUpdateFrequency(value); } );

	lightGroupBox.setFixedWidth(300);
	lightGroupBox.setLayout(&lightLayout);
	lightLayout.addRow(&directionLightLabel, &row1);
	lightLayout.addRow(&redBulbLabel, &row2);
	lightLayout.addRow(&greenBulbLabel, &row3);
	lightLayout.addRow(&blueBulbLabel, &row4);
	lightLayout.addRow(&spotLightLabel, &row5);
	lightLayout.addRow(&spotLightRadiusLabel, &spotLightRadiusSlider);
	row1.addWidget(&directionLightCheckBox);
	row1.addWidget(&directionLightSlider);
	row2.addWidget(&redBulbCheckBox);
	row2.addWidget(&redBulbSlider);
	row3.addWidget(&greenBulbCheckBox);
	row3.addWidget(&greenBulbSlider);
	row4.addWidget(&blueBulbCheckBox);
	row4.addWidget(&blueBulbSlider);
	row5.addWidget(&spotLightCheckBox);
	row5.addWidget(&spotLightButton);
	row5.addWidget(&spotLightVolumeSlider);
	directionLightLabel.setText("Sunlight");
	redBulbLabel.setText("Red bulb");
	greenBulbLabel.setText("Green bulb");
	blueBulbLabel.setText("Blue bulb");
	spotLightLabel.setText("Spotlight");
	spotLightButton.setText("Point at me");
	spotLightRadiusLabel.setText("Spotlight radius");
	//spotLightButton.setFixedWidth(70);
	directionLightSlider.setMinimum(0);
	directionLightSlider.setMaximum(10);
	directionLightSlider.setTickInterval(1);
	directionLightSlider.setTickPosition(QSlider::TicksBelow);
	directionLightSlider.setValue(10);
	redBulbSlider.setMinimum(0);
	redBulbSlider.setMaximum(10);
	redBulbSlider.setTickInterval(1);
	redBulbSlider.setTickPosition(QSlider::TicksBelow);
	redBulbSlider.setValue(10);
	greenBulbSlider.setMinimum(0);
	greenBulbSlider.setMaximum(10);
	greenBulbSlider.setTickInterval(1);
	greenBulbSlider.setTickPosition(QSlider::TicksBelow);
	greenBulbSlider.setValue(10);
	blueBulbSlider.setMinimum(0);
	blueBulbSlider.setMaximum(10);
	blueBulbSlider.setTickInterval(1);
	blueBulbSlider.setTickPosition(QSlider::TicksBelow);
	blueBulbSlider.setValue(10);	
	spotLightVolumeSlider.setMinimum(0);
	spotLightVolumeSlider.setMaximum(10);
	spotLightVolumeSlider.setTickInterval(1);
	spotLightVolumeSlider.setTickPosition(QSlider::TicksBelow);
	spotLightVolumeSlider.setValue(10);
	spotLightRadiusSlider.setMinimum(0);
	spotLightRadiusSlider.setMaximum(90);
	spotLightRadiusSlider.setTickInterval(10);
	spotLightRadiusSlider.setTickPosition(QSlider::TicksBelow);
	spotLightRadiusSlider.setValue(20);
	directionLightCheckBox.setChecked(true);
	redBulbCheckBox.setChecked(true);
	greenBulbCheckBox.setChecked(true);
	blueBulbCheckBox.setChecked(true);
	spotLightCheckBox.setChecked(true);

	connect(
		&directionLightCheckBox, &QCheckBox::stateChanged,
		[=](int state) { myGLWidget.setDirectionLight(state == Qt::Checked); });
	connect(
		&directionLightSlider, &QSlider::valueChanged,
		[=](int value) { myGLWidget.setDirectionLightBrightness(value / 10.0f); });
	connect(
		&redBulbCheckBox, &QCheckBox::stateChanged,
		[=](int state) { myGLWidget.setRedLight(state == Qt::Checked); });
	connect(
		&redBulbSlider, &QSlider::valueChanged,
		[=](int value) { myGLWidget.setRedLightBrightness(value / 10.0f); });
	connect(
		&greenBulbCheckBox, &QCheckBox::stateChanged,
		[=](int state) { myGLWidget.setGreenLight(state == Qt::Checked); });
	connect(
		&greenBulbSlider, &QSlider::valueChanged,
		[=](int value) { myGLWidget.setGreenLightBrightness(value / 10.0f); });
	connect(
		&blueBulbCheckBox, &QCheckBox::stateChanged,
		[=](int state) { myGLWidget.setBlueLight(state == Qt::Checked); });
	connect(
		&blueBulbSlider, &QSlider::valueChanged,
		[=](int value) { myGLWidget.setBlueLightBrightness(value / 10.0f); });
	connect(
		&spotLightCheckBox, &QCheckBox::stateChanged,
		[=](int state) { myGLWidget.setSpotLight(state == Qt::Checked); });
	connect(
		&spotLightButton, &QPushButton::pressed,
		[=]() { myGLWidget.pointSpotLight(); });
	connect(
		&spotLightVolumeSlider, &QSlider::valueChanged,
		[=](int value) { myGLWidget.setSpotLightBrightness(value / 10.0f); });
	connect(
		&spotLightRadiusSlider, &QSlider::valueChanged,
		[=](int value) { myGLWidget.setSpotLightAngle(value); });

	bottomBox.setFixedWidth(300);
	bottomBox.setLayout(&bottomForm);

	bottomForm.addRow(&outlineLabel, &outlineRow);
	outlineRow.setAlignment(Qt::AlignLeft);
	outlineRow.addWidget(&outlineCheckBox);
	outlineRow.addWidget(&outlineSlider);	
	outlineLabel.setText("Outline");
	outlineSlider.setValue(5);
	outlineSlider.setMinimum(0);
	outlineSlider.setMaximum(15);
	outlineSlider.setTickInterval(1);
	outlineSlider.setTickPosition(QSlider::TicksBelow);

	connect(
		&outlineCheckBox, &QCheckBox::stateChanged,
		[=](int state) { myGLWidget.setOutline(state == Qt::Checked); });
	connect(
		&outlineSlider, &QSlider::valueChanged,
		[=](int value) { myGLWidget.setOutlineScale(1.0f + value / 100.0f); });

	bottomForm.addRow(&reverseCamLabel, &reverseCamCheckBox);
	reverseCamLabel.setText("Reverse camera");

	connect(
		&reverseCamCheckBox, &QCheckBox::stateChanged,
		[=](int state) { myGLWidget.setReverseCamera(state == Qt::Checked); });

	bottomForm.addRow(&kernelLabel, &kernelRow);
	kernelRow.setAlignment(Qt::AlignLeft);
	kernelRow.addWidget(&kernelCheckBox);
	kernelRow.addWidget(&kernelComboBox);
	kernelLabel.setText("Post-processing kernel");
	kernelComboBox.addItem("Sharpen");
	kernelComboBox.addItem("Blur");
	kernelComboBox.addItem("Edge detection");
	kernelComboBox.addItem("Double vision");
	kernelComboBox.addItem("Inverting");

	connect(
		&kernelCheckBox, &QCheckBox::stateChanged,
		[=](int state) { myGLWidget.setKernel(state == Qt::Checked); });
	void (QComboBox::* mySignal)(int) = &QComboBox::activated;
	connect(
		&kernelComboBox, mySignal,
		[=](int index) { myGLWidget.setKernelMatrix(index); });

	connect(&updateTimer, SIGNAL(timeout()), this, SLOT(update()));
	updateTimer.start(0);
}

void MyGLWindow::update()
{
	//QWidget* focused = QApplication::focusWidget();
	//if (focused != nullptr)
	//	focused->clearFocus();

	fpsLabel.setText("fps = " + QString::number(round(1.0f / timeManager.deltaTime)));

	timeManager.update();
	inputManager.update();
	myGLWidget.updateGL();
}

// input manager stuff
void MyGLWindow::keyPressEvent(QKeyEvent* event)
{
	inputManager.keyPressEvent(event);
	if (inputManager.isKeyPressed(Qt::Key_Escape))
		exit(0);
}
void MyGLWindow::keyReleaseEvent(QKeyEvent* event)
{
	inputManager.keyReleaseEvent(event);
}
void MyGLWindow::mousePressEvent(QMouseEvent* event)
{
	inputManager.mousePressEvent(event);
}
void MyGLWindow::mouseReleaseEvent(QMouseEvent* event)
{
	inputManager.mouseReleaseEvent(event);
}
void MyGLWindow::focusInEvent(QFocusEvent* event)
{
	inputManager.update();
}
