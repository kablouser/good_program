#include "OpenGLProfile.h"

OpenGLProfile::OpenGLProfile()
{
	if (current == nullptr)
	{
		current = this;
	}
	else
	{
		qCritical("OpenGLProfile : profile already exists");
	}
}

OpenGLProfile::~OpenGLProfile()
{
	if (current == this)
	{
		current = nullptr;
	}
}

OpenGLProfile* OpenGLProfile::current = nullptr;
