#include "OutlineRenderer.h"
#include "UnlitRenderer.h"

OutlineRenderer::OutlineRenderer(
	SceneObject* parent,
	Mesh* mesh, Material* material,
	QVector3D color, bool scaleAlongNormals, float scale) :
	StandardRenderer(parent, mesh, material),
	outlineEnabled(true), outlineColor(color), outlineAlongNormals(scaleAlongNormals), outlineScale(scale) {}

void OutlineRenderer::renderActual(const QMatrix4x4& projection, const QMatrix4x4& view, bool isReflectionEnabled)
{
	if (outlineEnabled)
	{
		OpenGLProfile::getCurrent()->glEnable(GL_STENCIL_TEST);
		OpenGLProfile::getCurrent()->glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
		OpenGLProfile::getCurrent()->glStencilFunc(GL_ALWAYS, 1, 0xFF); // all fragments should pass the stencil test
		OpenGLProfile::getCurrent()->glStencilMask(0xFF); // enable writing to the stencil buffer

		// render model normally
		StandardRenderer::renderActual(projection, view, isReflectionEnabled);

		OpenGLProfile::getCurrent()->glStencilFunc(GL_NOTEQUAL, 1, 0xFF);
		OpenGLProfile::getCurrent()->glStencilMask(0x00); // disable writing to the stencil buffer

		// draw model upscaled
		UnlitRenderer::setShaderParameters(
			projection, view, transform, true, outlineAlongNormals, outlineScale,
			MaterialComponent<QVector3D>(outlineColor), isReflectionEnabled);
		mesh->draw();

		OpenGLProfile::getCurrent()->glStencilFunc(GL_ALWAYS, 1, 0xFF);
		OpenGLProfile::getCurrent()->glStencilMask(0xFF);
		OpenGLProfile::getCurrent()->glDisable(GL_STENCIL_TEST);
	}
	else
	{
		// render model without outline
		StandardRenderer::renderActual(projection, view, isReflectionEnabled);
	}
}
