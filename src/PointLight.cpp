#include "PointLight.h"
#include "CubemapVectors.h"

PointLight::PointLight(SceneObject* parent, QVector3D color) :
    GenericLight(parent)
{
	lightData.position = QVector3D();
	lightData.components.ambient = QVector3D(0, 0, 0);
	lightData.components.diffuse = color;
	lightData.components.specular = color;
	lightData.scaling.constant = 1.0f;
	lightData.scaling.linear = 0.09f; 	
	lightData.scaling.quadratic = 0.032f;
	allPointLights.insert(this);
}

PointLight::~PointLight()
{
	allPointLights.erase(this);
}

bool PointLight::initialize(
	const char* vertexShaderPath, 
	const char* geometryShaderPath, 
	const char* fragmentShaderPath,
	int shadowCubemapResolution,
	float nearPlane,
	float farPlane)
{
	bool success =
		shadowCubemapShader.compileFiles(
			vertexShaderPath, geometryShaderPath, fragmentShaderPath) &&
		shadowCubemapFramebuffer.createDepthOnly3D(
			shadowCubemapResolution, shadowCubemapResolution) &&
		shadowCubemapShader.checkUniformId("model", modelId) &&
		shadowCubemapShader.checkUniformId("lightPosition", lightPositionId) &&
		shadowCubemapShader.checkUniformId("farPlane", farPlaneId) &&
		shadowCubemapShader.checkUniformId("viewToProjections[0]", viewToProjectionIds[0]) &&
		shadowCubemapShader.checkUniformId("viewToProjections[1]", viewToProjectionIds[1]) &&
		shadowCubemapShader.checkUniformId("viewToProjections[2]", viewToProjectionIds[2]) &&
		shadowCubemapShader.checkUniformId("viewToProjections[3]", viewToProjectionIds[3]) &&
		shadowCubemapShader.checkUniformId("viewToProjections[4]", viewToProjectionIds[4]) &&
		shadowCubemapShader.checkUniformId("viewToProjections[5]", viewToProjectionIds[5]);

	if (success)
	{
		PointLight::shadowCubemapResolution = shadowCubemapResolution;
		PointLight::nearPlane = nearPlane;
		PointLight::farPlane = farPlane;
		StandardRenderer::setPointLightFarPlane(farPlane);
	}
	return success;
}

void PointLight::updateShadowMaps(int viewportWidth, int viewportHeight)
{
	shadowCubemapShader.use();

	OpenGLProfile::getCurrent()->glViewport(0, 0, shadowCubemapResolution, shadowCubemapResolution);

	QMatrix4x4 projection;
	projection.perspective(90.0f, 1.0f, nearPlane, farPlane);
	QMatrix4x4 viewToProjection;	

	for (auto i = allPointLights.begin(); i != allPointLights.end(); ++i)
	{
		if ((*i)->getExecutable())
		{
			QVector3D lightPosition = (*i)->transform->getPosition();
			for (int j = 0; j < 6; ++j)
			{
				viewToProjection = projection;
				viewToProjection.lookAt(lightPosition, lightPosition + CubemapForwards[j], CubemapUpwards[j]);
				shadowCubemapShader.setUniformMatrix(viewToProjectionIds[j], viewToProjection);
			}

			shadowCubemapShader.setUniform3f(lightPositionId, lightPosition);
			shadowCubemapShader.setUniformFloat(farPlaneId, farPlane);
			
			shadowCubemapFramebuffer.attachExternalDepth3D((*i)->shadowMap);
			OpenGLProfile::getCurrent()->glClear(GL_DEPTH_BUFFER_BIT);

			Renderer::foreachRenderer([=](Renderer* renderer)
			{
				// set model matrix
				shadowCubemapShader.setUniformMatrix(modelId, renderer->getModelMatrix());
				renderer->draw();
			}
			);
		}
	}

	Framebuffer::bindDefault();
	OpenGLProfile::getCurrent()->glViewport(0, 0, viewportWidth, viewportHeight);
}

void PointLight::updateLightData()
{
	StandardRenderer::setPointLight(&lightData);
}

void PointLight::setEnabled(bool isEnabled)
{
	if (getEnabled() == isEnabled)
		return;

	if (isEnabled)
		updateLightData();
	else
		StandardRenderer::removePointLight(&lightData);

	SceneComponent::setEnabled(isEnabled);
}

void PointLight::initializeLight()
{
	lightData.position = transform->getPosition();
	lightData.shadowMap = &shadowMap;
	shadowMap.createRaw3D(shadowCubemapResolution, shadowCubemapResolution,
		GL_CLAMP_TO_EDGE, GL_LINEAR, GL_LINEAR, GL_DEPTH_COMPONENT);
}

bool PointLight::hasLightChanged()
{
	QVector3D newPosition = transform->getPosition();
	if (qFuzzyCompare(lightData.position, newPosition) == false)
	{
		lightData.position = newPosition;
		return true;
	}
	else
	{
		return false;
	}
}

std::set<PointLight*> PointLight::allPointLights;
ShaderProgram PointLight::shadowCubemapShader;
Framebuffer PointLight::shadowCubemapFramebuffer;
int PointLight::shadowCubemapResolution = 0.0f;
float PointLight::nearPlane = 0.0f, PointLight::farPlane = 0.0f;
int PointLight::modelId = -1, PointLight::lightPositionId = -1, PointLight::farPlaneId = -1;
int PointLight::viewToProjectionIds[6]{ -1, -1, -1, -1, -1, -1 };