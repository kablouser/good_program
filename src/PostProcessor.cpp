#include "PostProcessor.h"
#include "Renderer.h"

bool PostProcessor::initialize(
	const char* vertexShaderPath, const char* fragmentShaderPath,
	float offsetPixels,
	QMatrix3x3 kernel,
	QVector3D baseColor)
{
	if (shaderProgram.compileFiles(vertexShaderPath, fragmentShaderPath) &&
		shaderProgram.checkUniformId("screenTexture", screenTextureId) &&
		shaderProgram.checkUniformId("offsetPixels", offsetPixelsId) &&
		shaderProgram.checkUniformId("kernel", kernelId) &&
		shaderProgram.checkUniformId("baseColor", baseColorId))
	{
		overlayMesh.setAsQuad(2.0f);
		PostProcessor::offsetPixels = offsetPixels;
		PostProcessor::kernel = kernel;
		PostProcessor::baseColor = baseColor;
		return true;
	}
	return false;
}

void PostProcessor::renderAll(
	const QMatrix4x4& projection,
	const QMatrix4x4& view,
	const QVector3D& cameraPosition,
	Framebuffer* outputTarget, QVector4D backgroundColor,
	bool isReflectionEnabled)
{
	Renderer::renderAll(projection, view, cameraPosition, &framebuffer, backgroundColor, isReflectionEnabled);

	if (outputTarget == nullptr)
		Framebuffer::bindDefault();
	else
		outputTarget->bind();
	OpenGLProfile::getCurrent()->glClearColor(
		backgroundColor.x(),
		backgroundColor.y(),
		backgroundColor.z(),
		backgroundColor.w());
	OpenGLProfile::getCurrent()->glClear(GL_COLOR_BUFFER_BIT);

	OpenGLProfile::getCurrent()->glDisable(GL_DEPTH_TEST);
	shaderProgram.use();
	shaderProgram.setTextureUnit(&framebuffer.getAttachment(), screenTextureId, 0);
	shaderProgram.setUniformFloat(offsetPixelsId, offsetPixels);
	shaderProgram.setUniformMatrix(kernelId, kernel);
	shaderProgram.setUniform3f(baseColorId, baseColor);

	overlayMesh.draw();

	OpenGLProfile::getCurrent()->glEnable(GL_DEPTH_TEST);
}

bool PostProcessor::viewportUpdate(int screenWidth, int screenHeight)
{
	return framebuffer.create(screenWidth, screenHeight);
}

float PostProcessor::offsetPixels;
QMatrix3x3 PostProcessor::kernel;
QVector3D PostProcessor::baseColor;

float PostProcessor::defaultKernel[9]
{
	0.0f, 0.0f, 0.0f,
	0.0f, 1.0f, 0.0f,
	0.0f, 0.0f, 0.0f
};
ShaderProgram PostProcessor::shaderProgram;
Framebuffer PostProcessor::framebuffer;
Mesh PostProcessor::overlayMesh;
int 
	PostProcessor::screenTextureId = -1, 
	PostProcessor::offsetPixelsId = -1,
	PostProcessor::kernelId = -1,
	PostProcessor::baseColorId = -1;
