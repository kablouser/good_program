#include "ReflectionProbe.h"
#include "Skybox.h"
#include "StandardRenderer.h"
#include "CubemapVectors.h"
using namespace std;

bool ReflectionProbe::initialize(int probeResolution)
{
	if (framebuffer.create(probeResolution, probeResolution))
	{
		ReflectionProbe::probeResolution = probeResolution;
		return true;
	}
	return false;
}

void ReflectionProbe::updateProbes(int viewportWidth, int viewportHeight)
{
	if (allReflectionProbes.size() == 0)
		return;

	auto setIterator = allReflectionProbes.begin();
	for (int i = 0; (i*6+5) < updateIterator; ++i)
		++setIterator;

	bool tooManyProbes = probesPerFrame < allReflectionProbes.size() * 6;
	int maxIterations = tooManyProbes ? probesPerFrame : allReflectionProbes.size() * 6;
	OpenGLProfile::getCurrent()->glViewport(0, 0, probeResolution, probeResolution);
	QMatrix4x4 projection, view;
	projection.perspective(90.0f, 1.0f, 0.1f, 100.0f);
	do
	{
		Texture& cubemap = (*setIterator)->cubemap;
		if (cubemap.isValid() == false)
		{
			cubemap.createRaw3D(probeResolution, probeResolution);
		}
		for (int i = updateIterator % 6; i < 6 && 0 < maxIterations; ++i, --maxIterations)
		{
			view.setToIdentity();
			QVector3D position = (*setIterator)->position;
			QVector3D lookat = (*setIterator)->position + CubemapForwards[i];
			view.lookAt(position, lookat, CubemapUpwards[i]);
			framebuffer.attachExternalFace(cubemap, i);
			Renderer::renderAll(projection, view, position, &framebuffer,
				QVector4D(0.3,0.3,0.3,1), false);
		}
		if (0 < maxIterations)
		{
			++setIterator;
			if (setIterator == allReflectionProbes.end())
				setIterator = allReflectionProbes.begin();
		}
		else
		{
			if (tooManyProbes)
				updateIterator =
				((size_t)updateIterator + probesPerFrame) %
				(allReflectionProbes.size() * 6);
			else
				updateIterator = 0;
			break;
		}
	} while (true);

	OpenGLProfile::getCurrent()->glViewport(0, 0, viewportWidth, viewportHeight);
}

void ReflectionProbe::setEnvironmentCubemap(const QVector3D& position)
{
	Texture* closestTexture = nullptr;
	if (allReflectionProbes.size() == 0)
	{
		closestTexture = &Skybox::getCubemap();
	}
	else
	{
		set<ReflectionProbe*>::iterator iterator = allReflectionProbes.begin();
		set<ReflectionProbe*>::iterator closestIterator = iterator;
		float closestDistanceSquared = ((*closestIterator)->position - position).lengthSquared();
		++iterator;
		for (; iterator != allReflectionProbes.end(); ++iterator)
		{
			if ((*iterator)->cubemap.isValid())
			{
				float distanceSquared = ((*iterator)->position - position).lengthSquared();
				if (distanceSquared < closestDistanceSquared)
				{
					closestIterator = iterator;
				}
			}
		}
		if ((*closestIterator)->cubemap.isValid())
			closestTexture = &(*closestIterator)->cubemap;
		else
			closestTexture = &Skybox::getCubemap();
	}

	StandardRenderer::setEnvironmentCubemap(closestTexture);
}

ReflectionProbe::ReflectionProbe(QVector3D position) :
	position(position)
{
	allReflectionProbes.insert(this);
}

ReflectionProbe::~ReflectionProbe()
{
	allReflectionProbes.erase(this);
}

unsigned int ReflectionProbe::probesPerFrame = 10; // max 30
std::set<ReflectionProbe*> ReflectionProbe::allReflectionProbes;
int
	ReflectionProbe::updateIterator = 0,
	ReflectionProbe::probeResolution = 0;
Framebuffer ReflectionProbe::framebuffer;
