#include "Renderer.h"
#include "StandardRenderer.h"
#include "Skybox.h"

Renderer::Renderer(SceneObject* parent) :
	SceneComponent(parent), transform(nullptr)
{
	allRenderers.insert(this);
}

Renderer::~Renderer()
{
	allRenderers.erase(this);
}

void Renderer::start()
{
	transform = getComponent<Transform>();
	if (transform == nullptr)
		qCritical("Renderer : cannot find transform");
}

void Renderer::renderAll(
	const QMatrix4x4& projection,
	const QMatrix4x4& view,
	const QVector3D& cameraPosition,
	Framebuffer* outputTarget, 
	QVector4D backgroundColor,
	bool isReflectionsEnabled)
{
	if (outputTarget == nullptr)
		Framebuffer::bindDefault();
	else
		outputTarget->bind();

	OpenGLProfile::getCurrent()->glClearColor(
		backgroundColor.x(), 
		backgroundColor.y(), 
		backgroundColor.z(), 
		backgroundColor.w());
	OpenGLProfile::getCurrent()->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	
	StandardRenderer::setReflections(isReflectionsEnabled);

	for (auto i = allRenderers.begin(); i != allRenderers.end(); i++)
	{
		if ((*i)->getExecutable())
		{
			(*i)->render(projection, view, isReflectionsEnabled);
		}
	}
	Skybox::render(projection, view);
	StandardRenderer::renderAllTransparent(projection, view, cameraPosition, isReflectionsEnabled);
}

void Renderer::foreachRenderer(std::function<void(Renderer*)> const& action)
{
	for (auto i = allRenderers.begin(); i != allRenderers.end(); i++)
	{
		if ((*i)->getExecutable())
		{
			action(*i);
		}
	}
}

QMatrix4x4 Renderer::getModelMatrix() const
{
	QMatrix4x4 model;
	model.translate(transform->getPosition());
	model.rotate(transform->getRotation());
	return model;
}

std::set<Renderer*> Renderer::allRenderers;
