#include "Scene.h"
#include <QtGlobal>

Scene::Scene() : allComponents(&compareComponentOrder)
{
	if (current == nullptr)
		current = this;
}

Scene* Scene::current = nullptr;

Scene* Scene::getCurrentScene()
{
	return current;
}

void Scene::addComponent(SceneComponent* component)
{
	if (started)
		allComponents.insert(component);
	else
		unsortedAllComponents.push_back(component);
}

bool Scene::removeComponent(SceneComponent* component)
{
	auto i = allComponents.find(component);
	if (i == allComponents.end())
		return false;
	else
	{
		(*i)->setAttachedObject(nullptr);
		allComponents.erase(i);
		return true;
	}
}

void Scene::addRootObject(SceneObject* rootObject)
{
	rootObjects.insert(rootObject);
}

bool Scene::removeRootObject(SceneObject* rootObject)
{
	for (auto i = rootObjects.begin(); i != rootObjects.end(); i++)
	{
		if (rootObject == *i)
		{
			rootObjects.erase(i);
			return true;
		}
	}
	return false;
}

int Scene::getRootObjectsSize()
{
	return rootObjects.size();
}

std::set<SceneObject*>::iterator Scene::getRootObjectsIterator()
{
	return rootObjects.begin();
}

std::set<SceneObject*>::iterator Scene::getRootObjectsEnd()
{
	return rootObjects.end();
}

void Scene::start()
{
	if (started)
	{
		qCritical("Scene : should never start more than once");
		return;
	}

	started = true;
	allComponents.insert(unsortedAllComponents.begin(), unsortedAllComponents.end());
	unsortedAllComponents.clear();

	update();
}

void Scene::update()
{
	for (auto i = allComponents.begin(); i != allComponents.end();)
	{
		if (*i == nullptr)
		{
			allComponents.erase(i++);
		}
		else if ((*i)->getEnded())
		{
			(*i)->setAttachedObject(nullptr);
			allComponents.erase(i++);
		}
		else if ((*i)->getExecutable())
		{
			if ((*i)->getStarted() == false)
			{
				(*i)->setStarted(true);
				(*i)->start();
			}
			else
			{
				(*i)->update();
			}
			++i;
		}
		else
		{
			++i;
		}
	}
}

bool Scene::compareComponentOrder(SceneComponent* componentA, SceneComponent* componentB)
{
	return *componentA < *componentB;
}
