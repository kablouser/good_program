#include "SceneComponent.h"
#include <QtGlobal>

SceneComponent::SceneComponent(SceneObject* parent) :
	SceneComponent(parent, Scene::getCurrentScene()) { }

SceneComponent::SceneComponent(SceneObject* parent, Scene* scene) :
	isStarted(false), isEnded(false), isEnabled(true)
{
	if (scene != nullptr)
		scene->addComponent(this);
	else
		qWarning("SceneComponent : is not attached to a scene");

	setAttachedObject(parent);
}

SceneObject* SceneComponent::getAttachedObject()
{
	return attachedObject;
}

void SceneComponent::setAttachedObject(SceneObject* sceneObject)
{
	if (attachedObject == sceneObject)
		return;

	if (attachedObject != nullptr)
	{
		SceneObject* oldParent = attachedObject;
		attachedObject = sceneObject;
		oldParent->removeComponent(this);
	}
	else
		attachedObject = sceneObject;

	if (attachedObject != nullptr)
		attachedObject->addComponent(this);
}

bool SceneComponent::getEnded()
{
	return isEnded;
}

void SceneComponent::setEnded(bool isEnded)
{
	this->isEnded = isEnded;
}

bool SceneComponent::getEnabled()
{
	return isEnabled;
}

void SceneComponent::setEnabled(bool isEnabled)
{
	this->isEnabled = isEnabled;
}

bool SceneComponent::getStarted()
{
	return isStarted;
}

void SceneComponent::setStarted(bool isStarted)
{
	this->isStarted = isStarted;
}

bool SceneComponent::getExecutable()
{
	return isEnabled && (attachedObject == nullptr || attachedObject->getEnabled());
}

int SceneComponent::getExecutionOrder() const
{
	return 0;
}

bool SceneComponent::operator<(const SceneComponent& other)
{
	return getExecutionOrder() < other.getExecutionOrder();
}
