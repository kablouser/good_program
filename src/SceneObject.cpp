#include "SceneObject.h"
#include <QtGlobal>

SceneObject::SceneObject(QString name, SceneObject* parent) :
	SceneObject(name, parent, Scene::getCurrentScene()) { }

SceneObject::SceneObject(QString name, SceneObject* parent, Scene* scene) :
	name(name),
	scene(scene),
	isSelfEnabled(true), isInheritedEnabled(false), isEnabled(false)
{
	if (scene == nullptr)
		qWarning("SceneObject : is not attached to a scene");
	else if (parent == nullptr)
		scene->addRootObject(this);

	// this will correct the values of isInheritedEnabled and isEnabled
	// however this only works when THIS HAS NO CHILDREN
	setParent(parent);
}

void SceneObject::setParent(SceneObject* newParent)
{
	SceneObject* traverseNew = newParent;
	while (traverseNew != nullptr)
	{
		if (traverseNew == this)
		{
			qWarning("SceneObject : parent hierachy tree cannot form closed cycle");
			return;
		}
		traverseNew = traverseNew->getParent();
	}

	if (parent != nullptr)
	{
		for (auto i = parent->children.begin(); i != parent->children.end(); i++)
			if (*i == this)
			{
				parent->children.erase(i);
				break;
			}
	}

	bool isOldRoot = parent == nullptr;
	bool isNewRoot = newParent == nullptr;
	SceneObject* oldParent = parent;
	parent = newParent;
	
	if (newParent != nullptr)
	{
		newParent->children.insert(this);
		setInheritedEnabled(newParent->getEnabled());
	}
	else
	{
		setInheritedEnabled(true);
	}

	if (scene != nullptr && isOldRoot != isNewRoot)
	{
		if (isNewRoot)
			scene->addRootObject(this);
		else
			scene->removeRootObject(this);
	}

	// send parent event to components
	for (auto i = components.begin(); i != components.end(); i++)
		(*i)->onParent(oldParent);
}

void SceneObject::addComponent(SceneComponent* component)
{
	if (component != nullptr)
	{
		component->setAttachedObject(this);
		components.insert(component);
	}
}

bool SceneObject::removeComponent(SceneComponent* component)
{
	for (auto i = components.begin(); i != components.end(); i++)
		if (component == *i)
		{
			component->setAttachedObject(nullptr);
			components.erase(i);
			return true;
		}
	return false;
}

bool SceneObject::getSelfEnabled()
{
	return isSelfEnabled;
}

void SceneObject::setSelfEnabled(bool isSelfEnabled)
{
	if (this->isSelfEnabled == isSelfEnabled)
		return;
	this->isSelfEnabled = isSelfEnabled;

	updateEnabled();
}

bool SceneObject::getEnabled()
{
	return isSelfEnabled && isInheritedEnabled;
}

void SceneObject::setInheritedEnabled(bool isInheritedEnabled)
{
	if (this->isInheritedEnabled == isInheritedEnabled)
		return;
	this->isInheritedEnabled = isInheritedEnabled;

	updateEnabled();
}

void SceneObject::updateEnabled()
{
	bool newEnabled = getEnabled();
	if (this->isEnabled == newEnabled)
		return;
	this->isEnabled = newEnabled;

	for (auto i = children.begin(); i != children.end(); i++)
		(*i)->setInheritedEnabled(newEnabled);
}