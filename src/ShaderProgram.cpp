#include "ShaderProgram.h"
#include "OpenGLProfile.h"

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

ShaderProgram::ShaderProgram() : id(-1) { }

ShaderProgram::~ShaderProgram()
{
	if (id != -1 && OpenGLProfile::getCurrent() != nullptr)
		OpenGLProfile::getCurrent()->glDeleteProgram(id);
}

void ShaderProgram::use()
{	
	OpenGLProfile::getCurrent()->glUseProgram(id);
}

bool ShaderProgram::compileFiles(const char* vertexShaderPath, const char* fragmentShaderPath)
{
	std::string vertexString, fragmentString;
	if (readFile(vertexShaderPath, "vertex", vertexString) == false ||
		readFile(fragmentShaderPath, "fragment", fragmentString) == false)
		return false;
	
	const char* vertexCString = vertexString.c_str();
	const char* fragmentCString = fragmentString.c_str();

	return compileStrings(&vertexCString, &fragmentCString);
}

bool ShaderProgram::compileFiles(const char* vertexShaderPath, const char* geometryShaderPath, const char* fragmentShaderPath)
{
	std::string vertexString, geometryString, fragmentString;
	if (readFile(vertexShaderPath, "vertex", vertexString) == false ||
		readFile(geometryShaderPath, "geometry", geometryString) == false ||
		readFile(fragmentShaderPath, "fragment", fragmentString) == false)
		return false;

	const char* vertexCString = vertexString.c_str();
	const char* geometryCString = geometryString.c_str();
	const char* fragmentCString = fragmentString.c_str();	

	return compileStrings(&vertexCString, &geometryCString, &fragmentCString);
}

bool ShaderProgram::compileStrings(const char** vertexShaderSource, const char** fragmentShaderSource)
{
	unsigned int vertexShader = compileShader(vertexShaderSource, GL_VERTEX_SHADER);
	if (vertexShader == 0)
		return false;

	unsigned int fragmentShader = compileShader(fragmentShaderSource, GL_FRAGMENT_SHADER);
	if (fragmentShader == 0)
		return false;

	if(id == -1)
		id = OpenGLProfile::getCurrent()->glCreateProgram();
	OpenGLProfile::getCurrent()->glAttachShader(id, vertexShader);
	OpenGLProfile::getCurrent()->glAttachShader(id, fragmentShader);
	OpenGLProfile::getCurrent()->glLinkProgram(id);

	int success;
	OpenGLProfile::getCurrent()->glGetProgramiv(id, GL_LINK_STATUS, &success);
	if (!success)
	{
		const int infoLogSize = 512;
		char infoLog[infoLogSize];
		OpenGLProfile::getCurrent()->glGetProgramInfoLog(id, infoLogSize, NULL, infoLog);

		qFatal("shader linking failed. %s", infoLog);
		return false;
	}
	
	OpenGLProfile::getCurrent()->glDeleteShader(vertexShader);
	OpenGLProfile::getCurrent()->glDeleteShader(fragmentShader);

	return true;
}

bool ShaderProgram::compileStrings(const char** vertexShaderSource, const char** geometryShaderSource, const char** fragmentShaderSource)
{
	unsigned int vertexShader = compileShader(vertexShaderSource, GL_VERTEX_SHADER);
	if (vertexShader == 0)
		return false;

	unsigned int geometryShader = compileShader(geometryShaderSource, GL_GEOMETRY_SHADER);
	if (geometryShader == 0)
		return false;

	unsigned int fragmentShader = compileShader(fragmentShaderSource, GL_FRAGMENT_SHADER);
	if (fragmentShader == 0)
		return false;

	if (id == -1)
		id = OpenGLProfile::getCurrent()->glCreateProgram();
	OpenGLProfile::getCurrent()->glAttachShader(id, vertexShader);
	OpenGLProfile::getCurrent()->glAttachShader(id, geometryShader);
	OpenGLProfile::getCurrent()->glAttachShader(id, fragmentShader);
	OpenGLProfile::getCurrent()->glLinkProgram(id);

	int success;
	OpenGLProfile::getCurrent()->glGetProgramiv(id, GL_LINK_STATUS, &success);
	if (!success)
	{
		const int infoLogSize = 512;
		char infoLog[infoLogSize];
		OpenGLProfile::getCurrent()->glGetProgramInfoLog(id, infoLogSize, NULL, infoLog);

		qFatal("shader linking failed. %s", infoLog);
		return false;
	}

	OpenGLProfile::getCurrent()->glDeleteShader(vertexShader);
	OpenGLProfile::getCurrent()->glDeleteShader(geometryShader);
	OpenGLProfile::getCurrent()->glDeleteShader(fragmentShader);

	return true;
}

bool ShaderProgram::readFile(const char* path, const char* shaderName, std::string& outString)
{
	std::ifstream shaderFile;
	// ensure ifstream objects can throw exceptions:
	shaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
	try
	{
		// open files
		shaderFile.open(path);
		std::stringstream shaderStream;
		// read file's buffer contents into streams
		shaderStream << shaderFile.rdbuf();
		// close file handlers
		shaderFile.close();
		// convert stream into string
		outString = shaderStream.str();
		return true;
	}
	catch (std::ifstream::failure e)
	{
		qFatal("can't read %s shader file. %s", shaderName, e.what());
		return false;
	}
}

unsigned int ShaderProgram::compileShader(const char** sourceCode, GLenum shaderType)
{
	int shaderID = OpenGLProfile::getCurrent()->glCreateShader(shaderType);
	OpenGLProfile::getCurrent()->glShaderSource(shaderID, 1, sourceCode, NULL);
	OpenGLProfile::getCurrent()->glCompileShader(shaderID);

	int success;
	OpenGLProfile::getCurrent()->glGetShaderiv(shaderID, GL_COMPILE_STATUS, &success);

	if (success)
	{
		return shaderID;
	}
	else
	{
		const int infoLogSize = 512;
		char infoLog[infoLogSize];
		OpenGLProfile::getCurrent()->glGetShaderInfoLog(shaderID, infoLogSize, NULL, infoLog);

		const char* vertexName = "vertex";
		const char* fragmentName = "fragment";
		const char* unknownName = "unknown";
		const char* useName;
		if (shaderType == GL_VERTEX_SHADER)
			useName = vertexName;
		else if (shaderType == GL_FRAGMENT_SHADER)
			useName = fragmentName;
		else
			useName = unknownName;

		qFatal("%s shader compilation failed. %s", useName, infoLog);
		return 0;
	}
}

void ShaderProgram::setTextureUnit(Texture* texture, int uniformId, unsigned int textureUnit)
{
	OpenGLProfile::getCurrent()->glActiveTexture(GL_TEXTURE0 + (int)textureUnit);
	texture->bind();
	setUniformInt(uniformId, (int)textureUnit);
	OpenGLProfile::getCurrent()->glActiveTexture(GL_TEXTURE0);
}

void ShaderProgram::setTextureUnit3D(Texture* cubemap, int uniformId, unsigned int textureUnit)
{
	OpenGLProfile::getCurrent()->glActiveTexture(GL_TEXTURE0 + (int)textureUnit);
	cubemap->bind3D();
	setUniformInt(uniformId, (int)textureUnit);
}

bool ShaderProgram::checkUniformId(const char* name, int& outId)
{
	outId = OpenGLProfile::getCurrent()->glGetUniformLocation(id, name);
	if (outId == -1)
	{
		qWarning("unknown uniform variable name : %s", name);
		return false;
	}
	else
	{
		return true;
	}
}


