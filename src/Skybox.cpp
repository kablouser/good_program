#include "Skybox.h"
#include "OpenGLProfile.h"

bool Skybox::initialize(const char* vertexShaderPath, const char* fragmentShaderPath, const char* cubemapSidePaths[6])
{
	isEnabled =
		shaderProgram.compileFiles(vertexShaderPath, fragmentShaderPath) &&
		shaderProgram.checkUniformId("viewToProjection", viewToProjectionId) &&
		shaderProgram.checkUniformId("cubemap", cubemapId) &&
		cubemap.create3D(cubemapSidePaths);

	if (isEnabled && cube.getIsDataSet() == false)
	{
        float skyboxVertices[] =
        {
            // positions
            -1.0f,  1.0f, -1.0f,
            -1.0f, -1.0f, -1.0f,
             1.0f, -1.0f, -1.0f,
             1.0f, -1.0f, -1.0f,
             1.0f,  1.0f, -1.0f,
            -1.0f,  1.0f, -1.0f,

            -1.0f, -1.0f,  1.0f,
            -1.0f, -1.0f, -1.0f,
            -1.0f,  1.0f, -1.0f,
            -1.0f,  1.0f, -1.0f,
            -1.0f,  1.0f,  1.0f,
            -1.0f, -1.0f,  1.0f,

             1.0f, -1.0f, -1.0f,
             1.0f, -1.0f,  1.0f,
             1.0f,  1.0f,  1.0f,
             1.0f,  1.0f,  1.0f,
             1.0f,  1.0f, -1.0f,
             1.0f, -1.0f, -1.0f,

            -1.0f, -1.0f,  1.0f,
            -1.0f,  1.0f,  1.0f,
             1.0f,  1.0f,  1.0f,
             1.0f,  1.0f,  1.0f,
             1.0f, -1.0f,  1.0f,
            -1.0f, -1.0f,  1.0f,

            -1.0f,  1.0f, -1.0f,
             1.0f,  1.0f, -1.0f,
             1.0f,  1.0f,  1.0f,
             1.0f,  1.0f,  1.0f,
            -1.0f,  1.0f,  1.0f,
            -1.0f,  1.0f, -1.0f,

            -1.0f, -1.0f, -1.0f,
            -1.0f, -1.0f,  1.0f,
             1.0f, -1.0f, -1.0f,
             1.0f, -1.0f, -1.0f,
            -1.0f, -1.0f,  1.0f,
             1.0f, -1.0f,  1.0f
        };
		int attributeSize = 3;
		GLenum attributeType = GL_FLOAT;

		cube.setData(
			sizeof(skyboxVertices), skyboxVertices,
			0, NULL,
			sizeof(int), &attributeSize, &attributeType,
			GL_STATIC_DRAW);
	}

	return isEnabled;
}

void Skybox::render(const QMatrix4x4& projection, const QMatrix4x4& view)
{
	if (isEnabled == false)
		return;

    // remove translation
    QMatrix4x4 viewWithoutTranslation = view;
    viewWithoutTranslation.data()[0 + 4 * 3] = 0.0f;
    viewWithoutTranslation.data()[1 + 4 * 3] = 0.0f;
    viewWithoutTranslation.data()[2 + 4 * 3] = 0.0f;

	shaderProgram.use();
	// vertex shader uniform variables
    // get view without translation, so skybox stays
	shaderProgram.setUniformMatrix(viewToProjectionId,
        projection * viewWithoutTranslation);
	// fragment shader uniform variables
	shaderProgram.setTextureUnit3D(&cubemap, cubemapId, 0);

    OpenGLProfile::getCurrent()->glDepthFunc(GL_LEQUAL);
	cube.draw();
    OpenGLProfile::getCurrent()->glDepthFunc(GL_LESS);
}

Texture& Skybox::getCubemap()
{
    return cubemap;
}

bool Skybox::isEnabled = false;
ShaderProgram Skybox::shaderProgram;
int Skybox::viewToProjectionId = -1, Skybox::cubemapId = -1;
Texture Skybox::cubemap;
Mesh Skybox::cube;
