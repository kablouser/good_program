#include "SpotLight.h"
#include "DirectionLight.h"
#include <QtMath>

SpotLight::SpotLight(SceneObject* parent,
	QVector3D color, float angle) :
	GenericLight(parent)
{
	lightData.position = QVector3D();
	lightData.direction = QVector3D(0, 0, -1);
	lightData.components.ambient = QVector3D();
	lightData.components.diffuse = color;
	lightData.components.specular = color;
	lightData.scaling.constant = 1.0f;
	lightData.scaling.linear = 0.01f;
	lightData.scaling.quadratic = 0.001f;
	lightData.cutOff = qCos(qDegreesToRadians(angle * 0.9f));
	lightData.outerCutoff = qCos(qDegreesToRadians(angle));
	allSpotLights.insert(this);
}

SpotLight::~SpotLight()
{
	allSpotLights.erase(this);
}

bool SpotLight::initialize(int shadowMapResolution, float nearPlane, float farPlane)
{
	if (shadowMapFramebuffer.createDepthOnly(shadowMapResolution, shadowMapResolution))
	{
		SpotLight::shadowMapResolution = shadowMapResolution;
		SpotLight::nearPlane = nearPlane;
		SpotLight::farPlane = farPlane;
		return true;
	}
	return false;
}

void SpotLight::updateShadowMaps(int viewportWidth, int viewportHeight)
{
	OpenGLProfile::getCurrent()->glViewport(0, 0, shadowMapResolution, shadowMapResolution);

	for (auto i = allSpotLights.begin(); i != allSpotLights.end(); ++i)
	{
		if ((*i)->getExecutable())
		{
			shadowMapFramebuffer.attachExternalDepth((*i)->shadowMap);
			OpenGLProfile::getCurrent()->glClear(GL_DEPTH_BUFFER_BIT);
			Renderer::foreachRenderer([=](Renderer* renderer)
			{
				// set model matrix
				DirectionLight::setShadowMapShader(
					(*i)->lightData.lightSpace * renderer->getModelMatrix());
				renderer->draw();
			}
			);
		}
	}

	OpenGLProfile::getCurrent()->glViewport(0, 0, viewportWidth, viewportHeight);
}

void SpotLight::updateLightData()
{
	if (getExecutable())
	{
		if (transform != nullptr)
		{
			lightData.lightSpace.setToIdentity();
			lightData.lightSpace.perspective(
				qRadiansToDegrees(qAcos(lightData.outerCutoff)) * 2.0f, 1.0f, nearPlane, farPlane);
			lightData.lightSpace.lookAt(
				transform->getPosition(), transform->getPosition() + lightData.direction, transform->getUp());
		}
		StandardRenderer::setSpotLight(&lightData);
	}
}

void SpotLight::setEnabled(bool isEnabled)
{
	if (getEnabled() == isEnabled)
		return;

	SceneComponent::setEnabled(isEnabled);

	if (isEnabled)
		updateLightData();
	else
		StandardRenderer::removeSpotLight(&lightData);	
}

void SpotLight::initializeLight()
{
	lightData.position = transform->getPosition();
	lightData.direction = transform->getForward();
	lightData.shadowMap = &shadowMap;
	shadowMap.createRaw(shadowMapResolution, shadowMapResolution,
		GL_CLAMP_TO_EDGE, GL_LINEAR, GL_LINEAR, GL_DEPTH_COMPONENT);
}

bool SpotLight::hasLightChanged()
{
	QVector3D newPosition = transform->getPosition();
	QVector3D newDirection = transform->getForward();
	if (qFuzzyCompare(lightData.position, newPosition) == false || 
		qFuzzyCompare(lightData.direction, newDirection) == false)
	{
		lightData.position = newPosition;
		lightData.direction = newDirection;
		return true;
	}
	else
	{
		return false;
	}
}

std::set<SpotLight*> SpotLight::allSpotLights;
Framebuffer SpotLight::shadowMapFramebuffer;
int SpotLight::shadowMapResolution;
float SpotLight::nearPlane, SpotLight::farPlane;
