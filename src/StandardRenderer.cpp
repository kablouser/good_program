#include "StandardRenderer.h"
#include "ShaderProgram.h"
#include "ReflectionProbe.h"
#include "DummyTextures.h"
#include <string>

using namespace std;

const int
	directionLightsMax = 5,
	pointLightsMax = 5,
	spotLightsMax = 5;
const int diffuseTextureUnit = 0;
const int specularTextureUnit = 1;
const int emissionTextureUnit = 2;
const int environmentTextureUnit = 3;
inline int getDirectionLightTextureUnit(int index)
{
	return 4 + index;
}
inline int getPointLightTextureUnit(int index)
{
	return 4 + directionLightsMax + index;
}
inline int getSpotLightTextureUnit(int index)
{
	return 4 + directionLightsMax + pointLightsMax + index;
}

StandardRenderer::StandardRenderer(
	SceneObject* parent,
	Mesh* mesh, Material* material) :
	Renderer(parent), mesh(mesh), material(material) {}

bool StandardRenderer::initialize(const char* vertexShaderPath, const char* fragmentShaderPath)
{
	return
		shaderProgram.compileFiles(vertexShaderPath, fragmentShaderPath) &&

		shaderProgram.checkUniformId("model", model) &&
		shaderProgram.checkUniformId("modelToProjection", modelToProjection) &&
		shaderProgram.checkUniformId("modelToView", modelToView) &&
		shaderProgram.checkUniformId("directionModelToView", directionModelToView) &&
		shaderProgram.checkUniformId("worldToView", worldToView) &&
		shaderProgram.checkUniformId("directionWorldToView", directionWorldToView) &&

		shaderProgram.checkUniformId("material.isDiffuseSingle", isDiffuseSingle) &&
		shaderProgram.checkUniformId("material.singleDiffuse", singleDiffuse) &&
		shaderProgram.checkUniformId("material.diffuse[0]", diffuse) &&

		shaderProgram.checkUniformId("material.isSpecularSingle", isSpecularSingle) &&
		shaderProgram.checkUniformId("material.singleSpecular", singleSpecular) &&
		shaderProgram.checkUniformId("material.specular[0]", specular) &&

		shaderProgram.checkUniformId("material.isEmissionSingle", isEmissionSingle) &&
		shaderProgram.checkUniformId("material.singleEmission", singleEmission) &&
		shaderProgram.checkUniformId("material.emission[0]", emission) &&

		shaderProgram.checkUniformId("material.shininess", shininess) &&
		shaderProgram.checkUniformId("material.alphaCutoff", alphaCutoff) &&

		shaderProgram.checkUniformId("isReflectionEnabled", isReflectionEnabled) &&
		shaderProgram.checkUniformId("environmentCubemap", environmentCubemap) &&
		shaderProgram.checkUniformId("pointLightFarPlane", pointLightFarPlane) &&		

		initializeLightIds();
}

void StandardRenderer::renderAllTransparent(
	const QMatrix4x4& projection, 
	const QMatrix4x4& view, 
	const QVector3D& cameraPosition,
	bool isReflectionsEnabled)
{
	int size = transparentRenderers.size();
	if (size == 0)
		return;

	sort(transparentRenderers.begin(), transparentRenderers.end(),
		[&cameraPosition](const StandardRenderer* renderer1, const StandardRenderer* renderer2)
	{
		return 
			(cameraPosition - renderer1->transform->getPosition()).lengthSquared() >=
			(cameraPosition - renderer2->transform->getPosition()).lengthSquared();
	});

	OpenGLProfile::getCurrent()->glEnable(GL_BLEND);

	//draw transparent meshes
	for (int i = 0; i < size; i++)
	{
		transparentRenderers[i]->renderActual(projection, view, isReflectionsEnabled);
	}

	OpenGLProfile::getCurrent()->glDisable(GL_BLEND);

	// clear the list of transparent rendereres for next render loop
	transparentRenderers.clear();
}

void StandardRenderer::render(const QMatrix4x4& projection, const QMatrix4x4& view, bool isReflectionsEnabled)
{
	if (mesh == nullptr || material == nullptr)
	{
		qCritical("StandardRender : must have mesh and material");
		return;
	}

	if (material->isTransparent)
	{
		// transparent renderers needs sorting before rendering
		transparentRenderers.push_back(this);
		return;
	}

	renderActual(projection, view, isReflectionsEnabled);
}

void StandardRenderer::renderActual(const QMatrix4x4& projection, const QMatrix4x4& view, bool isReflectionsEnabled)
{
	QMatrix4x4 model;
	QVector3D position = transform->getPosition();
	model.translate(position);
	model.rotate(transform->getRotation());
	if(isReflectionsEnabled)
		ReflectionProbe::setEnvironmentCubemap(position);

	QMatrix4x4 viewTransform = view * model;

	shaderProgram.use();
	// vertex shader uniform variables
	shaderProgram.setUniformMatrix(StandardRenderer::model, model);
	shaderProgram.setUniformMatrix(modelToProjection, projection * viewTransform);
	shaderProgram.setUniformMatrix(modelToView, viewTransform);
	shaderProgram.setUniformMatrix(directionModelToView, viewTransform.inverted().transposed());
	// fragment shader uniform variables
	shaderProgram.setUniformMatrix(worldToView, view);
	shaderProgram.setUniformMatrix(directionWorldToView, view.inverted().transposed());

	// fragment diffuse settings
	shaderProgram.setUniformInt(isDiffuseSingle, material->diffuse.isSingle);
	if (material->diffuse.isSingle)
	{
		shaderProgram.setUniform4f(singleDiffuse, material->diffuse.data.singleColor);
	}
	else
	{
		shaderProgram.setTextureUnit(material->diffuse.data.texture, diffuse, diffuseTextureUnit);
	}

	// fragment specular settings
	shaderProgram.setUniformInt(isSpecularSingle, material->specular.isSingle);
	if (material->specular.isSingle)
	{
		shaderProgram.setUniform3f(singleSpecular, material->specular.data.singleColor);
	}
	else
	{
		shaderProgram.setTextureUnit(material->specular.data.texture, specular, specularTextureUnit);
	}

	// fragment emission settings
	shaderProgram.setUniformInt(isEmissionSingle, material->emission.isSingle);
	if (material->emission.isSingle)
	{
		shaderProgram.setUniform3f(singleEmission, material->emission.data.singleColor);
	}
	else
	{
		shaderProgram.setTextureUnit(material->emission.data.texture, emission, emissionTextureUnit);
	}

	shaderProgram.setUniformFloat(shininess, material->shininess);
	shaderProgram.setUniformFloat(alphaCutoff, material->alphaCutoff);

	mesh->draw();
}

template <typename T>
bool setLight(T* lightData, vector<T*>& allLights, int maxLights, int sizeId,
	void(*setIndex)(int,T*), // function pointer
	ShaderProgram& shaderProgram)
{
	if (lightData != nullptr)
	{
		for (int i = 0; i < allLights.size(); ++i)
		{
			if (allLights[i] == lightData)
			{
				setIndex(i, lightData);
				return true;
			}
		}
		if (allLights.size() < maxLights)
		{
			allLights.push_back(lightData);
			setIndex(allLights.size() - 1, lightData);
			// setIndex will bind the shaderProgram, which is needed for setUniform
			shaderProgram.setUniformInt(sizeId, allLights.size());			
			return true;
		}
	}
	return false;
}

template <typename T>
bool removeLight(T* lightData, vector<T*>& allLights, int maxLights, int sizeId,
	void(*setIndex)(int, T*), // function pointer
	ShaderProgram& shaderProgram)
{
	for (int i = 0; i < allLights.size(); ++i)
	{
		if (allLights[i] == lightData)
		{
			allLights.erase(allLights.begin() + i);
			shaderProgram.setUniformInt(sizeId, allLights.size());
			for (; i < allLights.size(); ++i)
			{
				setIndex(i, allLights[i]);
			}
			return true;
		}
	}
	return false;
}

bool StandardRenderer::setDirectionLight(DirectionLight* directionLight)
{
	return setLight<DirectionLight>(
		directionLight, allDirectionLights, directionLightsMax, directionLightsEnabled,
		setDirectionLightIndex, shaderProgram);
}

bool StandardRenderer::removeDirectionLight(DirectionLight* directionLight)
{
	return removeLight<DirectionLight>(
		directionLight, allDirectionLights, directionLightsMax, directionLightsEnabled,
		setDirectionLightIndex, shaderProgram);
}

bool StandardRenderer::setPointLight(PointLight* pointLight)
{
	return setLight<PointLight>(
		pointLight, allPointLights, pointLightsMax, pointLightsEnabled,
		setPointLightIndex, shaderProgram);
}

bool StandardRenderer::removePointLight(PointLight* pointLight)
{
	return removeLight<PointLight>(
		pointLight, allPointLights, pointLightsMax, pointLightsEnabled,
		setPointLightIndex, shaderProgram);
}

bool StandardRenderer::setSpotLight(SpotLight* spotLight)
{
	return setLight<SpotLight>(
		spotLight, allSpotLights, spotLightsMax, spotLightsEnabled,
		setSpotLightIndex, shaderProgram);
}

bool StandardRenderer::removeSpotLight(SpotLight* spotLight)
{
	return removeLight<SpotLight>(
		spotLight, allSpotLights, spotLightsMax, spotLightsEnabled,
		setSpotLightIndex, shaderProgram);
}

void StandardRenderer::setEnvironmentCubemap(Texture* cubemap)
{
	shaderProgram.use();
	shaderProgram.setTextureUnit3D(cubemap, environmentCubemap, environmentTextureUnit);
}

void StandardRenderer::setReflections(bool isEnabled)
{
	shaderProgram.use();
	shaderProgram.setUniformInt(isReflectionEnabled, isEnabled);
}

void StandardRenderer::setPointLightFarPlane(float farPlane)
{	
	shaderProgram.use();
	shaderProgram.setUniformFloat(pointLightFarPlane, farPlane);
}

bool StandardRenderer::initializeLightIds()
{
	shaderProgram.use();
	for (int i = 0; i < directionLightsMax; i++)
	{
		string indexer("directionLights[" + to_string(i) + "].");
		bool success =
			shaderProgram.checkUniformId((indexer + "direction").c_str(), directionLightIds[i].direction) &&
			shaderProgram.checkUniformId((indexer + "ambient").c_str(), directionLightIds[i].components.ambient) &&
			shaderProgram.checkUniformId((indexer + "diffuse").c_str(), directionLightIds[i].components.diffuse) &&
			shaderProgram.checkUniformId((indexer + "specular").c_str(), directionLightIds[i].components.specular) &&
			shaderProgram.checkUniformId((indexer + "shadowMap").c_str(), directionLightIds[i].shadowMap) &&
			shaderProgram.checkUniformId((indexer + "lightSpace").c_str(), directionLightIds[i].lightSpace);

		shaderProgram.setTextureUnit(DummyTextures::getDummyTexture(), directionLightIds[i].shadowMap,
			getDirectionLightTextureUnit(i));
		if (success == false)
			return false;
	}

	for (int i = 0; i < pointLightsMax; i++)
	{
		string indexer("pointLights[" + to_string(i) + "].");
		bool success =
			shaderProgram.checkUniformId((indexer + "position").c_str(), pointLightIds[i].position) &&
			shaderProgram.checkUniformId((indexer + "ambient").c_str(), pointLightIds[i].components.ambient) &&
			shaderProgram.checkUniformId((indexer + "diffuse").c_str(), pointLightIds[i].components.diffuse) &&
			shaderProgram.checkUniformId((indexer + "specular").c_str(), pointLightIds[i].components.specular) &&
			shaderProgram.checkUniformId((indexer + "constant").c_str(), pointLightIds[i].scaling.constant) &&
			shaderProgram.checkUniformId((indexer + "linear").c_str(), pointLightIds[i].scaling.linear) &&
			shaderProgram.checkUniformId((indexer + "quadratic").c_str(), pointLightIds[i].scaling.quadratic) &&
			shaderProgram.checkUniformId((indexer + "shadowMap").c_str(), pointLightIds[i].shadowMap);

		shaderProgram.setTextureUnit(DummyTextures::getDummyTexture(), pointLightIds[i].shadowMap,
			getPointLightTextureUnit(i));
		if (success == false)
			return false;
	}

	for (int i = 0; i < spotLightsMax; i++)
	{
		string indexer("spotLights[" + to_string(i) + "].");
		bool success =
			shaderProgram.checkUniformId((indexer + "position").c_str(), spotLightIds[i].position) &&
			shaderProgram.checkUniformId((indexer + "direction").c_str(), spotLightIds[i].direction) &&
			shaderProgram.checkUniformId((indexer + "ambient").c_str(), spotLightIds[i].components.ambient) &&
			shaderProgram.checkUniformId((indexer + "diffuse").c_str(), spotLightIds[i].components.diffuse) &&
			shaderProgram.checkUniformId((indexer + "specular").c_str(), spotLightIds[i].components.specular) &&
			shaderProgram.checkUniformId((indexer + "constant").c_str(), spotLightIds[i].scaling.constant) &&
			shaderProgram.checkUniformId((indexer + "linear").c_str(), spotLightIds[i].scaling.linear) &&
			shaderProgram.checkUniformId((indexer + "quadratic").c_str(), spotLightIds[i].scaling.quadratic) &&
			shaderProgram.checkUniformId((indexer + "cutOff").c_str(), spotLightIds[i].cutOff) &&
			shaderProgram.checkUniformId((indexer + "outerCutoff").c_str(), spotLightIds[i].outerCutoff) &&
			shaderProgram.checkUniformId((indexer + "shadowMap").c_str(), spotLightIds[i].shadowMap) &&
			shaderProgram.checkUniformId((indexer + "lightSpace").c_str(), spotLightIds[i].lightSpace);

		shaderProgram.setTextureUnit(DummyTextures::getDummyTexture(), spotLightIds[i].shadowMap,
			getSpotLightTextureUnit(i));
		if (success == false)
			return false;
	}

	allDirectionLights.reserve(directionLightsMax);
	allPointLights.reserve(pointLightsMax);
	allSpotLights.reserve(spotLightsMax);

	// set dummy environment cubemap
	shaderProgram.setTextureUnit(DummyTextures::getDummyTexture(), environmentCubemap, environmentTextureUnit);

	return
		shaderProgram.checkUniformId("directionLightsEnabled", directionLightsEnabled) &&
		shaderProgram.checkUniformId("pointLightsEnabled", pointLightsEnabled) &&
		shaderProgram.checkUniformId("spotLightsEnabled", spotLightsEnabled);
}

void StandardRenderer::setDirectionLightIndex(int index, DirectionLight* directionLight)
{
	shaderProgram.use();
	shaderProgram.setUniform3f(directionLightIds[index].direction, directionLight->direction);
	shaderProgram.setUniform3f(directionLightIds[index].components.ambient, directionLight->components.ambient);
	shaderProgram.setUniform3f(directionLightIds[index].components.diffuse, directionLight->components.diffuse);
	shaderProgram.setUniform3f(directionLightIds[index].components.specular, directionLight->components.specular);
	shaderProgram.setTextureUnit(directionLight->shadowMap, directionLightIds[index].shadowMap,
		getDirectionLightTextureUnit(index));
	shaderProgram.setUniformMatrix(directionLightIds[index].lightSpace, directionLight->lightSpace);
}

void StandardRenderer::setPointLightIndex(int index, PointLight* pointLight)
{
	shaderProgram.use();
	shaderProgram.setUniform3f(pointLightIds[index].position, pointLight->position);	
	shaderProgram.setUniform3f(pointLightIds[index].components.ambient, pointLight->components.ambient);	
	shaderProgram.setUniform3f(pointLightIds[index].components.diffuse, pointLight->components.diffuse);
	shaderProgram.setUniform3f(pointLightIds[index].components.specular, pointLight->components.specular);
	shaderProgram.setUniformFloat(pointLightIds[index].scaling.constant, pointLight->scaling.constant);
	shaderProgram.setUniformFloat(pointLightIds[index].scaling.linear, pointLight->scaling.linear);
	shaderProgram.setUniformFloat(pointLightIds[index].scaling.quadratic, pointLight->scaling.quadratic);
	shaderProgram.setTextureUnit3D(pointLight->shadowMap, pointLightIds[index].shadowMap,
		getPointLightTextureUnit(index));
}

void StandardRenderer::setSpotLightIndex(int index, SpotLight* spotLight)
{
	shaderProgram.use();
	shaderProgram.setUniform3f(spotLightIds[index].position, spotLight->position);
	shaderProgram.setUniform3f(spotLightIds[index].direction, spotLight->direction);
	shaderProgram.setUniform3f(spotLightIds[index].components.ambient, spotLight->components.ambient);
	shaderProgram.setUniform3f(spotLightIds[index].components.diffuse, spotLight->components.diffuse);
	shaderProgram.setUniform3f(spotLightIds[index].components.specular, spotLight->components.specular);
	shaderProgram.setUniformFloat(spotLightIds[index].scaling.constant, spotLight->scaling.constant);
	shaderProgram.setUniformFloat(spotLightIds[index].scaling.linear, spotLight->scaling.linear);
	shaderProgram.setUniformFloat(spotLightIds[index].scaling.quadratic, spotLight->scaling.quadratic);
	shaderProgram.setUniformFloat(spotLightIds[index].cutOff, spotLight->cutOff);
	shaderProgram.setUniformFloat(spotLightIds[index].outerCutoff, spotLight->outerCutoff);
	shaderProgram.setTextureUnit(spotLight->shadowMap, spotLightIds[index].shadowMap,
		getSpotLightTextureUnit(index));
	shaderProgram.setUniformMatrix(spotLightIds[index].lightSpace, spotLight->lightSpace);
}

ShaderProgram StandardRenderer::shaderProgram;

vector<StandardRenderer::DirectionLight*> StandardRenderer::allDirectionLights;
vector<StandardRenderer::PointLight*> StandardRenderer::allPointLights;
vector<StandardRenderer::SpotLight*> StandardRenderer::allSpotLights;

int
	StandardRenderer::model = -1,
	StandardRenderer::modelToProjection = -1, 
	StandardRenderer::modelToView = -1, 
	StandardRenderer::directionModelToView = -1, 
	StandardRenderer::worldToView = -1, 
	StandardRenderer::directionWorldToView = -1,
	StandardRenderer::isDiffuseSingle = -1, StandardRenderer::singleDiffuse = -1, StandardRenderer::diffuse = -1,
	StandardRenderer::isSpecularSingle = -1, StandardRenderer::singleSpecular = -1, StandardRenderer::specular = -1,
	StandardRenderer::isEmissionSingle = -1, StandardRenderer::singleEmission = -1, StandardRenderer::emission = -1,
	StandardRenderer::shininess = -1, StandardRenderer::alphaCutoff = -1,
	StandardRenderer::directionLightsEnabled = -1,
	StandardRenderer::pointLightsEnabled = -1,
	StandardRenderer::spotLightsEnabled = -1,
	StandardRenderer::isReflectionEnabled = -1,
	StandardRenderer::environmentCubemap = -1,
	StandardRenderer::pointLightFarPlane = -1;

StandardRenderer::DirectionLightIds StandardRenderer::directionLightIds[directionLightsMax];
StandardRenderer::PointLightIds StandardRenderer::pointLightIds[pointLightsMax];
StandardRenderer::SpotLightIds StandardRenderer::spotLightIds[spotLightsMax];
std::vector<StandardRenderer*> StandardRenderer::transparentRenderers;