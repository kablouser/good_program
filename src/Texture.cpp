#include "Texture.h"

#include <QImage>
#include <QGLWidget>

Texture::Texture() : id(-1) { }

Texture::~Texture()
{
	if (OpenGLProfile::getCurrent() == nullptr)
		return;
	if(id != -1)
		OpenGLProfile::getCurrent()->glDeleteTextures(1, &id);
}

bool Texture::create(
	const char* imagePath,
	GLint wrapMode,
	GLint textureFilter,
	GLint mipmapFilter)
{
	if (id == -1)
		OpenGLProfile::getCurrent()->glGenTextures(1, &id);

	OpenGLProfile::getCurrent()->glBindTexture(GL_TEXTURE_2D, id);
	// set the texture wrapping/filtering options (on the currently bound texture object)
	OpenGLProfile::getCurrent()->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrapMode);
	OpenGLProfile::getCurrent()->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrapMode);
	OpenGLProfile::getCurrent()->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, textureFilter);
	OpenGLProfile::getCurrent()->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, mipmapFilter);
	// load and generate the texture
	QImage original = QImage(QString(imagePath));
	if (original.isNull())
	{
		qWarning("could not open image : %s", imagePath);
		OpenGLProfile::getCurrent()->glBindTexture(GL_TEXTURE_2D, 0);
		return false;
	}

	QImage image = QGLWidget::convertToGLFormat(original);
	if (image.isNull())
	{
		qWarning("could not open image : %s", imagePath);
		OpenGLProfile::getCurrent()->glBindTexture(GL_TEXTURE_2D, 0);
		return false;
	}
	else
	{
		OpenGLProfile::getCurrent()->glTexImage2D(
			GL_TEXTURE_2D, 0, GL_RGBA,
			image.width(), image.height(), 0, GL_RGBA,
			GL_UNSIGNED_BYTE, image.bits());
		OpenGLProfile::getCurrent()->glGenerateMipmap(GL_TEXTURE_2D);
		OpenGLProfile::getCurrent()->glBindTexture(GL_TEXTURE_2D, 0);
		return true;
	}
}

bool Texture::create3D(
	const char* cubemapSidePaths[6],
	GLint wrapMode,
	GLint textureFilter,
	GLint mipmapFilter)
{
	if (id == -1)
		OpenGLProfile::getCurrent()->glGenTextures(1, &id);

	OpenGLProfile::getCurrent()->glBindTexture(GL_TEXTURE_CUBE_MAP, id);

	for (int i = 0; i < 6; ++i)
	{
		// load and generate the texture
		QImage original = QImage(QString(cubemapSidePaths[i]));
		if (original.isNull())
		{
			qWarning("could not open image : %s", cubemapSidePaths[i]);
			OpenGLProfile::getCurrent()->glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
			return false;
		}

		bool flip = !(i == 2 || i == 3);

		// OpenGL cubemap texture coordinates are flipped vertically, except when its up or face
		QImage image = QGLWidget::convertToGLFormat(original).mirrored(flip, flip);
		if (image.isNull())
		{
			qWarning("could not open image : %s", cubemapSidePaths[i]);
			OpenGLProfile::getCurrent()->glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
			return false;
		}
		else
		{
			OpenGLProfile::getCurrent()->glTexImage2D(
				GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, 
				GL_RGBA, image.width(), image.height(),
				0, GL_RGBA, GL_UNSIGNED_BYTE, image.bits());
		}
	}

	OpenGLProfile::getCurrent()->glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, wrapMode);
	OpenGLProfile::getCurrent()->glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, wrapMode);
	OpenGLProfile::getCurrent()->glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, wrapMode);
	OpenGLProfile::getCurrent()->glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, textureFilter);
	OpenGLProfile::getCurrent()->glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, mipmapFilter);
	OpenGLProfile::getCurrent()->glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
	return true;
}

void Texture::createRaw(
	int width, int height,
	GLint wrapMode,
	GLint textureFilter,
	GLint mipmapFilter,
	GLint format)
{
	if (id == -1)
		OpenGLProfile::getCurrent()->glGenTextures(1, &id);

	OpenGLProfile::getCurrent()->glBindTexture(GL_TEXTURE_2D, id);
	OpenGLProfile::getCurrent()->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrapMode);
	OpenGLProfile::getCurrent()->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrapMode);
	OpenGLProfile::getCurrent()->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, textureFilter);
	OpenGLProfile::getCurrent()->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, mipmapFilter);
	OpenGLProfile::getCurrent()->glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, getDataType(format), NULL);
	OpenGLProfile::getCurrent()->glBindTexture(GL_TEXTURE_2D, 0);
}

void Texture::createRaw3D(
	int widthPerFace, int heightPerFace,
	GLint wrapMode,
	GLint textureFilter,
	GLint mipmapFilter,
	GLint format)
{
	if (format == GL_RGB)
	{
		qFatal("Texture : cubemap cannot use RGB, must be RGBA!");
		return;
	}
	if (id == -1)
		OpenGLProfile::getCurrent()->glGenTextures(1, &id);

	OpenGLProfile::getCurrent()->glBindTexture(GL_TEXTURE_CUBE_MAP, id);
	OpenGLProfile::getCurrent()->glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, wrapMode);
	OpenGLProfile::getCurrent()->glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, wrapMode);
	OpenGLProfile::getCurrent()->glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, wrapMode);
	OpenGLProfile::getCurrent()->glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, textureFilter);
	OpenGLProfile::getCurrent()->glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, mipmapFilter);

	for (int i = 0; i < 6; ++i)
	{
		OpenGLProfile::getCurrent()->glTexImage2D(
			GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0,
			format, widthPerFace, heightPerFace, 0, format, getDataType(format), NULL);
	}

	OpenGLProfile::getCurrent()->glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
}

void Texture::createDummy()
{
	if (id == -1)
		OpenGLProfile::getCurrent()->glGenTextures(1, &id);
}

void Texture::bind()
{
	OpenGLProfile::getCurrent()->glBindTexture(GL_TEXTURE_2D, id);
}

void Texture::bind3D()
{
	OpenGLProfile::getCurrent()->glBindTexture(GL_TEXTURE_CUBE_MAP, id);
}

GLint Texture::getDataType(GLint format)
{
	switch (format)
	{
	case GL_DEPTH_COMPONENT:
		return GL_FLOAT;	
	case GL_RGB:
		return GL_UNSIGNED_BYTE;
	case GL_RGBA:
		return GL_UNSIGNED_BYTE;
	default:
		qFatal("Texture : unknown data type %i", format);
		return -1;
	}	
}
