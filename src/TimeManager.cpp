#include "TimeManager.h"

TimeManager::TimeManager()
{
	time = deltaTime = 0;
	programTimer.start();
}

void TimeManager::update()
{
	float newTime = programTimer.elapsed() / 1000.0;
	deltaTime = newTime - time;
	time = newTime;
}
