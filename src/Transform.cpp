#include "Transform.h"
#include "MathUtility.h"
#include <QMatrix4x4>

Transform::Transform(SceneObject* parent, QVector3D position, QQuaternion rotation) :
	SceneComponent(parent),
	localPosition(position), localRotation(rotation),
	isWorldAnchored(true),
	parentWorldPosition(), parentWorldRotation() { }

QVector3D Transform::getFlatForward() const
{
	QVector3D forward = getForward();
	forward.setY(0);
	forward.normalize();
	return forward;
}

QVector3D Transform::getForward() const
{
	return getRotation().rotatedVector(QVector3D(0, 0, -1));
}

QVector3D Transform::getRight() const
{
	return getRotation().rotatedVector(QVector3D(1, 0, 0));
}

QVector3D Transform::getUp() const
{
	return getRotation().rotatedVector(QVector3D(0, 1, 0));
}

const QVector3D& Transform::getLocalPosition() const
{
	return localPosition;
}

const QQuaternion& Transform::getLocalRotation() const
{
	return localRotation;
}

void Transform::setLocalPosition(const QVector3D& localPosition)
{
	this->localPosition = localPosition;
	//update positions of all children components

	QVector3D worldPosition = getPosition();
	foreachChildComponent<Transform>([&worldPosition](Transform* child)
	{
		child->parentWorldPosition = worldPosition;
		child->setLocalPosition(child->getLocalPosition());
	});
}

void Transform::setLocalRotation(const QQuaternion& localRotation)
{
	this->localRotation = localRotation;
	//update positions AND rotations of all children components

	QQuaternion worldRotation = getRotation();
	foreachChildComponent<Transform>([&worldRotation](Transform* child)
	{
		child->parentWorldRotation = worldRotation;
		child->setLocalRotation(child->getLocalRotation());
	});
}

QVector3D Transform::getPosition() const
{
	return parentWorldPosition + parentWorldRotation.rotatedVector(localPosition);
}

QQuaternion Transform::getRotation() const
{
	return parentWorldRotation * localRotation;
}

void Transform::setPosition(const QVector3D& position)
{
	setLocalPosition(position - parentWorldPosition);
}

void Transform::setRotation(const QQuaternion& rotation)
{
	setLocalRotation(MathUtility::inverseQuaternion(parentWorldRotation) * rotation);
}

void Transform::onParent(SceneObject* oldParent)
{
	QVector3D worldPosition;
	QQuaternion worldRotation;

	if (isWorldAnchored)
	{
		// remember old position and rotation
		worldPosition = getPosition();
		worldRotation = getRotation();
	}

	// check for new parent
	Transform* newParent = getParentComponent<Transform>();
	if (newParent != nullptr)
	{
		parentWorldPosition = newParent->getPosition();
		parentWorldRotation = newParent->getRotation();
	}
	else
	{
		parentWorldPosition = QVector3D();
		parentWorldRotation = QQuaternion();
	}

	if (isWorldAnchored)
	{
		// set world position and rotation to old data
		setPosition(worldPosition);
		setRotation(worldRotation);
	}

	// broadcast new world position and rotation to child components
	worldPosition = getPosition();
	worldRotation = getRotation();
	foreachChildComponent<Transform>([&worldPosition, &worldRotation](Transform* child)
	{
		child->parentWorldPosition = worldPosition;
		child->parentWorldRotation = worldRotation;
		child->setLocalPosition(child->getLocalPosition());
		child->setLocalRotation(child->getLocalRotation());
	});
}
