#include "UnlitRenderer.h"
#include "ReflectionProbe.h"

UnlitRenderer::UnlitRenderer(SceneObject* parent, Mesh* mesh, QVector3D color) :
    Renderer(parent), mesh(mesh), unlitData(color) {}

UnlitRenderer::UnlitRenderer(SceneObject* parent, Mesh* mesh, Texture* texture) :
    Renderer(parent), mesh(mesh), unlitData(texture) {}

bool UnlitRenderer::initialize(const char* vertexShaderPath, const char* fragmentShaderPath)
{
	return unlitShader.compileFiles(vertexShaderPath, fragmentShaderPath) &&
		unlitShader.checkUniformId("modelToProjection", modelToProjection) &&
		unlitShader.checkUniformId("isScaleEnabled", isScaleEnabled) &&
		unlitShader.checkUniformId("scaleAlongNormals", scaleAlongNormals) &&
		unlitShader.checkUniformId("scale", scale) &&
		unlitShader.checkUniformId("isTextureEnabled", isTextureEnabled) &&
		unlitShader.checkUniformId("texture0", texture0) &&
		unlitShader.checkUniformId("color", color);
}

void UnlitRenderer::setShaderParameters(
	const QMatrix4x4& projection,
	const QMatrix4x4& view,
	Transform* transform,
	bool isScaleEnabled,
	bool scaleAlongNormals,
	float scale,
	const MaterialComponent<QVector3D>& unlitData,
	bool isReflectionEnabled)
{
	QVector3D position = transform->getPosition();
	QMatrix4x4 model;
	model.translate(position);
	model.rotate(transform->getRotation());
	if(isReflectionEnabled)
		ReflectionProbe::setEnvironmentCubemap(position);

	unlitShader.use();
	// vertex shader uniform variables	
	unlitShader.setUniformMatrix(modelToProjection,
		projection * view * model);
	unlitShader.setUniformInt(UnlitRenderer::isScaleEnabled, isScaleEnabled);
	if (isScaleEnabled)
	{
		unlitShader.setUniformInt(UnlitRenderer::scaleAlongNormals, scaleAlongNormals);
		unlitShader.setUniformFloat(UnlitRenderer::scale, scale);
	}

	unlitShader.setUniformInt(isTextureEnabled, unlitData.isSingle == false);
	if (unlitData.isSingle)
	{
		unlitShader.setUniform3f(color, unlitData.data.singleColor);
	}
	else
	{
		unlitShader.setTextureUnit(unlitData.data.texture, texture0, 0);
	}
}

void UnlitRenderer::render(const QMatrix4x4& projection, const QMatrix4x4& view, bool isReflectionEnabled)
{
	setShaderParameters(projection, view, transform, false, false, 1.0f, unlitData, isReflectionEnabled);
	mesh->draw();
}

ShaderProgram UnlitRenderer::unlitShader;
int UnlitRenderer::modelToProjection = -1,
	UnlitRenderer::isScaleEnabled = -1,
	UnlitRenderer::scaleAlongNormals = -1,
	UnlitRenderer::scale = -1,
	UnlitRenderer::isTextureEnabled = -1,
	UnlitRenderer::texture0 = -1,
	UnlitRenderer::color = -1;
